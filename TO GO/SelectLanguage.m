//
//  SelectLanguage.m
//  TO GO
//
//  Created by volive solutions on 25/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "SelectLanguage.h"
#import "sharedclass.h"
#import "Login.h"
#import "AppDelegate.h"
#import "Home.h"

@interface SelectLanguage ()
{
    NSString *languageString;
    BOOL isSelected;
    AppDelegate * appDel;
}

@end

@implementation SelectLanguage

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    _englishBtn_Outlet.layer.cornerRadius = 4;
    _arabicBtn_Outlet.layer.cornerRadius = 4;
    
    isSelected = NO;
    
    // self.navigationController.navigationBar.hidden = YES;
   
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
//    float degrees = 45.0;
//    float radians = (degrees/180.0) * M_PI;
    
//    [UIView animateWithDuration:0.25
//                     animations:^{
//                         _logoImageView.transform = CGAffineTransformScale(view.transform, 0.01, 0.01);;
//                     }];
    
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
//        self.logoImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.englishBtn_Outlet.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.arabicBtn_Outlet.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
        self.logoImageView.transform = CGAffineTransformMakeRotation(360.0);
        //self.englishBtn_Outlet.transform = CGAffineTransformMakeRotation(360.0);
       // self.arabicBtn_Outlet.transform = CGAffineTransformMakeRotation(360.0);
        
        
    }completion:^(BOOL finished){
        
        // Finished scaling down imageview, now resize it
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.logoImageView.transform = CGAffineTransformIdentity;
            self.englishBtn_Outlet.transform = CGAffineTransformIdentity;
            self.arabicBtn_Outlet.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            
            
        }];
        
        
    }];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)english_BTN:(id)sender {
    
   // if (_englishBtn_Outlet.isSelected == YES) {
    languageString = @"en";
    [[NSUserDefaults standardUserDefaults]setObject:languageString forKey:@"language"];
    
//    if ([appDel.side isEqualToString:@"side"]) {
//        
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self.navigationController pushViewController:home animated:TRUE];
//
//        
//    }else{
    
    Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
    [self.navigationController pushViewController:login animated:TRUE];
    
    
    //}
    
}
- (IBAction)arabic_BTN:(id)sender {
    
   // if (_englishBtn_Outlet.isSelected == YES) {
    languageString = @"ar";
    [[NSUserDefaults standardUserDefaults]setObject:languageString forKey:@"language"];
    
//    if ([appDel.side isEqualToString:@"side"]) {
//        
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self.navigationController pushViewController:home animated:TRUE];
//
//    }else{
    
        Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
        [self.navigationController pushViewController:login animated:TRUE];
    //}


}
@end
