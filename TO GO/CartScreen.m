    //
//  CartScreen.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "CartScreen.h"
#import "CartTableViewCell.h"
#import "SWRevealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"
#import "payment.h"
#import "UIViewController+ENPopUp.h"
#import "ApprovedAlertController.h"
#import "AddnewCard.h"
#import "paymentCell.h"
#import "OrderHistory.h"
#import "AppDelegate.h"
#import "TrackOrder.h"
#import "RestaurantMenu.h"


@interface CartScreen ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GMSMapViewDelegate>{
    NSArray *cartlist;
    
    NSString *dateString,*orderTotalString,*payByCashString,*paymentTypeString,*shopLatitudeString,*shopLongitudeString,*transactionIdString,*codString,*cardIDString;
    
    UIToolbar *toolbar1;
    NSMutableArray *namesArray;
    NSMutableArray *priceArray;
    NSMutableArray *flavoursPriceArray;
    NSMutableArray *flavourNameArray;
    NSMutableArray *itemCountArray;
    NSMutableArray *itemIdsArray;
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    NSMutableArray *priceArrayConstant;
    
    NSMutableArray *smallCostArray;
    NSMutableArray *mediumCostArray;
    NSMutableArray *largeCostArray;
    NSMutableArray *sizeArray;
    
    NSString *smallCostString;
    NSString *mediumCostString;
    NSString *largeCostString;
    NSString *sizeString ,* statusG;
    int totalFinalCost;
    NSMutableArray * totalFinalPriceArr;
    
    
    NSMutableArray* card_namesArray;
    NSMutableArray* card_numbersArray;
    NSMutableArray* card_idsArray;
    NSMutableArray* selected_array;
    NSMutableArray * productType;
    
    NSString *logoImg;
    NSString *cat_name;
    NSString *shop_name;
    NSString *totalAmount;
    NSString *boldTotalAmount;
    NSString *itemCount;
    NSString *itemName;
    NSString *latitudeString;
    NSString *longitudeString;
    NSString *value;
    
    NSString *deleteString;
    
    int totalPrice;
    int price1 , priceA,priceB,priceC;
    int price2;
    NSString *tax;
    
    NSString *orderIdString,* PBCStr;
    
    NSString *myLat;
    NSString *myLong;
    
    NSString *destLat;
    NSString *destLong;
    BOOL isClicked2;
    NSString * successStr,*approvedStr;
    
    NSMutableArray *cartDictionary;
    
    GMSCameraPosition *cameraPosition;
    GMSMapView *googleMapView;
    GMSMarker *marker,*destMarker;
    GMSPolyline *polyLine;
    GMSMutablePath *mutablePath;
    GMSPath *path;
    
    CartTableViewCell *cell;
    ApprovedAlertController * approved;
    
    NSString *approveCheckString;
    
    paymentCell *payCell;
    
    
    NSString *progress;
    NSString* languageString;
    
    //Timer
    UILabel *progress1;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    int stepperValue;
    int mainItemCount;
    
    int price;

    
    int totalstr;
    NSString * TotalStr;
    
    BOOL isSelected;
    
    NSString * totalPriceStr;
    
    AppDelegate *delegate;

}

@end

@implementation CartScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    
    totalFinalPriceArr=[NSMutableArray new];

    currMinute=5;
    currSeconds=00;
    
    _checkStatus_Btn.hidden = YES;
    isSelected=NO;
    
    isClicked2 = YES;
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    approveCheckString = @"approve";
    
    destLat = @"17.4444187";
    destLong = @"78.3511019";
    
    productType = [NSMutableArray new];
    
    
    myLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    myLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
   // tax = [[NSUserDefaults standardUserDefaults]objectForKey:@"tax"];
    
    [_payBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Send For Approval"] forState:UIControlStateNormal];
    
    
    // Do any additional setup after loading the view.
    cartlist = @[@"",@""];
    
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Cart"];
    cartDictionary = [[NSMutableArray alloc]init];
    
    [self getServiceCall];
    
}

-(void)start
{
    delegate.progTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}

-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
            
        progress=[NSString stringWithFormat:@"%@%d%@%02d",@":",currMinute,@":",currSeconds];
        [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"%@ %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Waiting for approval,Please Wait..."],progress]];
        //[self.view setUserInteractionEnabled:TRUE];
    }
    else
    {
        [delegate.progTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:TRUE];
        [self orderStatusServiceCall];
       
    }
}

#pragma mark Get Cart Items Servicecall
-(void)getServiceCall{
    
    myLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    myLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/cart_items?API-KEY=225143&user_id=134721528&lang=en
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    [SVProgressHUD showWithStatus:progress];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];

    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"cart_items?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            namesArray = [NSMutableArray new];
            priceArray = [NSMutableArray new];
            itemIdsArray = [NSMutableArray new];
            itemCountArray = [NSMutableArray new];
            latitudeArray = [NSMutableArray new];
            longitudeArray = [NSMutableArray new];
            priceArrayConstant = [NSMutableArray new];
            flavoursPriceArray = [NSMutableArray new];
            flavourNameArray = [NSMutableArray new];
            card_namesArray = [NSMutableArray new];
            card_numbersArray = [NSMutableArray new];
            card_idsArray = [NSMutableArray new];
            selected_array = [NSMutableArray new];
            smallCostArray = [NSMutableArray new];
            mediumCostArray = [NSMutableArray new];
            largeCostArray = [NSMutableArray new];
            sizeArray = [NSMutableArray new];
            
            // First Cell
            [namesArray addObject:@""];
            [priceArray addObject:@""];
            [itemIdsArray addObject:@""];
            [itemCountArray addObject:@""];
            [latitudeArray addObject:@""];
            [longitudeArray addObject:@""];
            [priceArrayConstant addObject:@""];
            [flavoursPriceArray addObject:@""];
            [flavourNameArray addObject:@""];
            [selected_array addObject:@""];
            [card_namesArray addObject:@""];
            [card_numbersArray addObject:@""];
            [card_idsArray addObject:@""];
            [smallCostArray addObject:@""];
            [mediumCostArray addObject:@""];
            [largeCostArray addObject:@""];
            [sizeArray addObject:@""];
            [productType addObject:@"FirstCell"];
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                NSArray *array = [dict objectForKey:@"data"];
                int totalCost = 0;
                
             //   tax = [[dict objectForKey:@"data"]objectForKey:@"tax"];
                for(NSDictionary *mainData in array){
                    
                    
                    [namesArray addObject:[mainData objectForKey:@"item_name"]];
                    [productType addObject:@"SecondCell"];
                    
                    sizeString = [mainData objectForKey:@"item_size"];
                    tax = [NSString stringWithFormat:@"%@",[mainData objectForKey:@"tax"]];
                    
                    if ([sizeString isEqualToString:@"small"]) {
                        
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"small_cost"] intValue];
                        priceA = [[mainData objectForKey:@"small_cost"] intValue];

                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        
                        totalPrice = (mainItemCount * price1) + price;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];

                        
                    }else if ([sizeString isEqualToString:@"medium"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"medium_cost"] intValue];
                        priceA = [[mainData objectForKey:@"medium_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        
                        totalPrice = (mainItemCount * price1) + price;

                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];

                        
                    }else if ([sizeString isEqualToString:@"large"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"large_cost"] intValue];
                        priceA = [[mainData objectForKey:@"large_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        
                        totalPrice = (mainItemCount * price1) + price;

                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];

                    }
                    
                    [flavoursPriceArray addObject:[mainData objectForKey:@"flavor_price"]];
                    [flavourNameArray addObject:[mainData objectForKey:@"flavors_name_en"]];
                   // [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];

                    [itemCountArray addObject:[mainData objectForKey:@"item_count"]];
                    [itemIdsArray addObject:[mainData objectForKey:@"id"]];
                    [sizeArray addObject:[mainData objectForKey:@"item_size"]];
                    
                    [smallCostArray addObject:[mainData objectForKey:@"small_cost"]];
                    [mediumCostArray addObject:[mainData objectForKey:@"medium_cost"]];
                    [largeCostArray addObject:[mainData objectForKey:@"large_cost"]];
                    
                    smallCostString = [mainData objectForKey:@"small_cost"];
                    mediumCostString = [mainData objectForKey:@"medium_cost"];
                    largeCostString = [mainData objectForKey:@"large_cost"];
                    
                    
                    NSLog(@"itemIdsArray ***** %@",itemIdsArray);
                    
                    [selected_array addObject:@""];
                    [card_namesArray addObject:@""];
                    [card_numbersArray addObject:@""];
                    [card_idsArray addObject:@""];
                    
                   // [latitudeArray addObject:[mainData objectForKey:@"latitude"]];
                   // [longitudeArray addObject:[mainData objectForKey:@"longitude"]];
                    latitudeString = [NSString stringWithFormat:@"%@",[mainData objectForKey:@"latitude"]];
                    longitudeString = [NSString stringWithFormat:@"%@",[mainData objectForKey:@"longitude"]];
                    [latitudeArray addObject:latitudeString];
                    [longitudeArray addObject:longitudeString];
                    totalCost = (totalCost + [[mainData objectForKey:@"cost"] intValue]);
                    
                    [cartDictionary addObject:mainData];
               
                }
                
                logoImg = [[array objectAtIndex:0] objectForKey:@"logo"];
                [[NSUserDefaults standardUserDefaults]setObject:logoImg forKey:@"logo"];
                shop_name = [[array objectAtIndex:0]objectForKey:@"shop_name"];
                [[NSUserDefaults standardUserDefaults]setObject:shop_name forKey:@"shopName"];
                totalAmount = [NSString stringWithFormat:@"%d",totalCost];
                boldTotalAmount = [NSString stringWithFormat:@"%d",totalCost];
                [[NSUserDefaults standardUserDefaults]setObject:boldTotalAmount forKey:@"totalAmount"];
                
                [namesArray addObject:@""];
                [priceArray addObject:@""];
                [itemIdsArray addObject:@""];
                [itemCountArray addObject:@""];
                [latitudeArray addObject:@""];
                [longitudeArray addObject:@""];
                [priceArrayConstant addObject:@""];
                [flavoursPriceArray addObject:@""];
                [flavourNameArray addObject:@""];
                [selected_array addObject:@""];
                [card_namesArray addObject:@""];
                [card_numbersArray addObject:@""];
                [card_idsArray addObject:@""];
                [smallCostArray addObject:@""];
                [mediumCostArray addObject:@""];
                [largeCostArray addObject:@""];
                [sizeArray addObject:@""];
                [productType addObject:@"ThirdCell"];
                
               dispatch_async(dispatch_get_main_queue(), ^{
                  // [self.cartTbl reloadData];
                   
                   [self getCardsServiceCall];
                   });
                
             
            }
            
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
            
        });
        
    }];
    
}
    
-(void)stepperChanged:(ANStepperView*)stepper{
    

        stepperValue = [stepper.currentTitle intValue];

        [itemCountArray replaceObjectAtIndex:stepper.tag withObject:[NSString stringWithFormat:@"%d",stepperValue]];

    value = [NSString stringWithFormat:@"%@",[priceArrayConstant objectAtIndex:stepper.tag]];
    
        int arrValue = [value intValue];

        int result = arrValue * stepperValue;
    
    
        NSLog(@"%d",result);
        NSString *modified = [NSString stringWithFormat:@"%d",result];
    
       [priceArray replaceObjectAtIndex:stepper.tag withObject:modified];
    
      //  totalFinalPriceArr=[NSMutableArray new];
    
    
        NSLog(@"quantity array %@",itemCountArray);
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [_cartTbl reloadData];
        });
    
}

- (IBAction)stepperAction:(id)sender {
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)Action:(UIBarButtonItem *)sender {
    
    delegate.side = @"backFromCart";
    [self.navigationController popViewControllerAnimated:TRUE];
    
//    NSLog(@"back1");
//    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)show
{
    
    
    [cell.payByCashTextField resignFirstResponder];
    
}
-(void)cancel
{
    [cell.payByCashTextField resignFirstResponder];
    
    
}

#pragma mark Tableview delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productType.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // if (indexPath.row == 0) {
    if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"FirstCell"]) {
   
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        [cell.logoImg sd_setImageWithURL:[NSURL URLWithString:logoImg] placeholderImage:[UIImage imageNamed:@""] completed:nil];
        cell.nameLbl.text = shop_name;

    }
   else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"SecondCell"]) {
        
//        cell= [tableView dequeueReusableCellWithIdentifier:@"cell"];
//        [cell.logoImg sd_setImageWithURL:[NSURL URLWithString:logoImg] placeholderImage:[UIImage imageNamed:@""] completed:nil];
//        cell.nameLbl.text = shop_name;
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        
        
        cell.qtyview.value = [[itemCountArray objectAtIndex:indexPath.row] integerValue];
       [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",cell.qtyview.value] forKey:@"count"];
        // cell.flavourLabel.text = [flavourNameArray objectAtIndex:index];
        
        NSString *myString = [[NSNumber numberWithFloat:cell.qtyview.value] stringValue];
        
        NSLog(@"stepper value %@",myString);
        cell.qtyview.tag = indexPath.row;
        cell.qtyview.maximumValue = 100000;
       
        [cell.qtyview addTarget:self action:@selector(stepperChanged:) forControlEvents:UIControlEventValueChanged];
        
        cell.itemNameLbl.text = [NSString stringWithFormat:@"%@(%@)",[namesArray objectAtIndex:indexPath.row],[sizeArray objectAtIndex:indexPath.row]];
        cell.flavourNameLabel.text = [flavourNameArray objectAtIndex:indexPath.row];
        cell.qtyview.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.qtyview.layer.borderWidth = 0.5;
       
       if ([sizeString isEqualToString:@"small"]) {
           
            price1 = [[priceArray objectAtIndex:indexPath.row] intValue];
           // price2 = [[flavoursPriceArray objectAtIndex:indexPath.row] intValue];
           
            totalPrice = 1 * (price1);
           
            [totalFinalPriceArr addObject:[NSString stringWithFormat:@"%d",totalPrice]];
           
       }else if ([sizeString isEqualToString:@"medium"]){
           
            price1 = [[priceArray objectAtIndex:indexPath.row] intValue];
           // price2 = [[flavoursPriceArray objectAtIndex:indexPath.row] intValue];
           
            totalPrice = 1 * (price1 );
            [totalFinalPriceArr addObject:[NSString stringWithFormat:@"%d",totalPrice]];
           
       }else if ([sizeString isEqualToString:@"large"]){
          
            price1 = [[priceArray objectAtIndex:indexPath.row] intValue];
            //price2 = [[flavoursPriceArray objectAtIndex:indexPath.row] intValue];
           
            totalPrice = 1 * (price1);
           
            [totalFinalPriceArr addObject:[NSString stringWithFormat:@"%d",totalPrice]];
       }
       
      
        totalFinalCost = 0;
       
           for (NSNumber * n in totalFinalPriceArr) {
               
               totalFinalCost += [n intValue];
           }

      // }
       
        cell.priceLbl.text = [NSString stringWithFormat:@"%d SAR",totalPrice];
    }
    else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"ThirdCell"]) {
        
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell3" forIndexPath:indexPath];
        cell.totalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Total"];
       // cell.taxNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Tax"];
        cell.grandTotalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Grand Total"];
        cell.yourOrderIdLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Your Order Id:"];
        
        cell.totalValueLabel.text = [NSString stringWithFormat:@"%d SAR",totalFinalCost];
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",totalFinalCost] forKey:@"price"];
        
        cell.orderIdLabel.text = orderIdString;
        [cell.updateBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Update"] forState:UIControlStateNormal];
        cell.updateBtn.layer.cornerRadius=5;
        cell.updateBtn.clipsToBounds=YES;
        
        cell.updateBtn.layer.borderColor=[[UIColor redColor]CGColor];
        cell.updateBtn.layer.borderWidth = 1;
 
        float totalA= totalFinalCost;
        
        int taxA= tax.intValue;
        
        totalstr= taxA * (totalA / 100);
        
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",totalstr] forKey:@"totalStr"];
        
        int Total = totalA + totalstr;
    
        cell.taxNameLabel.text=[NSString stringWithFormat:@"%@(%d%@)",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Tax"],taxA,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Per"]];
        
        cell.taxValueLabel.text=[NSString stringWithFormat:@"%d SAR",totalstr];
        
        TotalStr=[NSString stringWithFormat:@"%d",Total];
        [[NSUserDefaults standardUserDefaults]setObject:TotalStr forKey:@"grandTotalString"];
        
        cell.grandTotalValueLabel.text=[NSString stringWithFormat:@"%@ SAR",TotalStr];
        
        [self mapcalling];
    }
    else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"ForthCell"]) {
        
        
        if ([statusG isEqualToString:@"0"]) {
            
            cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell4" forIndexPath:indexPath];
            cell.cardHolderNameLabel.text = @"" ;
            
            cell.cardNumberLabel.text = @"";
            
            cell.hidden = YES;
            
        }else
        {
        
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell4" forIndexPath:indexPath];
        cell.cardHolderNameLabel.text = [card_namesArray objectAtIndex:indexPath.row] ? [card_namesArray objectAtIndex:indexPath.row]:@"" ;
       
        cell.cardNumberLabel.text = [card_numbersArray objectAtIndex:indexPath.row] ? [card_numbersArray objectAtIndex:indexPath.row]:@"";
        
        if ([[selected_array objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            
            [cell.cardBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
            
        }else{
            [cell.cardBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
            
        }
            
        }
    }
    else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"FifthCell"]){
      
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell5" forIndexPath:indexPath];
        
        cell.addNewCardLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Add New Card"];
        
        //[cell.deleteBtn addTarget:self action:@selector(deleteCartItemServiceCall:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"SixthCell"]){
        
        cell= [self.cartTbl dequeueReusableCellWithIdentifier:@"cell6" forIndexPath:indexPath];
        
        toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
        toolbar1.barStyle = UIBarStyleBlackOpaque;
        toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        toolbar1.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0];
        
        UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
        
        [doneButton1 setTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
        
        [cancelButton1 setTintColor:[UIColor whiteColor]];
        toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
        
        [cell.payByCashTextField setInputAccessoryView:toolbar1];

        
        
        cell.totalAmountLabel.text = [NSString stringWithFormat:@"%@ SAR",TotalStr];
        cell.CODLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"COD"];
        cell.payByCashLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Pay by cash"];
        cell.payByCashTextField.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Enter cash to get ready change from restaurant"];
        
        PBCStr = cell.payByCashTextField.text;
        
        [[NSUserDefaults standardUserDefaults]setObject:PBCStr forKey:@"PBCStr"];
        
        cell.carNumberLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Car Number"];
        cell.carTypeLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Car Type"];
      //  cell.carTypeTF.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Benz"];
        cell.carNumberTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"carNumber"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"carNumber"] :@"";
        cell.carTypeTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"carType"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"carType"]:@"";
        
        
         //[cell.deleteBtn addTarget:self action:@selector(deleteCartItemServiceCall:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([statusG isEqualToString:@"0"]) {
            
            codString = @"COD";
            //[[NSUserDefaults standardUserDefaults]objectForKey:@"CODString"]
            [[NSUserDefaults standardUserDefaults]setObject:codString forKey:@"CODString"];
             [cell.payByCashBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
            
            
//            if ([[selected_array objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
//                [cell.payByCashBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
//                
//            }
//            else
//            {
//                [cell.payByCashBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
//                
//            }
        
        }else{
        
        if ([[selected_array objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [cell.payByCashBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
            
        }
        else
        {
            [cell.payByCashBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
            
        }
        }
    }
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"FirstCell"]) {
       
       return 75;
     
   }
   else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"SecondCell"]){
       return 62;
       
   }
   else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"ThirdCell"]){
       return 270;
       
   }
   else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"ForthCell"]){
       return 56;
       
   }
   else if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"FifthCell"]){
       return 56;
       
   }
   else {
       return 192;
       
   }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    

    if ([[productType objectAtIndex:indexPath.row] isEqualToString:@"FifthCell"]) {
        AddnewCard *card = [self.storyboard instantiateViewControllerWithIdentifier:@"AddnewCard"];
        
        
        [self.navigationController pushViewController:card animated:YES];
    }
}

#pragma MapsCalling

-(void)mapcalling
{
    googleMapView.delegate = self;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[myLat doubleValue]
                                                            longitude:[myLong doubleValue]
                                                                 zoom:10];
    googleMapView = [GMSMapView mapWithFrame:cell.cartMapView.bounds camera:camera];
    
    googleMapView.settings.myLocationButton = NO;
    googleMapView.camera = camera;
    
    googleMapView.myLocationEnabled = YES;
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([myLat doubleValue], [myLong doubleValue]);
    marker.tracksViewChanges = YES;
    marker.map = googleMapView;
    
    destMarker = [[GMSMarker alloc]init];
    destMarker.position = CLLocationCoordinate2DMake([latitudeString doubleValue], [longitudeString doubleValue]);
    destMarker.map = googleMapView;
    
    UIImage *userIcon = [UIImage imageNamed:@"pin"];
    userIcon = [userIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (userIcon.size.height/2), 0)];
    destMarker.icon = userIcon;
    
    
    [self drawRoute];
    [cell.cartMapView addSubview:googleMapView];
    
//    cell.TotalPrice.text = totalAmount;
//    cell.boldPrice.text = [NSString stringWithFormat:@"To Pay $%@",boldTotalAmount];
    
    

}

#pragma mark sendForApprovalServiceCall
    
-(void)sendForApprovalServiceCall{
    
    /*
     API-KEY:225143
     user_id:134721528
     vendor_id:3288062319
     latitude:17.3596521
     longitude:78.32156259
     */
    
    myLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    myLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];

    _checkStatus_Btn.hidden = YES;
    
    if (isSelected == YES || [statusG isEqualToString:@"0"]) {
        
        if (cell.payByCashTextField.text.length > 0) {
            
        
        
        [self.view setUserInteractionEnabled:FALSE];
        
        [self start];
        
        languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
        //http://voliveafrica.com/togo/api/services/order_requst_restaurant
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        //[SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Waiting for approval,Please Wait..."]];
        
        //[SVProgressHUD showWithStatus:@"Waiting for approval,Please Wait... : 5m:00ss"];
        
        
        NSMutableDictionary * approvalPostDictionary = [[NSMutableDictionary alloc]init];
        
        [approvalPostDictionary setObject:APIKEY forKey:@"API-KEY"];
        [approvalPostDictionary setObject:languageString forKey:@"lang"];
        [approvalPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
        [approvalPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"] forKey:@"vendor_id"];
        [approvalPostDictionary setObject:myLat forKey:@"latitude"];
        [approvalPostDictionary setObject:myLong forKey:@"longitude"];
        
        [[sharedclass sharedInstance]urlPerameterforPost:@"order_requst_restaurant" withPostDict:approvalPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
            if (dict) {
                NSLog(@"Images from server %@", dict);
                
                NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                
                //            successStr=[NSString stringWithFormat:@"%@",[dict objectForKey:@""]];
                //            approvedStr=[NSString stringWithFormat:@"%@",[dict objectForKey:@""]];
                
                // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([status isEqualToString:@"1"])
                    {
                        
                       // [SVProgressHUD dismiss];
                        _proceedToPay_Outlet.enabled = YES;
                        _proceedToPay_Outlet.userInteractionEnabled = YES;
                        
                        orderIdString = [dict objectForKey:@"order_id"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:orderIdString forKey:@"orderId"];
                        
                        cell.orderIdLabel.text = orderIdString;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [_cartTbl reloadData];
                            
                        });
                        
                      }
                    else
                    {
                        _proceedToPay_Outlet.enabled = YES;
                        _proceedToPay_Outlet.userInteractionEnabled = YES;

                        [SVProgressHUD dismiss];
                        // [_proccedToPayBtn setHidden:YES];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                        });
                        //[_payBtn_Outlet setTitle:@"SEND TO APPROVAL" forState:UIControlStateNormal];
                        
                    }
                });
            }
            else{
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
        }];
    }else{
        
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Please enter amount"] onViewController:self completion:nil];
        });
    }
    }else{
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Please Select Payment Type"] onViewController:self completion:nil];
        
    }
}


#pragma mark Order Status Service Call
-(void)orderStatusServiceCall{
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/togo/api/services/order_expired?API-KEY=225143&lang=ar&order_id=5273323857251
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:orderIdString forKey:@"order_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    [viewCartPostDictionary setObject:[NSString stringWithFormat:@"%d",totalstr] forKey:@"tax_amount"];
    [viewCartPostDictionary setObject:TotalStr forKey:@"grand_total"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"order_expired?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
                
                //[self getServiceCall];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
        });
        
    }];

}

#pragma mark Update CartCount Servicecall
-(void)updateCartCountServicecall:(id)sender{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_cartTbl];
    NSIndexPath *indexPath = [_cartTbl indexPathForRowAtPoint:btnPosition];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/update_cartcount?API-KEY=225143&id=75&count=1
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[itemIdsArray objectAtIndex:indexPath.row] forKey:@"id"];
    [viewCartPostDictionary setObject:[NSString stringWithFormat:@"%d",stepperValue] forKey:@"count"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"update_cartcount?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
        });
        
    }];
    
}


- (IBAction)proceedToPay:(id)sender {
    
    _proceedToPay_Outlet.enabled = NO;
    _proceedToPay_Outlet.userInteractionEnabled = NO;
    
    [self sendForApprovalServiceCall];
    
   }

-(void)dismiss
{
    [self dismissPopUpViewController];
    
}

#pragma mark Deaw Route
 - (void)drawRoute
 {
     dispatch_async(dispatch_get_main_queue(), ^{
     CLLocation *mylocation = [[CLLocation alloc]initWithLatitude:[myLat doubleValue] longitude:[myLong doubleValue]];
     
     CLLocation *destination = [[CLLocation alloc]initWithLatitude:[latitudeString doubleValue] longitude:[longitudeString doubleValue]];
     
     [self fetchPolylineWithOrigin:mylocation destination:destination completionHandler:^(GMSPolyline *polyline)
      {
          if(polyline)
              polyline.map = googleMapView;
      }];
     });
 }



#pragma mark Fetch Polyline
 - (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
 {
     NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
     NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
     NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
     NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
     NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
 
 
     NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
            ^(NSData *data, NSURLResponse *response, NSError *error)
        {
     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
     if(error)
     {
         if(completionHandler)
             completionHandler(nil);
         return;
     }
 
     NSArray *routesArray = [json objectForKey:@"routes"];
 
     GMSPolyline *polyline = nil;
     if ([routesArray count] > 0)
     {
         NSDictionary *routeDict = [routesArray objectAtIndex:0];
         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
         path = [GMSPath pathFromEncodedPath:points];
         polyline = [GMSPolyline polylineWithPath:path];
     }
            polyline.strokeColor = [UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0];
            polyline.strokeWidth = 2;
 
 // run completionHandler on main thread
     dispatch_sync(dispatch_get_main_queue(), ^{
         if(completionHandler)
             completionHandler(polyline);
     });
    }];
     [fetchDirectionsTask resume];
    }

#pragma mark Delete Cart Item Service Call
- (IBAction)deleteCartItemBtn:(id)sender {

    /*
     API-KEY:225143 
     user_id:2510747706
     id:1
     */
    
    deleteString = @"delete";
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    if (flavourNameArray.count>0) {
        
        CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.cartTbl];
        NSIndexPath *indexPath = [self.cartTbl indexPathForRowAtPoint:btnPosition];
        
        
        //http://voliveafrica.com/togo/api/services/delete_cart_item
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
        
        
        NSString * idStr=[NSString stringWithFormat:@"%@",[itemIdsArray objectAtIndex:indexPath.row]];
        
        
        NSMutableDictionary * deleteCartPostDictionary = [[NSMutableDictionary alloc]init];
        
        [deleteCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
        [deleteCartPostDictionary setObject:languageString forKey:@"lang"];
        [deleteCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
        //  [deleteCartPostDictionary setObject:idStr forKey:@"item_id"];
        
        if ([idStr isEqualToString:@""]) {
            
            idStr=@"0";
            
        }else{

            NSString * idStr=[NSString stringWithFormat:@"%@",[itemIdsArray objectAtIndex:indexPath.row-1]];
        }
        
        [deleteCartPostDictionary setObject:idStr forKey:@"id"];
        
        
        [deleteCartPostDictionary setObject:idStr forKey:@"flavour_id"];
        
        [[sharedclass sharedInstance]urlPerameterforPost:@"delete_cart_item" withPostDict:deleteCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
            
            if (dict) {
                
                NSLog(@"Images from server %@", dict);
                
                NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                
                
                // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([status isEqualToString:@"1"])
                    {
                         totalFinalCost=0;
                        
                        totalFinalPriceArr = [[NSMutableArray alloc]init];
                        
                        [SVProgressHUD dismiss];
                        
                        [priceArray removeObjectAtIndex:indexPath.row];
                        [namesArray removeObjectAtIndex:indexPath.row];
                        [itemCountArray removeObjectAtIndex:indexPath.row];
                        [itemIdsArray removeObjectAtIndex:indexPath.row];
                        [flavourNameArray removeObjectAtIndex:indexPath.row];
                        [flavoursPriceArray removeObjectAtIndex:indexPath.row];
                        
                        [latitudeArray removeObjectAtIndex:indexPath.row];
                        [longitudeArray removeObjectAtIndex:indexPath.row];
                        [priceArrayConstant removeObjectAtIndex:indexPath.row];
                        
                        [smallCostArray removeObjectAtIndex:indexPath.row];
                        [mediumCostArray removeObjectAtIndex:indexPath.row];
                        [largeCostArray removeObjectAtIndex:indexPath.row];
//                        [totalFinalPriceArr removeObjectAtIndex:indexPath.row];
                        
                        
                        [selected_array removeObjectAtIndex:indexPath.row];
                        [card_namesArray removeObjectAtIndex:indexPath.row];
                        [card_numbersArray removeObjectAtIndex:indexPath.row];
                        [card_idsArray removeObjectAtIndex:indexPath.row];
                        [productType removeObjectAtIndex:indexPath.row];
                        
                       
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                             [_cartTbl reloadData];
                            
                           // [self getServiceCall];
                        }];
                        
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
                           // [_cartTbl reloadData];
//                        });
                        
                    }
                    else
                    {
                        [SVProgressHUD dismiss];
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                        });
                        
                        
                    }
                });
            }
            else{
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
        }];
    }else{
        
        CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.cartTbl];
        NSIndexPath *indexPath = [self.cartTbl indexPathForRowAtPoint:btnPosition];
        languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
        
        //http://voliveafrica.com/togo/api/services/delete_cart_item
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
        
        
        NSString * idStr=[NSString stringWithFormat:@"%@",[itemIdsArray objectAtIndex:indexPath.row-1]];
        
        
        NSMutableDictionary * deleteCartPostDictionary = [[NSMutableDictionary alloc]init];
        
        [deleteCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
        [deleteCartPostDictionary setObject:languageString forKey:@"lang"];
        [deleteCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
        //  [deleteCartPostDictionary setObject:idStr forKey:@"item_id"];
        [deleteCartPostDictionary setObject:idStr forKey:@"id"];
        
        [[sharedclass sharedInstance]urlPerameterforPost:@"delete_cart_item" withPostDict:deleteCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
            
            if (dict) {
                
                NSLog(@"Images from server %@", dict);
                
                NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                
                
                // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([status isEqualToString:@"1"])
                    {
                        
                        [SVProgressHUD dismiss];
                        
                        [priceArray removeObjectAtIndex:indexPath.row];
                        [namesArray removeObjectAtIndex:indexPath.row];
                        [itemCountArray removeObjectAtIndex:indexPath.row];
                        [itemIdsArray removeObjectAtIndex:indexPath.row];
                        [flavourNameArray removeObjectAtIndex:indexPath.row];
                        [flavoursPriceArray removeObjectAtIndex:indexPath.row];
                        
                        [latitudeArray removeObjectAtIndex:indexPath.row];
                        [longitudeArray removeObjectAtIndex:indexPath.row];
                        [priceArrayConstant removeObjectAtIndex:indexPath.row];
                        //[totalFinalPriceArr removeObjectAtIndex:indexPath.row];
                        
                        
                        [selected_array removeObjectAtIndex:indexPath.row];
                        [card_namesArray removeObjectAtIndex:indexPath.row];
                        [card_numbersArray removeObjectAtIndex:indexPath.row];
                        [card_idsArray removeObjectAtIndex:indexPath.row];
                        [productType removeObjectAtIndex:indexPath.row];
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"OK"] style:UIAlertActionStyleDefault handler:nil];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_cartTbl reloadData];
                        });
                        
                    }
                    else
                    {
                        [SVProgressHUD dismiss];
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                        });
                        
                        
                    }
                });
            }
            else{
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
        }];

        
        
    }
    
    
   
}


#pragma mark Get Cards Service Call
-(void)getCardsServiceCall{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/togo/api/services/payment_cards?API-KEY=225143&user_id=134721528
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    [SVProgressHUD dismiss];
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"payment_cards?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
         statusG = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            if ([statusG isEqualToString:@"1"])
            {
                NSArray *data  = [dict objectForKey:@"data"];
                
                for(NSDictionary *mainData in data){
                    
                    [card_namesArray addObject:[mainData objectForKey:@"name"]];
                    [productType addObject:@"ForthCell"];
                    [card_numbersArray addObject:[mainData objectForKey:@"card_number"]];
                    [card_idsArray addObject:[mainData objectForKey:@"id"]];
                    [selected_array addObject:@"0"];
                    
                    [namesArray addObject:@""];
                    [priceArray addObject:@""];
                    [itemIdsArray addObject:@""];
                    [itemCountArray addObject:@""];
                    [latitudeArray addObject:@""];
                    [longitudeArray addObject:@""];
                    [priceArrayConstant addObject:@""];
                    [flavoursPriceArray addObject:@""];
                    [flavourNameArray addObject:@""];
                    
                }
                [selected_array addObject:@"0"];
                [selected_array addObject:@"0"];
                
                [namesArray addObject:@""];
                [priceArray addObject:@""];
                [itemIdsArray addObject:@""];
                [itemCountArray addObject:@""];
                [latitudeArray addObject:@""];
                [longitudeArray addObject:@""];
                [priceArrayConstant addObject:@""];
                [flavoursPriceArray addObject:@""];
                [flavourNameArray addObject:@""];
                [selected_array addObject:@""];
                [card_namesArray addObject:@""];
                [card_numbersArray addObject:@""];
                [card_idsArray addObject:@""];
                
                [productType addObject:@"FifthCell"];
                
                [namesArray addObject:@""];
                [priceArray addObject:@""];
                [itemIdsArray addObject:@""];
                [itemCountArray addObject:@""];
                [latitudeArray addObject:@""];
                [longitudeArray addObject:@""];
                [priceArrayConstant addObject:@""];
                [flavoursPriceArray addObject:@""];
                [flavourNameArray addObject:@""];
                [selected_array addObject:@""];
                [card_namesArray addObject:@""];
                [card_numbersArray addObject:@""];
                [card_idsArray addObject:@""];
                [productType addObject:@"SixthCell"];
                [SVProgressHUD dismiss];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_cartTbl reloadData];
                
                });
                
            }
            else
            {
                
                [SVProgressHUD dismiss];
                
                
                [selected_array addObject:@"0"];
                //[productType addObject:@"ForthCell"];
                [productType addObject:@"FifthCell"];
                [productType addObject:@"SixthCell"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                [_cartTbl reloadData];

                });
//                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
            }
        });
        
    }];
    
    
}

#pragma mark COD Button Action
- (IBAction)COD_BTN:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_cartTbl];
    NSIndexPath *indexPath = [_cartTbl indexPathForRowAtPoint:btnPosition];
    
    isSelected=YES;
    
    
    for (int i=0; i<selected_array.count; i++) {
        if (i==indexPath.row) {
            
            [selected_array replaceObjectAtIndex:indexPath.row withObject:@"1"];
            codString = @"COD";
            [[NSUserDefaults standardUserDefaults]setObject:codString forKey:@"CODString"];
            cardIDString =@"";
            [[NSUserDefaults standardUserDefaults]setObject:cardIDString forKey:@"cardId"];
            
        }
        else{
            [selected_array replaceObjectAtIndex:i withObject:@"0"];
        }
    }
   dispatch_async(dispatch_get_main_queue(), ^{
    [_cartTbl reloadData];
   });

}


#pragma mark Cards Button Action
- (IBAction)cards_BTN:(id)sender {
    
    isSelected=YES;
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_cartTbl];
    NSIndexPath *indexPath = [_cartTbl indexPathForRowAtPoint:btnPosition];
    
    
    for (int i=0; i<selected_array.count; i++) {
        if (i==indexPath.row) {
            [selected_array replaceObjectAtIndex:indexPath.row withObject:@"1"];
            codString = @"Online";
            [[NSUserDefaults standardUserDefaults]setObject:codString forKey:@"CODString"];
            cardIDString = [card_idsArray objectAtIndex:indexPath.row];
            [[NSUserDefaults standardUserDefaults]setObject:cardIDString forKey:@"cardId"];
            
        }
        else{
            [selected_array replaceObjectAtIndex:i withObject:@"0"];
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [_cartTbl reloadData];
    });
    
}

#pragma mark Delete card Servicecall
- (IBAction)deleteCard_BTN:(id)sender {
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/togo/api/services/delete_card?API-KEY=225143&user_id=134721528&card_id=1
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_cartTbl];
    NSIndexPath *indexPath = [_cartTbl indexPathForRowAtPoint:btnPosition];
    
    
    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc]init];
    [paymentDictionary setObject:APIKEY forKey:@"API-KEY"];
    [paymentDictionary setObject:languageString forKey:@"lang"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [paymentDictionary setObject:[card_idsArray objectAtIndex:indexPath.row] forKey:@"card_id"];
    [[sharedclass sharedInstance]fetchResponseforParameter:@"delete_card?" withPostDict:paymentDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                [self getServiceCall];
            }
            else
            {
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Error"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                [SVProgressHUD dismiss];
                
            }
        });
        
    }];

    
}


- (IBAction)updateBtnAction:(id)sender {
    
    [self updateCartCountServicecall:sender];

}


    
#pragma mark Delete Payment Cards
    //- (IBAction)deletePayment:(id)sender {
    //    //http://voliveafrica.com/togo/api/services/delete_card?API-KEY=225143&user_id=134721528&card_id=1
    //    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mytableview];
    //    NSIndexPath *indexPath = [_mytableview indexPathForRowAtPoint:btnPosition];
    //
    //
    //    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc]init];
    //    [paymentDictionary setObject:APIKEY forKey:@"API-KEY"];
    //    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    //    [paymentDictionary setObject:[card_idsArray objectAtIndex:indexPath.row] forKey:@"card_id"];
    //    [[sharedclass sharedInstance]fetchResponseforParameter:@"delete_card?" withPostDict:paymentDictionary andReturnWith:^(NSData *dataFromJson) {
    //
    //        NSDictionary *dict = (NSDictionary *)dataFromJson;
    //
    //        NSLog(@"Images from server %@", dict);
    //        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            if ([status isEqualToString:@"1"])
    //            {
    //                [SVProgressHUD dismiss];
    //                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
    //                [self getServiceCall];
    //            }
    //            else
    //            {
    //                [[sharedclass sharedInstance]showAlertWithTitle:@"Error" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
    //                [SVProgressHUD dismiss];
    //
    //            }
    //        });
    //
    //    }];
    //
    //}
    

@end
