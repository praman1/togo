//
//  DetailedMenuTableViewCell.h
//  TO GO
//
//  Created by volive solutions on 15/12/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantSubNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantItemImageView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn_Outlet;

@end
