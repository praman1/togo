//
//  AddnewCard.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "AddnewCard.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"

@interface AddnewCard ()
{
    sharedclass *ObjForShare;
    NSString *amountString;
    
    NSString* languageString;

}
@end

@implementation AddnewCard

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _payNowBtn_Outlet.userInteractionEnabled = FALSE;
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    amountString = [[NSUserDefaults standardUserDefaults]objectForKey:@"totalAmount"];
    
    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"New Card"];
    
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    _enterDetailsLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Enter Your Card Details"];
    _nameOnCardLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Name On Card"];
    _cardNumberLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Card Number"];
    _cardExpiryLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Card Expiry"];
    _cvvLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"CVV"];

    [_securelySaveBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Securely save my card for future use"] forState:UIControlStateNormal];
    
    _amountLabel.text = [NSString stringWithFormat:@"   Payment :$%@",amountString];
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    
    ObjForShare = [[sharedclass alloc] init];
    
    [ObjForShare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [ObjForShare textfieldAsLine:_numberTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [ObjForShare textfieldAsLine:_expireTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [ObjForShare textfieldAsLine:_CVVTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    
    
    
}
-(void)back:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getServiceNewCard{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/add_payment_card
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading,please wait..."]];
    NSMutableDictionary * addNewCardDicitionary = [[NSMutableDictionary alloc]init];
    [addNewCardDicitionary setObject:APIKEY forKey:@"API-KEY"];
    [addNewCardDicitionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [addNewCardDicitionary  setObject:_nameTxt.text forKey:@"name"];
    [addNewCardDicitionary setObject:_numberTxt.text forKey:@"card_number"];
    [addNewCardDicitionary setObject:_expireTxt.text forKey:@"card_expiry"];
     [addNewCardDicitionary setObject:_CVVTxt.text forKey:@"cvv"];
    [addNewCardDicitionary setObject:languageString forKey:@"lang"];
    
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_payment_card" withPostDict:addNewCardDicitionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            [SVProgressHUD dismiss];
            
            [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            
        }else{
            [SVProgressHUD dismiss];
            [[sharedclass sharedInstance]showAlertWithTitle:@"Error" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
        }
        
        
    
    }];
    
    
    
}

- (IBAction)payNowBtn:(id)sender {
    
    [self getServiceNewCard];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
