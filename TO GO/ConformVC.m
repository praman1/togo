//
//  ConformVC.m
//  TO GO
//
//  Created by volive solutions on 26/02/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "ConformVC.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "UIViewController+ENPopUp.h"
#import "AppDelegate.h"
#import "HeaderAppConstant.h"
#import "ServiceProviderController.h"
#import "TrackOrder.h"

@interface ConformVC ()<UITextFieldDelegate>
{
    
    AppDelegate*delegate;
    
    NSString *languageString;
    NSString *orderStatusString;
    
    
}

@end

@implementation ConformVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if ([delegate.spCheckString isEqualToString:@"conform"]) {
        
        _headingLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Item Preparing Time"];
        _timeTextfield.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Time.."];
        
        [_saveBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"SAVE"] forState:UIControlStateNormal];
        [_cancelBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"CANCEL"] forState:UIControlStateNormal];
    }else if ([delegate.spCheckString isEqualToString:@"reject"]){
        
        _headingLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order Rejected Reason"];
        _timeTextfield.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Rejected Reason"];
        
        [_saveBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"SAVE"] forState:UIControlStateNormal];
        [_cancelBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"CANCEL"] forState:UIControlStateNormal];
    }
    
    _timeTextfield.layer.cornerRadius = 4;
    _saveBtn_Outlet.layer.cornerRadius = 4;
    _cancelBtn_Outlet.layer.cornerRadius = 4;
    
    
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
//        [self. addGestureRecognizer:tap];

    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshMenu) name:@"refreshMenu" object:nil];
    

}

//-(void)refreshMenu
//{
//   
//    [self dismissViewControllerAnimated:TRUE completion:nil];
//}



-(void)hide{
    
    [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _timeTextfield.layer.borderColor = [[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]CGColor];
    
    if ([delegate.spCheckString isEqualToString:@"conform"]) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [textField becomeFirstResponder];
        
    }else if ([delegate.spCheckString isEqualToString:@"reject"]){
        
        textField.keyboardType = UIKeyboardTypeDefault;
        [textField becomeFirstResponder];

    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}



-(void)SPUpdateCartServicecall{
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    /*
     API-KEY:225143
     order_id:6795718359937
     order_status:2
     user_id:1051744624
     preparing_time:20
     reason:
     */
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/update_order_status";
    
    
    NSMutableDictionary * SPUpdateCartDictionary = [[NSMutableDictionary alloc]init];
    [SPUpdateCartDictionary setObject:APIKEY forKey:@"API-KEY"];
    [SPUpdateCartDictionary setObject:[NSString stringWithFormat:@"%@",_orderId ? _orderId :@""] forKey:@"order_id"];

    if ([delegate.spCheckString isEqualToString:@"conform"]){
        
        [SPUpdateCartDictionary setObject:_timeTextfield.text forKey:@"preparing_time"];
        [SPUpdateCartDictionary setObject:@"2" forKey:@"order_status"];
        
    }else if ([delegate.spCheckString isEqualToString:@"reject"]){
        
        [SPUpdateCartDictionary setObject:_timeTextfield.text forKey:@"reason"];
        [SPUpdateCartDictionary setObject:@"5" forKey:@"order_status"];
    }
    
    [SPUpdateCartDictionary setObject:languageString forKey:@"lang"];
    [SPUpdateCartDictionary setObject:[NSString stringWithFormat:@"%@",_userId ? _userId :@""] forKey:@"user_id"];
    
   
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"update_order_status" withPostDict:SPUpdateCartDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            
            NSLog(@"Images from server %@", dict);
            
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                
                orderStatusString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"order_status"]];
                
                NSLog(@"My orderStatusString %@",orderStatusString);
                
//             dispatch_async(dispatch_get_main_queue(), ^{
//                
//            [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
//                 
//                 });
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        
                        if ([orderStatusString isEqualToString:@"2"]) {
                            
                            delegate.orderStr = @"order";
                            
                            [[NSNotificationCenter defaultCenter] removeObserver:self];
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                        }
                        else if ([orderStatusString isEqualToString:@"5"]){
                            
                            delegate.orderStr = @"service";
                            
                            [[NSNotificationCenter defaultCenter] removeObserver:self];
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:nil];
                        }
                        
                        [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
                    }];
                    
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                });
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];

    
    
}

- (IBAction)saveBTN:(id)sender {
    
    [_timeTextfield resignFirstResponder];
    
    if (_timeTextfield.text.length > 0) {
        
        [self SPUpdateCartServicecall];
        
    }else if ([delegate.spCheckString isEqualToString:@"conform"]){
        
        [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:@"Please enter preparing time" onViewController:self completion:nil];
        
    }else if ([delegate.spCheckString isEqualToString:@"reject"]){
        
        [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:@"Please enter reject reason" onViewController:self completion:nil];
    }
    
    
    
}
- (IBAction)cancelBtn:(id)sender {
    
   [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
}
@end
