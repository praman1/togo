//
//  AddCartTableViewCell.h
//  TO GO
//
//  Created by volive solutions on 10/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"

@interface AddCartTableViewCell : UITableViewCell
//*** Cell1
@property (weak, nonatomic) IBOutlet UILabel *flavourLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (strong, nonatomic) IBOutlet ANStepperView *quantityStepperView;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;

//***Cell2
    @property (weak, nonatomic) IBOutlet UILabel *splRequestLabel;

@property (weak, nonatomic) IBOutlet UITextField *requestTextField;

@end
