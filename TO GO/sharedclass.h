//
//  sharedclass.h
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import <Reachability/Reachability.h>

@interface sharedclass : NSObject
-(void)textfieldAsLine:(UITextField *)myTextfield lineColor:(UIColor *)lineColor myView:(UIView *)view;

typedef void (^ ToGoGet) ( NSData * dataFromJson );
typedef void(^ToGoPost) (NSDictionary*  dict, NSArray*  arr, BOOL success, NSString * responseString);
+(sharedclass *)sharedInstance;

@property (strong ,nonatomic) NSDictionary *json;

-(void)showprogressfor:(NSString *)str;

-(void)showprogressforSucsess:(NSString *)str;

-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(ToGoGet)completionHandler;
-(void)urlPerameterforPost:(NSString* )paramStr withPostDict:(NSDictionary* )postDict andReturnwith:(ToGoPost)completionHandler;
-(NSString*)languageSelectedStringForKey:(NSString*) key;
-(void)showalrtfor:(NSString* )str inViewCtrl:(UIViewController* )vc;
-(BOOL)isNetworkAvalible;

@end
