//
//  OrderHistory.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "OrderHistory.h"
#import "SWRevealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "OrderHistoryTableViewCell.h"
#import "TrackOrder.h"
#import "OrderViewDetailsController.h"
#import "UIViewController+ENPopUp.h"
#import "Home.h"
#import "AppDelegate.h"

@interface OrderHistory ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *placedDateArray;
    NSMutableArray *orderIdArray;
    NSMutableArray *itemImageArray;
    NSMutableArray *itemNameArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *totalAmountArray;
    NSMutableArray *statusInfoArray;
    NSMutableArray *mobileNumberArray;
    OrderHistoryTableViewCell *orderCell;
    OrderViewDetailsController * order;
    Home *homeVC;
    
    AppDelegate *delegate;
    
    NSString *languageString;
}

@end

@implementation OrderHistory

- (void)viewDidLoad {
    [super viewDidLoad];
    [self orderHistoryServiceCall];
    // Do any additional setup after loading the view.
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController )
//    {
//        [self.menu setTarget: homeVC];
//        [self.menu setAction: @selector( back )];
    
        //[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
   // }

}

//-(void)back{
//    Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//    [self.navigationController pushViewController:home animated:TRUE];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewTrackOrder:(id)sender{
    
    delegate.spCheckString = @"customer";
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_orderHistoryTableView];
    NSIndexPath *indexPath = [_orderHistoryTableView indexPathForRowAtPoint:btnPosition];
    
    TrackOrder *trackOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
    trackOrderVC.checkString = delegate.spCheckString;
    trackOrderVC.nameStr=[itemNameArray objectAtIndex:indexPath.row];
    trackOrderVC.imageStr=[itemImageArray objectAtIndex:indexPath.row];
    
    [trackOrderVC trackOrderServiceCall:[orderIdArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:trackOrderVC animated:TRUE];
    
}


-(void)callToRestaurant:(id)sender{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_orderHistoryTableView];
    NSIndexPath *indexPath = [_orderHistoryTableView indexPathForRowAtPoint:btnPosition];
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",[mobileNumberArray objectAtIndex:indexPath.row]];
    NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
    [SVProgressHUD dismiss];
    //[phoneURL release];
   // [phoneStr release];
    
}


-(void)viewDetails:(id)sender
{
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_orderHistoryTableView];
    NSIndexPath *indexPath = [_orderHistoryTableView indexPathForRowAtPoint:btnPosition];
    
    delegate.spCheckString = @"yes";
    
    order =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewDetailsController"];
    order.statusString = [statusInfoArray objectAtIndex:indexPath.row];
    order.checkString = delegate.spCheckString;
    
    //order.view.frame = CGRectMake(0, 0, 300.0f, 450.0f);
    
    [order setModalPresentationStyle:UIModalPresentationOverCurrentContext];
   [self presentViewController:order animated:YES completion:nil];
    
    //[order.closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    
    
   // [self presentPopUpViewController:order];
    
}

-(void)dismiss
{
  
     [self dismissPopUpViewController];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return itemNameArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    orderCell = [_orderHistoryTableView dequeueReusableCellWithIdentifier:@"orderCell"];
    
    if(orderCell == nil)
    {
        orderCell = [[OrderHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderCell"];
    }
    
    // NSString *str = [NSString stringWithFormat:@"%@ : %@",[[SharedClass sharedInstance]languageSelectedStringForKey:@"Your Order Number :"],_orderID];
    
    [orderCell.itemImageView sd_setImageWithURL:[NSURL URLWithString:[itemImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    orderCell.itemNameLabel.text = [itemNameArray objectAtIndex:indexPath.row];
    orderCell.categoryNameLabel.text = [categoryNameArray objectAtIndex:indexPath.row];
    orderCell.placedOnDateLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On:"],[placedDateArray objectAtIndex:indexPath.row]];
    orderCell.orderIdLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order id"],[orderIdArray objectAtIndex:indexPath.row]];
    orderCell.totalAmountLabel.text = [totalAmountArray objectAtIndex:indexPath.row];
    orderCell.statusInfoLabel.text = [statusInfoArray objectAtIndex:indexPath.row];
    orderCell.totalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Total"];
    orderCell.statusLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Status:"];
    
    [orderCell.viewDetailsBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"View Details"] forState:UIControlStateNormal];
    
    [orderCell.trackOrder_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Track Order"] forState:UIControlStateNormal];
    
    
    [orderCell.viewDetailsBtn addTarget:self action:@selector(viewDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    [orderCell.trackOrder_Outlet addTarget:self action:@selector(viewTrackOrder:) forControlEvents:UIControlEventTouchUpInside];
    
    [orderCell.callBtn_Outlet addTarget:self action:@selector(callToRestaurant:) forControlEvents:UIControlEventTouchUpInside];
    
    return orderCell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 172;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    TrackOrder *trackOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
//    
//    [trackOrderVC trackOrderServiceCall:[orderIdArray objectAtIndex:indexPath.row]];
//    [self.navigationController pushViewController:trackOrderVC animated:TRUE];
    
   // [self performSegueWithIdentifier:@"track" sender:self];

}

-(void)orderHistoryServiceCall{

//http://voliveafrica.com/togo/api/services/order_history?API-KEY=225143&user_id=3208428429&lang=en
languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
[SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
[SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
[SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];

NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
[orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
[orderHistoryPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
[orderHistoryPostDictionary setObject:languageString forKey:@"lang"];


[[sharedclass sharedInstance]fetchResponseforParameter:@"order_history?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
    
    NSDictionary *dict = (NSDictionary *)dataFromJson;
    
    NSLog(@"Images from server %@", dict);
    NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        placedDateArray = [NSMutableArray new];
        orderIdArray = [NSMutableArray new];
        itemImageArray = [NSMutableArray new];
        itemNameArray = [NSMutableArray new];
        categoryNameArray = [NSMutableArray new];
        totalAmountArray = [NSMutableArray new];
        statusInfoArray = [NSMutableArray new];
        mobileNumberArray = [NSMutableArray new];
        
    if ([status isEqualToString:@"1"])
    {
            
        [SVProgressHUD dismiss];
        NSArray *array = [dict objectForKey:@"order_history"];
        for(NSDictionary *mainData in array){
                
            [placedDateArray addObject:[NSString stringWithFormat:@"%@",[mainData objectForKey:@"placed_on"]]];
            [orderIdArray addObject:[NSString stringWithFormat:@"%@",[mainData objectForKey:@"order_id"]]];
            [itemImageArray addObject:[mainData objectForKey:@"logo"]];
            [itemNameArray addObject:[mainData objectForKey:@"shop_name"]];
            [categoryNameArray addObject:[mainData objectForKey:@"shop_category"]];
            [totalAmountArray addObject:[NSString stringWithFormat:@"%@",[mainData objectForKey:@"net_amount"]]];
            [statusInfoArray addObject:[NSString stringWithFormat:@"%@",[mainData objectForKey:@"status"]]];
            [mobileNumberArray addObject:[NSString stringWithFormat:@"%@",[mainData objectForKey:@"mobile"]]];
                
            }
            
            [_orderHistoryTableView reloadData];
            
        }
        else
        {
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"order_history"]] onViewController:self completion:nil];
            });

            
        }
    });
    
}];
}


- (IBAction)orderBack:(id)sender {
    
        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:home animated:TRUE];

}

@end
