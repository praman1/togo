//
//  Selectlocation.m
//  TO GO
//
//  Created by Volive solutions on 10/31/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Selectlocation.h"
#import "SelectLocationCollectionViewCell.h"
#import "sharedclass.h"
#import "SWRevealViewController.h"
#import "Home.h"
#import "AppDelegate.h"

@interface Selectlocation ()<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,GMSAutocompleteViewControllerDelegate>{
    
    CLLocationManager *locationManager;
    NSString *latitude,*langitude,*addressString1,*addressString2,*addressString3,*addressString4;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UICollectionView *mycollectionview;

@end

@implementation Selectlocation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate  = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [locationManager requestWhenInUseAuthorization];
    // Do any additional setup after loading the view.
    
    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Select Location"];
    [_detectlocationBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@" Detect my location"] forState:UIControlStateNormal];
    
    _locationSearchBar.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Location Search"];;
    
    
   // self.navigationController.navigationBar.backItem = ];
    //self..image = [UIImage 
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.leftBarButtonItem = anotherButton;
       
    //[[UIBarButtonItem appearance] ];
    
    _detectlocationBtn.layer.cornerRadius = 5;
    _detectlocationBtn.layer.masksToBounds = YES;
    _detectlocationBtn.layer.borderWidth = 1.5;
    _detectlocationBtn.layer.borderColor = [UIColor colorWithRed:(235/255.0) green:(28/255.0) blue:(36/255.0) alpha:1].CGColor;
   
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([delegate.checkStatusString isEqualToString:@"API"]) {
        
        [[sharedclass sharedInstance]showalrtfor:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Location Detected"] inViewCtrl:self];
    }
    
}

-(void)back:(UIButton *)sender{
    delegate.checkStatusString = @"";
 [self.navigationController popViewControllerAnimated :YES];
}

#pragma mark - collectionview methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return 6;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SelectLocationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 5;
    
    return cell;

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    return CGSizeMake((_mycollectionview.frame.size.width/3)-10,40);

}

#pragma mark - tableviewmethods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = @"AI-Quwaysimah";
    return cell;
}

- (IBAction)location_BTN:(id)sender {
    
    locationManager = [[CLLocationManager alloc]init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    locationManager.distanceFilter = 200;
    [locationManager requestWhenInUseAuthorization];

    locationManager.pausesLocationUpdatesAutomatically = NO;
    [locationManager startUpdatingLocation];
    
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Error"]
                                
                                                                   message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Failed to get your location"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         
                                                         
                                                         
                                                     }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:^{
    }];

}

#pragma mark DidUpdateToLocation
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        langitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [[NSUserDefaults standardUserDefaults]setObject:langitude forKey:@"longitude"];
        [[NSUserDefaults standardUserDefaults]setObject:latitude forKey:@"latitude"];
        NSLog(@"My Langitude Value Is %@",langitude);
        NSLog(@"My Latitude Value Is %@",latitude);
    }
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
      
        if (error == nil && [placemarks count] > 0) {
          

            placemark = [placemarks firstObject];
            addressString1 = [NSString stringWithFormat:@"%@",placemark.locality];
            addressString2 = [NSString stringWithFormat:@"%@",placemark.subLocality];
            addressString3 = [NSString stringWithFormat:@"%@",placemark.subAdministrativeArea];
            addressString4 = [NSString stringWithFormat:@"%@",placemark.name];
           // NSString *addressString5 = [NSString stringWithFormat:@"%@",placemark.];
            
            [[NSUserDefaults standardUserDefaults]setObject:addressString1 forKey:@"cityName"];
            NSLog(@"Address String Is Locality %@",addressString1);
            NSLog(@"Address String Is subLocality %@",addressString2);
            NSLog(@"Address String Is subAdministrativeArea %@",addressString3);
            
            [[sharedclass sharedInstance]showalrtfor:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Location Detected"] inViewCtrl:self];
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    }];
}



// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    // Do something with the selected place.
    
     delegate.checkStatusString = @"API";
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    
    NSLog(@"Place attributions %@", place.addressComponents);
   // NSArray * addressComponents = place.addressComponents;
    
    for ( GMSAddressComponent * component in place.addressComponents) {
        if ( [component.type  isEqual: @"locality"] )
        {
            NSLog(@"Current city %@ ", component.name);
            NSString *cityName = component.name;
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",cityName] forKey:@"cityName"];
        }
    }
    
    NSLog(@"Latitude %f",place.coordinate.latitude);
    NSLog(@"Longitude %f",place.coordinate.longitude);
    
    langitude = [NSString stringWithFormat:@"%.8f", place.coordinate.longitude];
    latitude = [NSString stringWithFormat:@"%.8f", place.coordinate.latitude];
    
    [[NSUserDefaults standardUserDefaults]setObject:langitude forKey:@"longitude"];
    [[NSUserDefaults standardUserDefaults]setObject:latitude forKey:@"latitude"];
    
    [[sharedclass sharedInstance]showalrtfor:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Location Detected"] inViewCtrl:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (IBAction)search_BTN:(id)sender {
    
   
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    UIColor *darkGray = [UIColor darkGrayColor];
    UIColor *lightGray = [UIColor lightGrayColor];
    
    acController.secondaryTextColor = [UIColor colorWithWhite:1.0f alpha:0.5f];
    acController.primaryTextColor = lightGray;
    acController.primaryTextHighlightColor = [UIColor grayColor];
    acController.tableCellBackgroundColor = darkGray;
    acController.tableCellSeparatorColor = lightGray;
    acController.tintColor = lightGray;
    [self presentViewController:acController animated:YES completion:nil];

}

@end
