//
//  Discover.h
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface Discover : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UISearchBar *search;
@property (weak, nonatomic) IBOutlet UICollectionView *discoverCollectionView;
@property (weak, nonatomic) IBOutlet UIView *discoverMapView;
@property (weak, nonatomic) IBOutlet UILabel *discoverLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewBestSellersBtn_Outlet;

@end
