//
//  SuggestRestaurant.h
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SuggestRestaurant : UIViewController
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;

@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *number;
@property (weak, nonatomic) IBOutlet UITextField *contact;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
- (IBAction)customer_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *customerBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIImageView *customerCheckImage;
- (IBAction)restaurant_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *restaurentBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantCheckImage;
- (IBAction)send_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *suggestMapView;
@property (weak, nonatomic) IBOutlet UIScrollView *suggestScrollView;
@property (weak, nonatomic) IBOutlet UIView *suggestView;
@property (weak, nonatomic) IBOutlet UILabel *wantARestaurantLabel;
@property (weak, nonatomic) IBOutlet UILabel *addTheDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *areYouLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantLabel;
@property (weak, nonatomic) IBOutlet UILabel *rstrntNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn_Outlet;

@end
