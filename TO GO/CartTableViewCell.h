//
//  CartTableViewCell.h
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"
@interface CartTableViewCell : UITableViewCell
//cell1
@property (weak, nonatomic) IBOutlet  ANStepperView * qtyview;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *flavourNameLabel;

//cell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *logoImg;

//cell3

@property (weak, nonatomic) IBOutlet UILabel *totalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalValueLabel;

@property (weak, nonatomic) IBOutlet UIView *cartMapView;
@property (weak, nonatomic) IBOutlet UIButton *updateBtn;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourOrderIdLabel;

//cell4
@property (weak, nonatomic) IBOutlet UILabel *cardHolderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *cardBtn_Outlet;

//cell5
    @property (weak, nonatomic) IBOutlet UILabel *addNewCardLabel;
    

//cell6
    @property (weak, nonatomic) IBOutlet UILabel *CODLabel;
    @property (weak, nonatomic) IBOutlet UILabel *payByCashLabel;
    @property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
    @property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;
    @property (weak, nonatomic) IBOutlet UITextField *carNumberTF;
    @property (weak, nonatomic) IBOutlet UITextField *carTypeTF;
    
@property (weak, nonatomic) IBOutlet UITextField *payByCashTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *payByCashBtn_Outlet;

@end
