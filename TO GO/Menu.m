//
//  Menu.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Menu.h"
#import "MenuTableViewCell.h"
#import "Login.h"
#import "sharedclass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AppDelegate.h"
@interface Menu ()<UITableViewDataSource,UITableViewDelegate>{

    //NSArray *menulist;
    AppDelegate * appDelegate;
    NSString *languageString;
    NSArray *menuImageList;
    NSMutableArray *menuNameArray;
    NSMutableArray *menuImageArray;
}

@end

@implementation Menu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    
    // Do any additional setup after loading the view.
    _userProfileImageView.layer.cornerRadius = _userProfileImageView.frame.size.width/2;
    _userProfileImageView.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData) name:@"refreshData" object:nil];
    
   // menulist = @[@"Home",@"Order History",@"Notifications",@"Discover",@"Suggest Restaurant",@"",@"My Profile",@"Settings",@"Sign Out"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshMenu) name:@"refreshMenu" object:nil];
    
    
}

-(void)reloadData
{
    _userNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    _userEmailIdLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
   // _userProfileImageView.image = [[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"];
    [_userProfileImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"menu profile"]];
    
}

-(void)refreshMenu
{
    if ([languageString isEqualToString:@"ar"]) {
        
        menuNameArray = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Discover"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Suggest Restaurant"],[[sharedclass sharedInstance]languageSelectedStringForKey:@""],[[sharedclass sharedInstance]languageSelectedStringForKey:@"My Profile"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign Out"], nil];
    }else{
        
        menuNameArray = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Discover"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Suggest Restaurant"],[[sharedclass sharedInstance]languageSelectedStringForKey:@""],[[sharedclass sharedInstance]languageSelectedStringForKey:@"My Profile"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign Out"], nil];
        
        
    }
    [_menuTableView reloadData];
    
    menuImageArray = [[NSMutableArray alloc]initWithObjects:@"Home",@"Order History",@"Notifications",@"Discover",@"Suggest Restaurant",@"",@"My Profile",@"Change Language",@"Sign Out", nil];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    
    
    if ([languageString isEqualToString:@"ar"]) {
        
        menuNameArray = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Discover"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Suggest Restaurant"],[[sharedclass sharedInstance]languageSelectedStringForKey:@""],[[sharedclass sharedInstance]languageSelectedStringForKey:@"My Profile"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign Out"], nil];
    }else{
        
        menuNameArray = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Discover"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Suggest Restaurant"],[[sharedclass sharedInstance]languageSelectedStringForKey:@""],[[sharedclass sharedInstance]languageSelectedStringForKey:@"My Profile"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign Out"], nil];

        
    }
    [_menuTableView reloadData];
    
    menuImageArray = [[NSMutableArray alloc]initWithObjects:@"Home",@"Order History",@"Notifications",@"Discover",@"Suggest Restaurant",@"",@"My Profile",@"Change Language",@"Sign Out", nil];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    _userNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    _userEmailIdLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
    //_userProfileImageView.image = [[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"];
    [_userProfileImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"menu profile"]];
    
        
        //[self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
        
        [UIView animateWithDuration:0 animations:^{
            
            self.userProfileImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.userProfileImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
    });

}

-(void)viewWillDisappear:(BOOL)animated{
self.navigationController.navigationBar.hidden = NO;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return menuNameArray.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuTableViewCell *cell ;
    if (![menuNameArray[indexPath.row] isEqualToString:@""]) {
        
        
            cell= [tableView dequeueReusableCellWithIdentifier:@"cell"];
            
            cell.menuimg.image =[UIImage imageNamed:menuImageArray[indexPath.row]];
            cell.menutitle.text = [NSString stringWithFormat:@"%@",menuNameArray[indexPath.row]];
    }
    else{
        
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
    }
        return cell;

}

-(void)languageChange
{
    

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        
        [self performSegueWithIdentifier:@"Home" sender:self];
    }else if(indexPath.row == 1){
        
        [self performSegueWithIdentifier:@"orders" sender:self];
    
    }else if (indexPath.row == 2){
        
     [self performSegueWithIdentifier:@"notification" sender:self];
    
    }else if (indexPath.row == 3){
        
        [self performSegueWithIdentifier:@"Discover" sender:self];
    }else if(indexPath.row == 4){
        
        [self performSegueWithIdentifier:@"menuTosuggestions" sender:self];
    }
        else if (indexPath.row == 6){
            
        [self performSegueWithIdentifier:@"profile" sender:self];

        
        } else if (indexPath.row == 7){
            
//            appDelegate.side=@"side";
//            [self performSegueWithIdentifier:@"sideMenu" sender:self];
            
            //[self languageChange];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
            
            //***Small
            UIAlertAction *english = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"English"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"en" forKey:@"language"];
                
                [self viewDidLoad];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu" object:nil];
                
                appDelegate.langStr =@"en";
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu1" object:nil];
                
                
            }];
            
            
            UIAlertAction *arabic = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"عربى"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"ar" forKey:@"language"];
                [self viewDidLoad];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu" object:nil];
                
                 appDelegate.langStr =@"ar";
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu1" object:nil];
                
            }];
            //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:english];
            [alertController addAction:arabic];
            
            [alertController addAction:cancel];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
        }else if(indexPath.row == 8){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Logout"]
                                     message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Are you sure to Logout?"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Yes"]
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                        appDelegate.side=@"side1";
                                        
//                                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                                        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
//                                        window.rootViewController = [storyboard instantiateInitialViewController];
                                        
                                        UIViewController * gotoMainNav = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavVC"];
                                        [[UIApplication sharedApplication] delegate].window.rootViewController = gotoMainNav;
                                        [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                                        
                                        
                                    }];
        
        UIAlertAction* cancel = [UIAlertAction
                                   actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        
        
        //Add your buttons to alert controller
                [alert addAction:cancel];
                [alert addAction:yesButton];

        
        [self presentViewController:alert animated:YES completion:nil];
     
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
