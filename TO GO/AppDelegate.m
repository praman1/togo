//
//  AppDelegate.m
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/SignIn.h>
#import "sharedclass.h"
#import "CartScreen.h"
#import "TrackOrder.h"
#import "HeaderAppConstant.h"
#import "CartTableViewCell.h"
#import "Home.h"
#import "SWRevealViewController.h"


@interface AppDelegate (){
    
    CartTableViewCell *cell;
}
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@end

@implementation AppDelegate

static NSString * const kClientID =
@"567140786541-3reqsf2b54nlenv1adcu6ha6d8c7couo.apps.googleusercontent.com";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    //[GIDSignIn sharedInstance].delegate = self;
    [self registerForRemoteNotifications];
    [GMSServices provideAPIKey:@"AIzaSyBrcc0x2qko_9gX1bEhDPi7ZoKo3tC9_VY"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyBrcc0x2qko_9gX1bEhDPi7ZoKo3tC9_VY"];
    
    [NSThread sleepForTimeInterval:2.0];
    
    // [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"datacalling" object:nil];
    
    return YES;
}

-(void)refresh
{
    
    if ([_statusString isEqualToString:@"1"]) {
        
        [self postService];
        
        
    } else if ([_statusString isEqualToString:@"5"]){
        
        [self orderStatusServiceCall];
        
    }

}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
   
     [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *lastName = user.profile.givenName;
    NSString *firstName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    //  NSDictionary * dict=
    
    NSLog(@"user Id %@",userId);
    NSLog(@"device Id %@",idToken);
    NSLog(@"full name %@",fullName);
    NSLog(@"Last Name %@",lastName);
    NSLog(@"First Name %@",firstName);
    NSLog(@"email id %@",email);
    
    //NSString *valueToSave = @"someValue";

    [[NSUserDefaults standardUserDefaults] setObject:fullName forKey:@"fullName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {

    
    BOOL handled;
    
    if ([self.socialURLSelection isEqualToString:@"fb"]) {
        
        handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                 openURL:url
                                                       sourceApplication:sourceApplication
                                                              annotation:annotation];

        
    } else {
        handled = [[GIDSignIn sharedInstance] handleURL:url
                                      sourceApplication:sourceApplication
                                             annotation:annotation];
    }
    // Add any custom logic here.
    return handled;
} 



- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *tokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSLog(@"DeviceToken: %@", tokenString);
    
    [[NSUserDefaults standardUserDefaults]setObject:tokenString forKey:@"deviceToken"];
    
    
}

- (void)registerForRemoteNotifications {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
        
    }
    
    else {
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // iOS 10 will handle notifications through other methods
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))      {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    
    
    else
    {
        NSLog( @"FOREGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    
}
//push notification delegate methods
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification  withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    
    NSLog( @"for handling push in foreground" );
    
    [self.progTimer invalidate];
    
    [SVProgressHUD dismiss];
    //_paymentStr=@"payment";

    
    NSDictionary * alertDetails = [[NSDictionary alloc]initWithDictionary:notification.request.content.userInfo];
    NSLog(@"Push Notification Details %@", alertDetails);
    
    _paymentStr = [[alertDetails objectForKey:@"aps"]objectForKey:@"alert"];
    _statusString = [NSString stringWithFormat:@"%@",[[[[alertDetails objectForKey:@"aps"]objectForKey:@"info"] objectForKey:@"message"] objectForKey:@"status"]];
    NSLog(@"Notification Status Is %@",_statusString);
    // Your code
    NSLog(@"%@", notification.request.content.userInfo); //for getting response payload data

   // [[NSNotificationCenter defaultCenter]postNotificationName:@"datacalling" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reload" object:nil];
     if ([_statusString isEqualToString:@"1"]) {
        
        [self postService];
        
    }else if ([_statusString isEqualToString:@"5"]){
        
        [self orderStatusServiceCall];
    }
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response  withCompletionHandler:(void (^)())completionHandler   {
    NSLog( @"for handling push in background" );
    // Your code
    [self.progTimer invalidate];

    [SVProgressHUD dismiss];
   // _paymentStr=@"payment";
    
    NSDictionary * alertDetails = [[NSDictionary alloc]initWithDictionary:response.notification.request.content.userInfo];
    NSLog(@"Push Notification Details %@", alertDetails);
    _paymentStr = [[alertDetails objectForKey:@"aps"]objectForKey:@"alert"];
    
    _statusString = [NSString stringWithFormat:@"%@",[[[[alertDetails objectForKey:@"aps"]objectForKey:@"info"] objectForKey:@"message"] objectForKey:@"status"]];
    
    NSLog(@"Notification Status Is %@",_statusString);
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"datacalling" object:nil];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reload" object:nil];
    
    if ([_statusString isEqualToString:@"1"]) {
        
        [self postService];
        
    }else if ([_statusString isEqualToString:@"5"]){
        
        [self orderStatusServiceCall];
    }

    
   }

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"TO_GO"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

-(void)postService{
    
    //     API-KEY:225143
    //     card_id:5
    //     car_number:apo72252
    //     car_type:dfgah
    //     payment_type:online
    //     user_id:2856066910
    //     vendor_id:3288062319
    //     order_id:5530161372713
    //     pay_by_cash:2010
    
    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc]init];
    
    [paymentDictionary setObject:APIKEY forKey:@"API-KEY"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cardId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"cardId"] :@"" forKey:@"card_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"carNumber"] forKey:@"car_number"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"carType"] forKey:@"car_type"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"CODString"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"CODString"]:@"" forKey:@"payment_type"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"] forKey:@"vendor_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"orderId"] forKey:@"order_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"totalStr"] forKey:@"tax_amount"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"grandTotalString"] forKey:@"grand_total"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] forKey:@"lang"];
    
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"PBCStr"] forKey:@"pay_by_cash"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];

    
    [[sharedclass sharedInstance]urlPerameterforPost:@"payment" withPostDict:paymentDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
         dispatch_async(dispatch_get_main_queue(), ^{
             
        if ([status isEqualToString:@"1"])
        {
             [SVProgressHUD dismiss];
            _spCheckString = @"customer";
            dispatch_async(dispatch_get_main_queue(), ^{
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            
            TrackOrder *controller = (TrackOrder*)[mainStoryboard instantiateViewControllerWithIdentifier: @"TrackOrder"];
            controller.imageStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"logo"];
            controller.nameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"shopName"];
            controller.checkString = _spCheckString;
            [controller trackOrderServiceCall:[[NSUserDefaults standardUserDefaults]objectForKey:@"orderId"]];
            
            UINavigationController *navigate = [[UINavigationController alloc]initWithRootViewController:controller];
            navigate.navigationBar.barTintColor = [UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self];
                
            [self.window.rootViewController presentViewController:navigate animated:TRUE completion:nil];
            });
        }
        else
        {
            [SVProgressHUD dismiss];
            
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
//            
//             [alert addAction:ok];
//            
//            [self.window.rootViewController presentViewController:alert animated:TRUE completion:nil];
        }
         });
        
    }];
}


-(void)orderStatusServiceCall{
    
    
   // languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/togo/api/services/order_expired?API-KEY=225143&lang=ar&order_id=5273323857251
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"orderId"] forKey:@"order_id"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] forKey:@"lang"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"totalStr"] forKey:@"tax_amount"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"grandTotalString"] forKey:@"grand_total"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"order_expired?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                SWRevealViewController *home = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SWRevealViewController"];
                     
                     [[NSNotificationCenter defaultCenter] removeObserver:self];
                //[self.window.rootViewController presentViewController:home animated:TRUE completion:nil];
                     self.window.rootViewController = home;
                     
                 });
                
            }
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
        });
        
    }];
    
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
