//
//  SPOrderHistoryViewController.m
//  TO GO
//
//  Created by volive solutions on 23/02/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "SPOrderHistoryViewController.h"
#import "OrderHistoryTableViewCell.h"
#import "SWRevealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "UIViewController+ENPopUp.h"
#import "TrackOrder.h"
#import "OrderViewDetailsController.h"


@interface SPOrderHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    OrderHistoryTableViewCell *cell;
    
    NSMutableArray *placedDateArray;
    NSMutableArray *orderIdArray;
    NSMutableArray *itemImageArray;
    NSMutableArray *itemNameArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *totalAmountArray;
    NSMutableArray *statusInfoArray;
    NSMutableArray *mobileNumberArray;
    NSMutableArray *ordersCountArray;
    
    NSString *imageString;
    NSString *shopNameString;
    
    NSString *languageString;
    
    OrderViewDetailsController * order;

}

@end

@implementation SPOrderHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"];
    
//    UINavigationBar *bar = [self.navigationController navigationBar];
//    [bar setTintColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    
    [self orderHistoryServicecall];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{

    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated {

//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [super viewWillDisappear:animated];
}


#pragma mark Tableview Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return ordersCountArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell = [_SPOrderHistoryTableview dequeueReusableCellWithIdentifier:@"orderCell"];
    
    if (cell == nil) {
        
        cell = [[OrderHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderCell"];
    }
 
    [cell.itemImageView sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    cell.itemNameLabel.text = [NSString stringWithFormat:@"%@",shopNameString ? shopNameString :@""];
    cell.categoryNameLabel.text = [categoryNameArray objectAtIndex:indexPath.row];
    cell.placedOnDateLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On:"],[placedDateArray objectAtIndex:indexPath.row]];
    cell.orderIdLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order id:"],[orderIdArray objectAtIndex:indexPath.row]];
    cell.totalAmountLabel.text =[NSString stringWithFormat:@"%@SAR",[totalAmountArray objectAtIndex:indexPath.row]];
    cell.statusInfoLabel.text = [statusInfoArray objectAtIndex:indexPath.row];
    cell.totalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Total"];
    cell.statusLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Status:"];
    
    [cell.viewDetailsBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"View Details"] forState:UIControlStateNormal];
    
    [cell.trackOrder_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Track Order"] forState:UIControlStateNormal];
    
    
    [cell.viewDetailsBtn addTarget:self action:@selector(viewDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.trackOrder_Outlet addTarget:self action:@selector(viewTrackOrder:) forControlEvents:UIControlEventTouchUpInside];
    
   // [cell.callBtn_Outlet addTarget:self action:@selector(callToRestaurant:) forControlEvents:UIControlEventTouchUpInside];

   
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 172;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark OrderHistory Servicecall
-(void)orderHistoryServicecall{
  
    //http://voliveafrica.com/togo/api/services/confirmation_orders?API-KEY=225143&service_provider_id=2620161185&lang=en
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
    [orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [orderHistoryPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"service_provider_id"];
    [orderHistoryPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"confirmation_orders?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            placedDateArray = [NSMutableArray new];
            orderIdArray = [NSMutableArray new];
            itemImageArray = [NSMutableArray new];
            itemNameArray = [NSMutableArray new];
            categoryNameArray = [NSMutableArray new];
            totalAmountArray = [NSMutableArray new];
            statusInfoArray = [NSMutableArray new];
            mobileNumberArray = [NSMutableArray new];
            ordersCountArray = [NSMutableArray new];
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                shopNameString = [[[dict objectForKey:@"data"]objectForKey:@"shop_details"] objectForKey:@"shop_name"];
                imageString = [[[dict objectForKey:@"data"]objectForKey:@"shop_details"] objectForKey:@"logo"];
                
                ordersCountArray = [[dict objectForKey:@"data"]objectForKey:@"orders"];
                
                if (ordersCountArray.count > 0) {
                    
                    for (int i=0; i<ordersCountArray.count; i++) {
                        
                        [placedDateArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_recived"]]];
                        [orderIdArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_id"]]];
                        [totalAmountArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"total_amount"]]];
                        [statusInfoArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_status"]]];
                        [categoryNameArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"category"]]];
                        
                    }
                }
                
                [_SPOrderHistoryTableview reloadData];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"order_history"]] onViewController:self completion:nil];
                });
                
                
            }
        });
        
    }];

    
}



-(void)viewTrackOrder:(id)sender{
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_SPOrderHistoryTableview];
    NSIndexPath *indexPath = [_SPOrderHistoryTableview indexPathForRowAtPoint:btnPosition];
    
    TrackOrder *trackOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
    
    trackOrderVC.nameStr= shopNameString;
    trackOrderVC.imageStr=imageString;
    
    [trackOrderVC trackOrderServiceCall:[orderIdArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:trackOrderVC animated:TRUE];
    
}

/*
-(void)callToRestaurant:(id)sender{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_SPOrderHistoryTableview];
    NSIndexPath *indexPath = [_SPOrderHistoryTableview indexPathForRowAtPoint:btnPosition];
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",[mobileNumberArray objectAtIndex:indexPath.row]];
    NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
    [SVProgressHUD dismiss];
    //[phoneURL release];
    // [phoneStr release];
    
}
 
*/

-(void)viewDetails:(id)sender
{
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_SPOrderHistoryTableview];
    NSIndexPath *indexPath = [_SPOrderHistoryTableview indexPathForRowAtPoint:btnPosition];
    
    order =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewDetailsController"];
    order.statusString = [statusInfoArray objectAtIndex:indexPath.row];
    order.orderIdString = [orderIdArray objectAtIndex:indexPath.row];
    
    
    order.view.frame = CGRectMake(0, 0, 300.0f, 450.0f);
    
   // [order.closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self presentViewController:order animated:TRUE completion:nil];
   // [self presentPopUpViewController:order];
    
}

//-(void)dismiss
//{
//    
//    [self dismissPopUpViewController];
//    
//}





@end
