//
//  AppDelegate.h
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Google/SignIn.h>
#import <GooglePlaces/GooglePlaces.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,CLLocationManagerDelegate>



@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
-(void)postService;
@property (nonatomic,assign) NSString * latitude;
@property (nonatomic,assign) NSString * longitude;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property NSString *socialURLSelection;

@property NSString * restStr, *side , *paymentStr , *langStr, *spCheckString , *orderStr, *checkStatusString;

@property NSString *statusString;

@property NSTimer *progTimer;

@end

