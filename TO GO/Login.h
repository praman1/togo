//
//  Login.h
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface Login : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *mobileOremailTXT;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UIButton *loginbtn;
@property (weak, nonatomic) IBOutlet UIScrollView *loginScrollView;
- (IBAction)login_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *signInToContinueLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailOrMobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *loginUsingSocialMediaLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn_Outlet;
- (IBAction)facebookLogin_BTN:(id)sender;
- (IBAction)googlePlusLogin_BTN:(id)sender;
- (IBAction)forgotPassword_BTN:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *loginImageview;

@property (strong, nonatomic) IBOutlet FBSDKLoginButton *fbBtn;




@end
