//
//  AddnewCard.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddnewCard : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *numberTxt;

@property (weak, nonatomic) IBOutlet UITextField *expireTxt;
@property (weak, nonatomic) IBOutlet UITextField *CVVTxt;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *numberTF;
@property (weak, nonatomic) IBOutlet UITextField *expiryTF;
@property (weak, nonatomic) IBOutlet UITextField *cvvTF;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
    @property (weak, nonatomic) IBOutlet UILabel *enterDetailsLabel;
    @property (weak, nonatomic) IBOutlet UILabel *nameOnCardLabel;
    @property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
    @property (weak, nonatomic) IBOutlet UILabel *cardExpiryLabel;
    @property (weak, nonatomic) IBOutlet UILabel *cvvLabel;
    @property (weak, nonatomic) IBOutlet UIButton *securelySaveBtn_Outlet;
    @property (weak, nonatomic) IBOutlet UIButton *payNowBtn_Outlet;
    
    
@end
