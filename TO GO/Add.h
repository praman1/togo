//
//  Add.h
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantMenu.h"

@protocol RBFirstVCDelegate <NSObject>

- (void)receiveMessage:(NSMutableArray *)message count:(NSString *)count;

@end

@interface Add : UIViewController

@property (weak, nonatomic) id<RBFirstVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *add;
@property NSString *selected_catID;
@property NSString *itemIdString;
-(void)flavoursArray:(NSArray *)fArr;
@property (weak, nonatomic) IBOutlet UITableView *flavorTbl;
@property (weak, nonatomic) IBOutlet UILabel *flavourLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;

@property NSString *sizeStr;
@property NSString *itemPriceStr;
@property NSString *nameStr;
@property NSString *imageStr;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
