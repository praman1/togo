//
//  Home.h
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface Home : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;
@property NSString *cityNameString;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectLocationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *discoverCollectionView;
- (IBAction)close_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn_Outelt;


@end
