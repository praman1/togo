//
//  TabCollectionViewCell.h
//  TO GO
//
//  Created by volive solutions on 16/12/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UIView *itemView;

@end
