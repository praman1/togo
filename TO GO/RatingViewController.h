//
//  RatingViewController.h
//  TO GO
//
//  Created by volive solutions on 10/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"


@interface RatingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *ratingScrollView;
@property NSString* orderIdString;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIView *ratingVCView;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;
- (IBAction)submit_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *catNameLabel;

@property NSString * nameString,* imageString;

@property (weak, nonatomic) IBOutlet UILabel *goodLabel;
@property (weak, nonatomic) IBOutlet UILabel *complimentLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;


@end
