//
//  Signup.m
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Signup.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "Login.h"
#import "OTPverification.h"

#define SYSTEM_VERSION_LESS_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] ==NSOrderedAscending)
@interface Signup ()
{
    sharedclass *objForshare;
    NSString *tokenString;
    NSString *languageString;
    CLLocationManager *locationManager;
    UIAlertView * alert_location;

}

@end

@implementation Signup

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    objForshare = [[sharedclass alloc] init];
    
    
    locationManager = [[CLLocationManager alloc]init];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    
//    if ([CLLocationManager locationServicesEnabled]){
//        
//        NSLog(@"Location Services Enabled");
//        
//        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
//            
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"App Permission Denied"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"To re-enable, please go to Settings and turn on Location Service for this app."] preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            
//            [alert addAction:ok];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }
    [self LoadCustomDesign];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self checkLocationServicesAndStartUpdates];
    [UIView animateWithDuration:0 animations:^{
        
        self.createAccountBtn.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.signupImageview.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
    }completion:^(BOOL finished){
        
        // Finished scaling down imageview, now resize it
        
        [UIView animateWithDuration:0.4 animations:^{
            
            self.createAccountBtn.transform = CGAffineTransformIdentity;
            self.signupImageview.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            
            
        }];
        
        
    }];

}


-(void) checkLocationServicesAndStartUpdates
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    
    //Checking authorization status
    if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        alert_location = [[UIAlertView alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Please enable the GPS for better results" ]
                                                    message:[[sharedclass sharedInstance]languageSelectedStringForKey:@""]
                                                   delegate:self
                                          cancelButtonTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Settings"]
                                          otherButtonTitles:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"], nil];
        
        //TODO if user has not given permission to device
        if (![CLLocationManager locationServicesEnabled])
        {
            alert_location.tag = 100;
        }
        //TODO if user has not given permission to particular app
        else
        {
            alert_location.tag = 200;
        }
        
        [alert_location show];
        
        return;
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [locationManager startUpdatingLocation];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView == alert_location) {
        if(buttonIndex == 0)//Settings button pressed
        {
            if (alert_location.tag == 100)
            {
                //This will open ios devices location settings
                NSString* url = SYSTEM_VERSION_LESS_THAN(@"10.0") ? @"prefs:root=LOCATION_SERVICES" : @"App-Prefs:root=Privacy&path=LOCATION";
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
            }
            else if (alert_location.tag == 200)
            {
                //This will opne particular app location settings
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
            
            //[self checkLocationServicesAndStartUpdates];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else if(buttonIndex == 1)//Cancel button pressed.
        {
            //TODO for cancel
        }
        
    }
}

-(void)LoadCustomDesign
{
    
        if([languageString isEqualToString:@"ar"]){
    
            _nameTxt.textAlignment = NSTextAlignmentRight;
            _emailTxt.textAlignment = NSTextAlignmentRight;
            _pwdTxt.textAlignment = NSTextAlignmentRight;
            _mobileTxt.textAlignment = NSTextAlignmentRight;
            _carnumTxt.textAlignment = NSTextAlignmentRight;
            _carcolorTxt.textAlignment = NSTextAlignmentRight;

    
        } else  {
    
            _nameTxt.textAlignment = NSTextAlignmentLeft;
            _emailTxt.textAlignment = NSTextAlignmentLeft;
            _pwdTxt.textAlignment = NSTextAlignmentLeft;
            _mobileTxt.textAlignment = NSTextAlignmentLeft;
            _carnumTxt.textAlignment = NSTextAlignmentLeft;
            _carcolorTxt.textAlignment = NSTextAlignmentLeft;
        }
    
    objForshare = [[sharedclass alloc] init];
    
    [objForshare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_emailTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_pwdTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_mobileTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_carnumTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_carcolorTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    
    
    [_createAccountBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Create Account"] forState:UIControlStateNormal];
    [_loginBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Login"] forState:UIControlStateNormal];
    
    _signupToCreateAcntLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Signup to Create Account"];
    _nameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Name"];
    _emailLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Email"];
    _passwordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Password"];
    _mobileNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Mobile Number(with country code(+966))"];
    _carNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Car Number"];
    _carTypeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Car Type"];
     _alreadyHaveAccountLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Already have account?"];
    
    _createAccountBtn.layer.cornerRadius = 4;
    UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark Textfield Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTxt) {
        
        [objForshare textfieldAsLine:_nameTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _emailTxt) {
        
        [objForshare textfieldAsLine:_emailTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _pwdTxt) {
        
        [objForshare textfieldAsLine:_pwdTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _mobileTxt) {
        
        [objForshare textfieldAsLine:_mobileTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _carnumTxt) {
        
        [objForshare textfieldAsLine:_carnumTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _carcolorTxt) {
        
        [objForshare textfieldAsLine:_carcolorTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_signupScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if (textField == _nameTxt) {
        [objForshare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _emailTxt){
        [objForshare textfieldAsLine:_emailTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _pwdTxt){
        [objForshare textfieldAsLine:_pwdTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _mobileTxt){
        [objForshare textfieldAsLine:_mobileTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _carnumTxt){
        [objForshare textfieldAsLine:_carnumTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _carcolorTxt){
        [objForshare textfieldAsLine:_carcolorTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }

    [_nameTxt resignFirstResponder];
    [_emailTxt resignFirstResponder];
    [_pwdTxt resignFirstResponder];
    [_mobileTxt resignFirstResponder];
    [_carnumTxt resignFirstResponder];
    [_carcolorTxt resignFirstResponder];
    
    [_signupScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTxt) {
        
        [_emailTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailTxt) {
        
        [_pwdTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _pwdTxt) {
        
        [_mobileTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _mobileTxt) {
        
        [_carnumTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _carnumTxt) {
        
        [_carcolorTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _carcolorTxt) {
        
        [_carcolorTxt resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}

#pragma mark signUpServiceCall
-(void)signUpServiceCall
{
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/registration";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * registerPostDictionary = [[NSMutableDictionary alloc]init];
    
    [registerPostDictionary setObject:_nameTxt.text forKey:@"name"];
    [registerPostDictionary setObject:_emailTxt.text forKey:@"email"];
    [registerPostDictionary setObject:_pwdTxt.text forKey:@"passwd"];
    [registerPostDictionary setObject:_mobileTxt.text forKey:@"mobile"];
    [registerPostDictionary setObject:_carnumTxt.text forKey:@"car_number"];
    [registerPostDictionary setObject:_carcolorTxt.text forKey:@"car_type"];
    [registerPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
  
    [registerPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [registerPostDictionary setObject:@"ios" forKey:@"device"];
    [registerPostDictionary setObject:languageString forKey:@"lang"];
    

    
    [[sharedclass sharedInstance]urlPerameterforPost:@"registration" withPostDict:registerPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([status isEqualToString:@"1"])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        
                    });
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        OTPverification * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"OTPverification"];
                        otp.mobileNumberStr = _mobileTxt.text;
                        [self.navigationController pushViewController:otp animated:YES];
                        
                    }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                   
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    
                }
            });
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Error"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark createAccount_BTN
- (IBAction)createAccount_BTN:(id)sender {
    [_nameTxt resignFirstResponder];
    [_emailTxt resignFirstResponder];
    [_pwdTxt resignFirstResponder];
    [_mobileTxt resignFirstResponder];
    [_carnumTxt resignFirstResponder];
    [_carcolorTxt resignFirstResponder];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (_nameTxt.text.length < 3 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [_nameTxt becomeFirstResponder]; }];
        
    } else if ([emailTest evaluateWithObject:_emailTxt.text] == NO ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [_emailTxt becomeFirstResponder]; }];
        
    } else if (_pwdTxt.text.length < 5 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [_pwdTxt becomeFirstResponder]; }];
        
    } else if (_mobileTxt.text.length < 5 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [_mobileTxt becomeFirstResponder]; }];
        
    }else if (![_mobileTxt.text containsString:@"+"]) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter Mobile Number with country code(+)"] onViewController:self completion:^{  [_mobileTxt becomeFirstResponder]; }];
        
    } else {
        
        [self signUpServiceCall];
        
    }

}

@end
