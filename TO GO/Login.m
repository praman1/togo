//
//  Login.m
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Login.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "Home.h"
#import "HeaderAppConstant.h"
#import "SWRevealViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
#import "ServiceProviderController.h"

@interface Login ()<FBSDKLoginButtonDelegate,GIDSignInUIDelegate,GIDSignInDelegate>
{
    //sharedclass *objForshare;
    NSString *tokenString,*emailString;
    UIAlertController *alertController;
    NSString *emailIdString,*firstNameString,*lastNameString,*oauthProviderString,*userIdString;
    AppDelegate * appDelegate;
     NSString *languageString;
    NSString *userTypeString;
    
    ServiceProviderController * serviceVC ;
    
}

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    _mobileOremailTXT.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    _passwordTxt.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
       // [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
    
    [self loadCustomDesign];
    
    
    NSLog(@"facebook integration saved in bitbucket");

    firstNameString = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"firstName"];
    
    lastNameString = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"lastName"];
    
    emailIdString = [[NSUserDefaults standardUserDefaults]
                     stringForKey:@"email"];
    
    userIdString = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"userId"];

    
    
}

-(void)loadCustomDesign
{
    
//    CGRect basketTopFrame = self.view.frame;
//    basketTopFrame.origin.x = -basketTopFrame.size.height;
//    
////    CGRect basketBottomFrame = self.lowerview.frame;
////    basketBottomFrame.origin.y = self.view.bounds.size.height;
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.0];
//    [UIView setAnimationDelay:1.0];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//    
//    self.view.frame = basketTopFrame;
//   // self.lowerview.frame = basketBottomFrame;
//    
//    [UIView commitAnimations];
    

    
    
       // int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
        if([languageString isEqualToString:@"ar"]){
    
            _mobileOremailTXT.textAlignment = NSTextAlignmentRight;
            _passwordTxt.textAlignment = NSTextAlignmentRight;
    
        } else  {
    
            _mobileOremailTXT.textAlignment = NSTextAlignmentLeft;
            _passwordTxt.textAlignment = NSTextAlignmentLeft;
        }
    
    
   
    // Do any additional setup after loading the view.
   // objForshare = [[sharedclass alloc] init];
    
    [_loginBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Login"] forState:UIControlStateNormal];
    [_forgotPasswordBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Forgot Password?"] forState:UIControlStateNormal];
    [_signUpBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Signup"] forState:UIControlStateNormal];
    
    _welcomeLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Welcome"];
    _signInToContinueLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Sign in to continue"];
    _emailOrMobileLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Email/Mobile Number(with country code(+966))"];
    _passwordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Password"];
    _loginUsingSocialMediaLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"or login using social media"];
    _userLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"New user?"];
    
    [[sharedclass sharedInstance] textfieldAsLine:_mobileOremailTXT lineColor:[UIColor lightGrayColor] myView:self.view];
    [[sharedclass sharedInstance] textfieldAsLine:_passwordTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    
    _loginBtn_Outlet.layer.cornerRadius = 4;
    
    //self.navigationController.navigationBar.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [UIView animateWithDuration:0 animations:^{
        
        self.loginBtn_Outlet.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.loginImageview.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
    }completion:^(BOOL finished){
        
        // Finished scaling down imageview, now resize it
        
        [UIView animateWithDuration:0.4 animations:^{
            
            self.loginBtn_Outlet.transform = CGAffineTransformIdentity;
            self.loginImageview.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            
            
        }];
        
        
    }];

}

//- (void)viewWillDisappear:(BOOL)animated {
//    
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [super viewWillDisappear:animated];
//}

#pragma mark TextFieldDelegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _mobileOremailTXT) {
        
        [[sharedclass sharedInstance] textfieldAsLine:_mobileOremailTXT lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_loginScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _passwordTxt) {
        
        [[sharedclass sharedInstance] textfieldAsLine:_passwordTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_loginScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_mobileOremailTXT resignFirstResponder];
    [_passwordTxt resignFirstResponder];
    
    [_loginScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _mobileOremailTXT) {
        
        [_passwordTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTxt) {
        
        [_passwordTxt resignFirstResponder];
        
        returnValue = YES;
        
    }
    return returnValue;
}

#pragma mark loginServiceCall
-(void)loginServiceCall
{
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * loginPostDictionary = [[NSMutableDictionary alloc]init];
    
    [loginPostDictionary setObject:_mobileOremailTXT.text forKey:@"email"];
    [loginPostDictionary setObject:_passwordTxt.text forKey:@"passwd"];
    [loginPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
   // [loginPostDictionary setObject:@"fkdjh72364d72346ndri" forKey:@"token"];
    [loginPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [loginPostDictionary setObject:@"ios" forKey:@"device"];
    [loginPostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"login" withPostDict:loginPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:_mobileOremailTXT.text forKey:@"userName"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [[NSUserDefaults standardUserDefaults]setObject:_passwordTxt.text forKey:@"password"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"user_id"] forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"car_number"] forKey:@"carNumber"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"car_type"] forKey:@"carType"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"image"] forKey:@"profileImage"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"name"] forKey:@"name"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"email"] forKey:@"emailId"];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                    
                    userTypeString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"auth_level"]];
                    
                    
                    if ([userTypeString isEqualToString:@"6"]) {
                        
                        
                    serviceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceProviderController"];
                        //serviceVC.navigateString = @"spStr";
                        //[[NSNotificationCenter defaultCenter]removeObserver:self];
                        self.navigationController.navigationBar.hidden = NO;
                        
                        [self.navigationController pushViewController:serviceVC animated:TRUE];
                        
                       
                    }else{
                        
                        // [[NSNotificationCenter defaultCenter]removeObserver:self];
                        SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                        
                        self.navigationController.navigationBar.hidden = YES;
                        
                        [self.navigationController pushViewController:reveal animated:TRUE];
                        
                    }
                    

                    //[[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:^{}];
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


#pragma mark LoginButton
- (IBAction)login_BTN:(id)sender {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
   if (_passwordTxt.text.length < 5 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [_passwordTxt becomeFirstResponder]; }];
    }
    else{
        if ([[sharedclass sharedInstance]isNetworkAvalible] == YES) {
            
             [self loginServiceCall];
        }
        
        else{
            
            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please check your internet connection"] onViewController:self completion:nil];
        }
        
    }

}

#pragma mark forgotPasswordServiceCall
-(void)forgotPasswordServiceCall
{
    alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"]
                                                          message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter your registered Email-ID"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter Email-ID"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = alertController.textFields;
        
        UITextField * namefield = textfields[0];
        NSLog(@"jfhgjdjgd %@",namefield);
        
        emailString=namefield.text;
        // http://voliveafrica.com/togo/api/services/forgot?API-KEY=225143&email=testcase1@yopmail.com
        
        NSMutableDictionary * forgotPWPostDictionary = [[NSMutableDictionary alloc]init];
        [forgotPWPostDictionary setObject:APIKEY forKey:@"API-KEY"];
        [forgotPWPostDictionary setObject:emailString forKey:@"email"];
        [forgotPWPostDictionary setObject:languageString forKey:@"lang"];
        
        
        [[sharedclass sharedInstance]fetchResponseforParameter:@"forgot?" withPostDict:forgotPWPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@",dataDictionary);
            [SVProgressHUD dismiss];
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"successfully sent to emailid ");
                    
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dataDictionary objectForKey:@"message"] onViewController:self completion:nil];
                });
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dataDictionary objectForKey:@"message"] onViewController:self completion:nil];
                });
            }
        }];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark Facebook Login Service Call
-(void)facebookLoginServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
//    API-KEY : 225143
//    email : testcase13@yopmail.com
//    first_name : test
//    last_name : test
//    device : IOS
//    token : 658465dsfersdgg
//    oauth_provider : facebook
    
    NSMutableDictionary * facebookPostDictionary = [[NSMutableDictionary alloc]init];
    
    [facebookPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [facebookPostDictionary setObject:emailIdString forKey:@"email"];
    [facebookPostDictionary setObject:firstNameString forKey:@"first_name"];
    [facebookPostDictionary setObject:lastNameString forKey:@"last_name"];
    [facebookPostDictionary setObject:@"ios" forKey:@"device"];
    [facebookPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
   // [facebookPostDictionary setObject:@"fkdjh72364gfdgd72346ndri" forKey:@"token"];
    [facebookPostDictionary setObject:@"facebook" forKey:@"oauth_provider"];
    [facebookPostDictionary setObject:languageString forKey:@"lang"];
    
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"social" withPostDict:facebookPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"name"] forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"email"] forKey:@"emailId"];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];

                        
                        SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                        
                        [self.navigationController pushViewController:reveal animated:TRUE];
                        
                        
                    });
                    
//                   [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:^{ }];
//                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];
 }



#pragma mark Google+ Login Service Call
-(void)googlePlusLoginServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];

    NSMutableDictionary * googlePostDictionary = [[NSMutableDictionary alloc]init];
    
    [googlePostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [googlePostDictionary setObject:emailIdString forKey:@"email"];
    [googlePostDictionary setObject:firstNameString forKey:@"first_name"];
    [googlePostDictionary setObject:lastNameString forKey:@"last_name"];
    [googlePostDictionary setObject:@"ios" forKey:@"device"];
     [googlePostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
    //[facebookPostDictionary setObject:@"fkdjh72364gfdgd72346ndri" forKey:@"token"];
    [googlePostDictionary setObject:@"google" forKey:@"oauth_provider"];
    [googlePostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"social" withPostDict:googlePostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([status isEqualToString:@"1"])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                     
                       // [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"data"] forKey:@"name" ]];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"] objectForKey:@"name"] forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"] objectForKey:@"email"] forKey:@"emailId"];
                        
                       // [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"data"] forKey:@"emailId"];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];

                        
                        
                        SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                        
                        [self.navigationController pushViewController:reveal animated:TRUE];
 
                        
                    });
                    
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                }
            });
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:^{ }];
            });
        }
    }];
}

#pragma mark facebookLogin_BTN
- (IBAction)facebookLogin_BTN:(id)sender {
    
    appDelegate.socialURLSelection = @"fb";
       //[self fbLogin];
    
     FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult* result, NSError* error) {
         if (error) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                // [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Error"]  withMessage:[NSString stringWithFormat:@"%@",error.localizedDescription] onViewController:self completion:^{}];
                 
             });
             
         } else if (result.isCancelled) {
             
             
         } else {
             
             if ([FBSDKAccessToken currentAccessToken]) {
                 
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields" : @"id,name,email,first_name,last_name,picture.width(100).height(100)"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection* connection, id result, NSError* error) {
                     
                     if (error) {
                         
                         [SVProgressHUD dismiss];
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
//[[SharedClass sharedInstance]showAlertWithTitle::@"Error"]  withMessage:[NSString stringWithFormat:@"%@",error.localizedDescription] onViewController:self completion:^{}];
                             
                         });
                         
                     } else {
                         
                         NSLog(@"user info %@",result);
                         NSString * userImage;
                         
                         userIdString=[NSString stringWithFormat:@"%@",[result objectForKey:@"id"]];
                         firstNameString=[NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]];
                         lastNameString=[NSString stringWithFormat:@"%@",[result objectForKey:@"last_name"]];
                         emailIdString=[NSString stringWithFormat:@"%@",[result objectForKey:@"email"]];
                         
                         
                          
                        [[NSUserDefaults standardUserDefaults]setObject:firstNameString forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults]setObject:emailIdString forKey:@"emailId"];
                         
                         
                         [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                         
                         if (![[[[result objectForKey:@"picture"]objectForKey:@"data"]objectForKey:@"url"]isKindOfClass:[NSNull class]]) {
                             
                             userImage = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"picture"]objectForKey:@"data"]objectForKey:@"url"]];
                             
                             NSLog(@"fbImg:%@",userImage);
                             
                             userImage = [userImage stringByReplacingOccurrencesOfString:@"https://" withString:@""];
                             
                             [[NSUserDefaults standardUserDefaults]setObject:userImage forKey:@"profileImage"];
                             [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                             
                             NSLog(@"fbImg:%@",userImage);
                             
                             
                         } else {
                             
                             userImage = @"";
                             
                         }
                         
                         
                         [self facebookLoginServiceCall];
                         
                         
                     }
                 }];
             }
             
         }
     }];
    
 
}

-(void)fbLogin
{
    _fbBtn = [[FBSDKLoginButton alloc] init];
    _fbBtn.hidden=YES;
    _fbBtn.delegate=self;
    _fbBtn.readPermissions = @[@"public_profile",@"email",@"user_friends"];
    
    [_fbBtn sendActionsForControlEvents: UIControlEventTouchUpInside];
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email,first_name,last_name" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
             
             userIdString=[NSString stringWithFormat:@"%@",[result objectForKey:@"id"]];
             firstNameString=[NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]];
             lastNameString=[NSString stringWithFormat:@"%@",[result objectForKey:@"last_name"]];
             emailIdString=[NSString stringWithFormat:@"%@",[result objectForKey:@"email"]];
             
             
             [[NSUserDefaults standardUserDefaults]setObject:firstNameString forKey:@"name"];
             [[NSUserDefaults standardUserDefaults]setObject:emailIdString forKey:@"emailId"];
             
             [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];

             
             NSLog(@"facebook ID Is %@",userIdString);
             NSLog(@"facebook ID Name Is %@",firstNameString);
             NSLog(@"facebook ID Name Is %@",lastNameString);
             NSLog(@"facebook ID EmailId Is %@",emailIdString);
         }
         
     }];

}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
error:(NSError *)error
{
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [self fbLogin];
        [self facebookLoginServiceCall];
        
        NSLog(@"facebook integration saved in bitbucket");
    }
}

-(BOOL)loginButtonWillLogin:(FBSDKLoginButton *)loginButton
{
    [self toggleHiddenState:YES];
    
    return YES;
}

-(void)toggleHiddenState:(BOOL)shouldHide{
    
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"logout button is pressed here ******* ");
    
}

#pragma googlePlusLogin_BTN
- (IBAction)googlePlusLogin_BTN:(id)sender {
    
        [GIDSignIn sharedInstance].delegate = self;
        [GIDSignIn sharedInstance].uiDelegate = self;
        [[GIDSignIn sharedInstance] signIn];
 
        appDelegate.socialURLSelection = @"google";
    
    
   // [self googleLogin];
    
    
}


// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void)signIn:(GIDSignIn* )signIn didSignInForUser:(GIDGoogleUser* )user
    withError:(NSError *)error {
    
    if (error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Error"] withMessage:[NSString stringWithFormat:@"%@",error.localizedDescription] onViewController:self completion:^{}];
        });
        
        
    } else {
        
        
        //[appDelegate removeLoader];
        [SVProgressHUD dismiss];
        
        NSString * userImage;
        
        NSLog(@"user info google %@",user);
        
        NSLog(@"user id %@ user email %@ user name %@ user gemder %@",user.userID,user.profile.email,user.profile.name,user.profile.givenName);
        
            emailIdString = user.profile.email;
            firstNameString = user.profile.familyName;
            lastNameString = user.profile.givenName;
        
        if ( [user.profile hasImage]) {
            
            userImage = [NSString stringWithFormat:@"%@",[user.profile imageURLWithDimension:100]];
            
            NSData *nsdata = [userImage dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
            
            userImage = base64Encoded;
            
            [[NSUserDefaults standardUserDefaults]setObject:userImage forKey:@"profileImage"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
            
        } else {
            
            userImage = @"";
            
        }
        
        [self googlePlusLoginServiceCall];
    }
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ForgotPasswordButton
- (IBAction)forgotPassword_BTN:(id)sender {
    [self forgotPasswordServiceCall];
}


@end
