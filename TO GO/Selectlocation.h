//
//  Selectlocation.h
//  TO GO
//
//  Created by Volive solutions on 10/31/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
@interface Selectlocation : UIViewController<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *detectlocationBtn;
- (IBAction)location_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *locationSearchBar;
- (IBAction)search_BTN:(id)sender;

@end
