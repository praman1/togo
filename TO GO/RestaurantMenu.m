//
//  RestaurantMenu.m
//  TO GO
//
//  Created by Volive solutions on 10/31/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "RestaurantMenu.h"
#import "RestaurantmenuCollectionViewCell.h"
#import "SVProgressHUD.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "TabCollectionViewCell.h"
#import "AddToCart.h"
#import "AddCartTableViewCell.h"
#import "Add.h"
#import "CartScreen.h"
#import "AppDelegate.h"
#import "Home.h"

@interface RestaurantMenu ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    
    NSArray *titleArray;
   
    UIButton *aButton;
    UIView *bView;
    NSString *shopIdString,*menuIdString,*itemIdString;
    NSString *combinedString,*vendorIdString;
    NSMutableArray *forSelection;
    NSMutableArray *menuCountArray,*menuIdArray,*menuNameArray;
    NSMutableArray *itemsCountArray,*itemIdArray,*itemNameArray,*itemImageArray,*itemCostArray;
    NSMutableArray *selectUnselectArray;
    NSMutableArray *shopIdArrays;
    NSMutableArray *cartItemCountArray;
    
    NSMutableArray *smallCostArray;
    NSMutableArray *mediumCostArray;
    NSMutableArray *largeCostArray;
    
    NSMutableArray *smallCostArray1;
    NSMutableArray *mediumCostArray1;
    NSMutableArray *largeCostArray1;
    
     NSMutableArray * sizeArray;
    
    NSString *taxString;
    NSString *totalItems;
    
    NSString *languageString;
    NSString *fromString;
    NSString *toString;
    
    NSString *sizeString;
    NSString *addShopIdString;
    
    NSMutableArray *ForAddBtn;
    TabCollectionViewCell *tabCell;
    AppDelegate * appDelegate;
    int mainCostValue;
    NSMutableArray *mainItemsCountArr;
    
    NSMutableArray *priceArray;
    NSMutableArray *array;
    
    int totalPrice ;
    int finalPrice;
    int price1 , priceA,priceB,priceC;
    int price2;
    int mainItemCount;
    int price;
    NSString *smallCostString;
    NSString *mediumCostString;
    NSString *largeCostString;
    
    RestaurantmenuCollectionViewCell *datasetCell;
    // collectionSelect
    NSMutableArray *item_selectArray;
    Add *Addobj;
    NSMutableArray *all_flavors_array;
    
    RestaurantmenuCollectionViewCell *cell;
    int value;
    NSString *valueString;
    
}
@property (weak, nonatomic) IBOutlet UICollectionView *mycollectionview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewout;

@end

@implementation RestaurantMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mainCostValue = 0;
    mainItemsCountArr = [NSMutableArray new];
    array = [[NSMutableArray alloc]init];
    
    _itemLbl.text = [NSString stringWithFormat:@"0 %@ | %d SAR",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Items"],mainCostValue];
    
    [_openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"] forState:UIControlStateNormal];
    [_viewCartBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"VIEW CART"] forState:UIControlStateNormal];
    
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    shopIdArrays = [[NSUserDefaults standardUserDefaults]objectForKey:@"restaurantShopId"];
  
    
    
    // Do any additional setup after loading the view.
    
   
    ForAddBtn = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
    
    selectUnselectArray = [[NSMutableArray  alloc]initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
    Addobj = [self.storyboard instantiateViewControllerWithIdentifier:@"Addobj"];

}

-(void)viewWillAppear:(BOOL)animated
{
//    if ([appDelegate.side isEqualToString:@"backFromCart"]) {
//        
//        [self getCartItems];
//    }
     //_itemLbl.text = [NSString stringWithFormat:@"0 Items | %d SAR",mainCostValue];
   // _itemLbl.text = [NSString stringWithFormat:@"%d Items | %@ SAR",[[[NSUserDefaults standardUserDefaults]objectForKey:@"count"]intValue],[[NSUserDefaults standardUserDefaults]objectForKey:@"price"]];
    
    self.navigationController.navigationBar.hidden = YES;
    //[self restaurantItemsServiceCall:menuIdString];
}

- (void)receiveMessage:(NSMutableArray *)message count:(NSString *)count {
    
    int itemCount = [count intValue];
    
    int globalPrice = 0;
    
    for (int i=0; i<message.count; i++) {
        
        globalPrice = globalPrice + [[message objectAtIndex:i] intValue];
    }
    
    
    if (itemCount>1) {
        
        NSString*combine = [NSString stringWithFormat:@"%@ %@ | %dSAR",count,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Items"],globalPrice];
        _itemLbl.text = combine;
        
    }else{
    
    NSString*combine = [NSString stringWithFormat:@"%@ %@ | %dSAR",count,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Item"],globalPrice];
        _itemLbl.text = combine;
        
    }

}

#pragma mark restaurantMenuServiceCall
-(void)restaurantMenuServiceCall:(NSString *)shopId
{
    
   // [_openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"] forState:UIControlStateNormal];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    shopIdString = shopId;
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
   // http://voliveafrica.com/togo/api/services/restaurant_details?API-KEY=225143&lang=en&shop_id=5
    NSMutableDictionary * restaurantPostDictionary = [[NSMutableDictionary alloc]init];
    [restaurantPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [restaurantPostDictionary setObject:languageString forKey:@"lang"];
    [restaurantPostDictionary setObject:shopId forKey:@"shop_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"restaurant_details?" withPostDict:restaurantPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
      //  NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *mainImage = [[dataDictionary objectForKey:@"data"]objectForKey:@"image"];
                NSString *logoImage = [[dataDictionary objectForKey:@"data"]objectForKey:@"logo"];
                [_mainImageView sd_setImageWithURL:[NSURL URLWithString:mainImage] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
                [_logoImageView sd_setImageWithURL:[NSURL URLWithString:logoImage] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
                _restaurantNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"name"];
                _restaurantSubNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"category_name"];
                
                fromString =[NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"from_time"]];
                
                toString = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"to_time"]];
                
                NSDateFormatter* df3 = [[NSDateFormatter alloc] init];
                [df3 setDateFormat:@"dd-MM-yy hh:mm a"];
                NSDate* fromDate1 =[[NSDate alloc]init];
                fromDate1=[df3 dateFromString:fromString];
                [df3 setDateFormat:@"hh:mm a"];
                NSString *fromDate = [df3 stringFromDate:fromDate1];
                
                NSDateFormatter* df2 = [[NSDateFormatter alloc] init];
                [df2 setDateFormat:@"dd-MM-yy hh:mm a"];
                NSDate* toDate1 =[[NSDate alloc]init];
                toDate1=[df2 dateFromString:toString];
                [df2 setDateFormat:@"hh:mm a"];
                NSString *toDate = [df2 stringFromDate:toDate1];
                
                combinedString = [NSString stringWithFormat:@"%@ - %@",fromDate ? fromDate:@"",toDate ? toDate:@""];
                
                _restaurantTimeLabel.text =[NSString stringWithFormat:@"%@",combinedString];
                
                vendorIdString = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"vendor_id"]];
                
                taxString = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"tax"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:taxString forKey:@"tax"];
                
                [[NSUserDefaults standardUserDefaults]setObject:vendorIdString forKey:@"vendorId"];
                
                menuCountArray = [NSMutableArray new];
                menuIdArray = [NSMutableArray new];
                menuNameArray = [NSMutableArray new];

                menuCountArray=[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"];
                
                if (menuCountArray.count > 0) {
                    
                    for (int i=0; i<menuCountArray.count; i++) {
                        
                        [menuIdArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"]objectAtIndex:i]objectForKey:@"menu_id"]];
                        [menuNameArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"]objectAtIndex:i]objectForKey:@"menu"]];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:menuIdArray forKey:@"menuIdArray"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        [[NSUserDefaults standardUserDefaults]setObject:menuNameArray forKey:@"menuNameArray"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        NSLog(@"%@",menuIdArray);
                        NSLog(@"%@",menuNameArray);
                       
                    }
                     [_tabCollectionView reloadData];
                }
//                else{
//                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
//                }

                if (menuIdArray.count>0) {
                    [self restaurantItemsServiceCall:[menuIdArray objectAtIndex:0]];

                }else{
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance]languageSelectedStringForKey:@"No items in this category"] onViewController:self completion:nil];
                }

            });

        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];
}

#pragma mark restaurantItemsServiceCall
-(void)restaurantItemsServiceCall:(NSString *)menuId{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    menuIdString = menuId;
    // http://voliveafrica.com/togo/api/services/items?API-KEY=225143&lang=en&shop_id=1&menu_id=2
    NSMutableDictionary * restaurantPostDictionary = [[NSMutableDictionary alloc]init];
    [restaurantPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [restaurantPostDictionary setObject:languageString forKey:@"lang"];
     //[restaurantPostDictionary setObject:_shopIdStr forKey:@"shop_id"];
    [restaurantPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    //[restaurantPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"restaurantShopId"] forKey:@"shop_id"];
    [restaurantPostDictionary setObject:menuId forKey:@"menu_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"items?" withPostDict:restaurantPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
      //  NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                itemsCountArray = [NSMutableArray new];
                itemIdArray = [NSMutableArray new];
                itemNameArray = [NSMutableArray new];
                itemImageArray = [NSMutableArray new];
                itemCostArray = [NSMutableArray new];
                item_selectArray = [NSMutableArray new];
                all_flavors_array = [NSMutableArray new];
                cartItemCountArray = [NSMutableArray new];
                smallCostArray = [NSMutableArray new];
                mediumCostArray = [NSMutableArray new];
                largeCostArray = [NSMutableArray new];
                
                itemsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (itemsCountArray.count>0) {
                    
                    for (int i=0; i<itemsCountArray.count; i++) {
                        
                        [itemIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [itemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [itemImageArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [itemCostArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"cost"]];
                        [cartItemCountArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"cart_item_count"]];
                        [smallCostArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"small_cost"]];
                        [mediumCostArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"medium_cost"]];
                        [largeCostArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"large_cost"]];
                        
                        
                        //itemIdString = [NSString stringWithFormat:@"%@",itemIdArray];
                        
                        [all_flavors_array addObject:[[itemsCountArray objectAtIndex:i]objectForKey:@"flavours"]];
                        
                        [item_selectArray addObject:@"0"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:itemIdString forKey:@"itemId"];
                        
                        NSLog(@"%@",itemNameArray);
                        NSLog(@"all flavors array %@",all_flavors_array);
                        
                    }
                    [_mycollectionview reloadData];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
                    });

                }
                
            });
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];

}

#pragma mark addToCartServiceCall
-(void)addToCartServiceCall:(id)sender
{
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    // {"API-KEY":225143,"user_id":134721528,"vendor_id":3288062319,"item_id":3,"shop_id":3,"category_id":4,"item_count":3,"flavour_status":0}
    
    NSString *url=@"http://voliveafrica.com/togo/api/services/add_cart";
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
  
    
        NSDictionary *dic = @{
                              @"API-KEY":APIKEY,
                              @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"],
                              @"vendor_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"],
                              @"category_id":self.selected_catID,
                              @"shop_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"],
                              @"item_id":itemIdString,
                              @"item_count":@1,
                              @"flavour_status":@0,
                              @"item_size":sizeString ? sizeString : @""
                              
                               };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *postlength2=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];;
    
    NSString *postlength =[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",
      (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
     [[session dataTaskWithRequest:request completionHandler:^(NSData*  _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)  {
     
         NSDictionary *statusDict = [[NSDictionary alloc]init];
         statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
     
         
             if (statusDict) {
                 NSLog(@"Images from server %@", statusDict);
                 NSString * status = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
                 // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if ([status isEqualToString:@"1"])
                     {
                         [SVProgressHUD dismiss];
                         
                         addShopIdString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"shop_id"]];
                         
                         NSLog(@"my shop id is %@",addShopIdString);
                         
                         [[NSUserDefaults standardUserDefaults]setObject:addShopIdString forKey:@"addShopId"];
                         

                         [self getCartItems];
                         
                         if ([languageString isEqualToString:@"1"]) {
                             
                             [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] onViewController:self completion:nil];
                         }else{
                             
                             [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:@"تم أضافة المنتجات بنجاح " onViewController:self completion:nil];
                         }
                         
                         
                         
                     }
                     else
                     {
                         [SVProgressHUD dismiss];

                     }
                 });
             }
             else{
                 
                 [SVProgressHUD dismiss];
                 [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] onViewController:self completion:nil];
                 
             }
     }]
      
      resume];
    

}

#pragma mark getCartItems
-(void)getCartItems{
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/cart_items?API-KEY=225143&user_id=134721528&lang=en
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    //    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    // [SVProgressHUD showWithStatus:progress];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"cart_items?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        
        totalItems = [NSString stringWithFormat:@"%@",[dict objectForKey:@"total_items"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            priceArray = [NSMutableArray new];
            
            smallCostArray1 = [NSMutableArray new];
            mediumCostArray1 = [NSMutableArray new];
            largeCostArray1 = [NSMutableArray new];
            
            sizeArray = [NSMutableArray new];
            
            finalPrice = 0;
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                NSArray *array = [dict objectForKey:@"data"];
                int totalCost = 0;
                for(NSDictionary *mainData in array){
                    
                    
                    sizeString = [mainData objectForKey:@"item_size"];
                    
                    if ([sizeString isEqualToString:@"small"]) {
                        
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"small_cost"] intValue];
                        priceA = [[mainData objectForKey:@"small_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        totalPrice = (mainItemCount * price1) + price;
                        finalPrice = finalPrice + totalPrice;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        // [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                        
                    }else if ([sizeString isEqualToString:@"medium"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"medium_cost"] intValue];
                        priceA = [[mainData objectForKey:@"medium_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        totalPrice = (mainItemCount * price1) + price;
                        finalPrice = finalPrice + totalPrice;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        //[priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                        
                    }else if ([sizeString isEqualToString:@"large"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"large_cost"] intValue];
                        priceA = [[mainData objectForKey:@"large_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        totalPrice = (mainItemCount * price1) + price;
                        finalPrice = finalPrice + totalPrice;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                        
                    }
                    if ([totalItems isEqualToString:@"1"]) {
                        
                        _itemLbl.text = [NSString stringWithFormat:@"%@Item | %dSAR",totalItems,finalPrice];
                        
                    }else{
                        _itemLbl.text = [NSString stringWithFormat:@"%@Items | %dSAR",totalItems,finalPrice];
                    }
                    
                    
                     //_itemLbl.text = [NSString stringWithFormat:@"%dSAR",finalPrice];
                    
//                    [self.delegate receiveMessage:[NSString stringWithFormat:@"%d",totalPrice] count:[NSString stringWithFormat:@"%d",mainItemCount]];
                    
                    
                    [sizeArray addObject:[mainData objectForKey:@"item_size"]];
                    
                    [smallCostArray addObject:[mainData objectForKey:@"small_cost"]];
                    [mediumCostArray addObject:[mainData objectForKey:@"medium_cost"]];
                    [largeCostArray addObject:[mainData objectForKey:@"large_cost"]];
                    
                    smallCostString = [mainData objectForKey:@"small_cost"];
                    mediumCostString = [mainData objectForKey:@"medium_cost"];
                    largeCostString = [mainData objectForKey:@"large_cost"];
                    
                    
                    totalCost = (totalCost + [[mainData objectForKey:@"cost"] intValue]);
                    
                    
                }
            }
            
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
            
        });
        
    }];

}


#pragma mark deleteCartService
-(void)deleteCartService{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/togo/api/services/delete_cart?API-KEY=225143&user_id=134721528
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * deleteDictionary = [[NSMutableDictionary alloc]init];
    [deleteDictionary setObject:APIKEY forKey:@"API-KEY"];
    [deleteDictionary setObject:languageString forKey:@"lang"];
    [deleteDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"delete_cart?" withPostDict:deleteDictionary andReturnWith:^(NSData *dataFromJson) {
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
//                Home * home=[self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//                
//                [self.navigationController pushViewController:home animated:YES];
               
            }
            else
            {
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            }});
    }];
    
    
}

-(void)stepperAction:(id)sender{
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    datasetCell =
    (RestaurantmenuCollectionViewCell *)[self.mycollectionview cellForItemAtIndexPath:indexPath];
    
    value = datasetCell.stepView.value;
    
     dispatch_async(dispatch_get_main_queue(), ^{
    [self updateCart:sender];
     });
}

#pragma mark updateCart
-(void)updateCart:(id)stepperValue{
    
    CGPoint btnPosition = [stepperValue convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    RestaurantmenuCollectionViewCell *interCell = (RestaurantmenuCollectionViewCell *)[_mycollectionview cellForItemAtIndexPath:indexPath];
    
    
    //http://voliveafrica.com/togo/api/services/update_cart?API-KEY=225143&user_id=134721528&item_id=3&item_count=1
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    [SVProgressHUD dismiss];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:itemIdString forKey:@"item_id"];
    int value1 = interCell.stepView.value;
    
    [viewCartPostDictionary setObject:[NSString stringWithFormat:@"%d",value1 ] forKey:@"item_count"];
    [viewCartPostDictionary setObject:interCell.sizeBtn_Outlet.currentTitle forKey:@"size"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"update_cart?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
//                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
                NSString * itemCountStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"no_of_items"]];
                NSString * itemCostStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"total_cost"]];
                
                _itemLbl.text=[NSString stringWithFormat:@"%@ %@ | %@ SAR",itemCountStr,[[sharedclass sharedInstance] languageSelectedStringForKey:@"Items"],itemCostStr];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                
//                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
            }
        });
        
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated{
 self.navigationController.navigationBar.hidden = NO;

}


#pragma mark - collectionview methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _tabCollectionView) {
        return menuCountArray.count;
    }else{
        return itemsCountArray.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _mycollectionview) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        [cell.restaurantMenuImageView sd_setImageWithURL:[NSURL URLWithString:[itemImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
        cell.restaurantMenuNameLabel.text = [itemNameArray objectAtIndex:indexPath.row];
        [cell.addBtn setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"ADD"] forState:UIControlStateNormal];
        [cell.sizeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"size"] forState:UIControlStateNormal];

        cell.addBtn.tag = indexPath.row;
        cell.addBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.addBtn.layer.borderWidth = 1;
        cell.stepView.value = 1;
        cell.stepView.maximumValue = 1000000;
        [cell.stepView addTarget:self action:@selector(stepperAction:) forControlEvents:UIControlEventValueChanged];
     //   [cell.addBtn addTarget:self action:@selector(goToNext) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[item_selectArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cell.stepView.hidden = YES;
            cell.addBtn.hidden = NO;
        }else{
//            cartCell.qty.text = [NSString stringWithFormat:@"%@",ForAddBtn[indexPath.row]];
            cell.stepView.hidden = NO;
            cell.addBtn.hidden = YES;
        }
        return cell;
        
    }else{
        tabCell = [_tabCollectionView dequeueReusableCellWithReuseIdentifier:@"TabViewCell" forIndexPath:indexPath];
        tabCell.itemNameLabel.text = [menuNameArray objectAtIndex:indexPath.row];
        if ([[selectUnselectArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            
            
            tabCell.itemView.hidden = NO;
            
        } else {
            
            tabCell.itemView.hidden = YES;
            
        }
        return tabCell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _tabCollectionView) {
        
        selectUnselectArray = [[NSMutableArray  alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
        
        [selectUnselectArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
        
        [self restaurantItemsServiceCall:[menuIdArray objectAtIndex:indexPath.row]];
        [_tabCollectionView reloadData];
    }
    else{
        
        itemIdString = [itemIdArray objectAtIndex:indexPath.row];
        
//        AddToCart *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"AddToCart"];
//        [self.navigationController pushViewController:cart animated:TRUE];
//        [self restaurantItemsServiceCall:[shopIdArrays objectAtIndex:indexPath.row]];
    }
    }

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _mycollectionview) {
        
        CGFloat width = (CGFloat) (_mycollectionview.frame.size.width/2);
        
        return CGSizeMake(width-15,210);
    }
    else{
        CGFloat width = (CGFloat) (_tabCollectionView.frame.size.width/2);
        
        return CGSizeMake(width-5,45);
    }
    
}

-(void)goToNext
{
    AddToCart *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"AddToCart"];
    
    [self.navigationController pushViewController:cart animated:TRUE];
    
}
- (IBAction)sizeSelect_BTN:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sizes"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    //***Small
    UIAlertAction *small = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"small"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        RestaurantmenuCollectionViewCell *cell1 = (RestaurantmenuCollectionViewCell *)[_mycollectionview cellForItemAtIndexPath:indexPath];
        
        sizeString =@"small";
        [[NSUserDefaults standardUserDefaults]setObject:sizeString forKey:@"size"];
        NSLog(@"%@Size Is#####",sizeString);
         itemIdString = [itemIdArray objectAtIndex:indexPath.row];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[item_selectArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
                
                [self updateCart:sender];
            }else{
                int stepper = cell1.stepView.value;
                [self updateCart:sender];
            }
            });
        
        [cell1.sizeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"small"] forState:UIControlStateNormal];
        cell1.menuMainPriceLabel.text = [smallCostArray objectAtIndex:indexPath.row];
              }];
    
    //***Medium
    UIAlertAction *medium = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"medium"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        RestaurantmenuCollectionViewCell *cell1 = (RestaurantmenuCollectionViewCell *)[_mycollectionview cellForItemAtIndexPath:indexPath];
        
        sizeString =@"medium";
        [[NSUserDefaults standardUserDefaults]setObject:sizeString forKey:@"size"];
        NSLog(@"%@Size Is#####",sizeString);
         itemIdString = [itemIdArray objectAtIndex:indexPath.row];
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            if ([[item_selectArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
                
                [self updateCart:sender];
            }else{
                int stepper = cell1.stepView.value;
                [self updateCart:sender];
            }

            
             });
        
        [cell1.sizeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"medium"] forState:UIControlStateNormal];
        cell1.menuMainPriceLabel.text = [mediumCostArray objectAtIndex:indexPath.row];
        [mediumCostArray replaceObjectAtIndex:indexPath.row withObject:[mediumCostArray objectAtIndex:indexPath.row]];
        
        
    }];
    //***Large
    UIAlertAction *large = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"large"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        RestaurantmenuCollectionViewCell *cell1 = (RestaurantmenuCollectionViewCell *)[_mycollectionview cellForItemAtIndexPath:indexPath];
        
        sizeString =@"large";
        [[NSUserDefaults standardUserDefaults]setObject:sizeString forKey:@"size"];
        NSLog(@"%@Size Is#####",sizeString);
         itemIdString = [itemIdArray objectAtIndex:indexPath.row];
        
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if ([[item_selectArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
                 
                 [self updateCart:sender];
                 
             }else{
                 
                 int stepper = cell1.stepView.value;
                 [self updateCart:sender];
                 
             }
              });
        
        [cell1.sizeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"large"] forState:UIControlStateNormal];
        cell1.menuMainPriceLabel.text = [largeCostArray objectAtIndex:indexPath.row];
        [largeCostArray replaceObjectAtIndex:indexPath.row withObject:[largeCostArray objectAtIndex:indexPath.row]];
        
    }];
    //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
    int smallString = [[smallCostArray objectAtIndex:indexPath.row]intValue];
    int mediumString = [[mediumCostArray objectAtIndex:indexPath.row] intValue];
    int largeString = [[largeCostArray objectAtIndex:indexPath.row] intValue];
    
    if (smallString >0) {
        
        [alertController addAction:small];
        
    }
     if (mediumString >0){
        
        [alertController addAction:medium];
        
    }
     if (largeString >0){
        
        [alertController addAction:large];
        
    }
    
    
    // [controller addAction:latest];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
   // [_mycollectionview reloadData];
    
}

#pragma mark AddToCrt Button
- (IBAction)AddToCrt:(UIButton *)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    [[NSUserDefaults standardUserDefaults]setObject:itemIdString forKey:@"itemId"];
    
    [mainItemsCountArr addObject:itemIdString];
    
    NSArray *flavors = [all_flavors_array objectAtIndex:indexPath.row];
    
    NSLog(@"%ld",(long)sender.tag);
    
    //NSLog(@"mystring %@",addShopIdString);
    
    NSString * shopIdStr =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"addShopId"]];
    
    NSLog(@"mystring %@",shopIdStr);
    
     if ([shopIdStr isEqualToString:@""] || [shopIdStr isKindOfClass:[NSNull class]] || shopIdStr == nil || [shopIdStr isEqualToString:@"(null)"] || [shopIdStr isEqualToString:_shopIdStr1])
     {
         
        if (sizeString.length > 0) {
        if (flavors.count > 0) {
            
            Addobj.delegate = self;
            [Addobj flavoursArray:flavors];
            Addobj.itemIdString = [itemIdArray objectAtIndex:indexPath.row];
            //Addobj.itemPriceStr = [itemCostArray objectAtIndex:indexPath.row];
            Addobj.selected_catID = _selected_catID;
            Addobj.sizeStr = sizeString;
            Addobj.imageStr = [itemImageArray objectAtIndex:indexPath.row];
            NSLog(@"My Image Is %@",Addobj.imageStr);
            Addobj.nameStr = [itemNameArray objectAtIndex:indexPath.row];
            NSLog(@"My Name Is %@",Addobj.nameStr);
            [self.navigationController pushViewController:Addobj animated:YES];
            
        }else{
            
            if (sizeString.length > 0) {
                
                [item_selectArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                [_mycollectionview reloadData];
                [self addToCartServiceCall:sender];
            }else{
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please select size"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please select size"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];

        }
    
    }
    else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Your cart has items from other resturant do you want to discard it and add items from this resturant ?"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Yes"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
                    [self deleteCartService];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"addShopId"];
        
                }];
        
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:cancel];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(void)dismiss{
    NSLog(@"clicked");
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [Addobj.view setHidden:YES];
                     }
                     completion:^(BOOL finished){
                         //[Addobj.view setHidden:YES];
                     }];
    
}

- (IBAction)back:(UIButton *)sender {
    
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Do you want to delete all items from Cart?"] preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Yes"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            
//            [self deleteCartService];
//            
//            
//        }];
//        
//        UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:cancel];
//        [alert addAction:ok];
//        [self presentViewController:alert animated:YES completion:nil];
    
    [self.navigationController popViewControllerAnimated:TRUE];
    
}

#pragma mark viewCartBtnAction
- (IBAction)viewCartBtnAction:(id)sender {
    
    if (cartItemCountArray > 0) {
        
        CartScreen * cart=[self.storyboard instantiateViewControllerWithIdentifier:@"CartScreen"];
       // cart.tax = taxString;
        cart.itemSizeString = sizeString;
        [self.navigationController pushViewController:cart animated:YES];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please add items to cart"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }

}



@end
