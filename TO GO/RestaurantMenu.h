//
//  RestaurantMenu.h
//  TO GO
//
//  Created by Volive solutions on 10/31/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//


#import <UIKit/UIKit.h>
@protocol RBFirstVCDelegate;
#import "Add.h"


@interface RestaurantMenu : UIViewController<RBFirstVCDelegate>

//@property NSMutableArray *shopIdArray;
-(void)restaurantMenuServiceCall:(NSString *)shopId;
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantSubNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTimeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *tabCollectionView;
-(void)restaurantItemsServiceCall:(NSString *)menuId;
@property NSString *selected_catID;
@property NSString* openString;

@property NSString * shopIdStr1;
- (IBAction)viewCartBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *itemLbl;
    @property (weak, nonatomic) IBOutlet UIButton *viewCartBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn_Outlet;
- (IBAction)sizeSelect_BTN:(id)sender;

@end
