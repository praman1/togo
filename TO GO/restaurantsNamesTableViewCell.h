//
//  restaurantsNamesTableViewCell.h
//  TO GO
//
//  Created by volive solutions on 1/22/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface restaurantsNamesTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *restaurantSubNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTimeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantItemImgView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn_Outlet;

@end
