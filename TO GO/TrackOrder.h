//
//  TrackOrder.h
//  TO GO
//
//  Created by Volive solutions on 11/3/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface TrackOrder : UIViewController<GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *minBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *trackOrderMapView;
@property (weak, nonatomic) IBOutlet UILabel *orderReceivedInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderConfirmedInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *preparingTimeInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTimeInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmPickupInfoLabel;
    @property (weak, nonatomic) IBOutlet UILabel *orderRcvdLabel;
    @property (weak, nonatomic) IBOutlet UILabel *orderCnfmdLabel;
    @property (weak, nonatomic) IBOutlet UILabel *preparingTimeLabel;
    @property (weak, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
    @property (weak, nonatomic) IBOutlet UILabel *cnfmPickupLabel;
    
-(void)trackOrderServiceCall:(NSString *)orderId;
- (IBAction)confirmPickup_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *orderConfirmedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *orderRcvdImageView;

@property (weak, nonatomic) IBOutlet UIImageView *arrivalImageView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmImageView;
@property (weak, nonatomic) IBOutlet UIImageView *preparingTimeImageView;

@property NSString * imageStr,* nameStr,*checkString;

@property (weak, nonatomic) IBOutlet UIButton *cnfmBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn_Outlet;

@end
