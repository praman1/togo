//
//  SuggestRestaurant.m
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "SuggestRestaurant.h"
#import "sharedclass.h"
#import "SWRevealViewController.h"
#import "HeaderAppConstant.h"
@interface SuggestRestaurant ()<UITextFieldDelegate,GMSMapViewDelegate,CLLocationManagerDelegate>{
    sharedclass *objForShare;
    NSString *customerTypeString;
    
    NSString *currentLat;
    NSString *currentLong;
    
    NSString *selectedLat;
    NSString *seledctedLong;
    
    NSString *languageString;
    
    CLLocation *currentLocation;
   // GMSMapView *googleMapView;
    
    CLLocationCoordinate2D center;
    
    CLLocationManager *locationManager;
    
    
    BOOL isClicked;

}

@end

@implementation SuggestRestaurant

- (void)viewDidLoad {
    [super viewDidLoad];
    isClicked = NO;
    // Do any additional setup after loading the view.
    objForShare = [[sharedclass alloc] init];
    
    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Suggest Restaurant"];
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];

    
    [objForShare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForShare textfieldAsLine:_number lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForShare textfieldAsLine:_contact lineColor:[UIColor lightGrayColor] myView:self.view];
    
  //  currentLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
  //  currentLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
    
    if([languageString isEqualToString:@"ar"]){
        
        _nameTxt.textAlignment = NSTextAlignmentRight;
        _number.textAlignment = NSTextAlignmentRight;
        _contact.textAlignment = NSTextAlignmentRight;
        
    } else  {
        
        _nameTxt.textAlignment = NSTextAlignmentLeft;
        _number.textAlignment = NSTextAlignmentLeft;
        _contact.textAlignment = NSTextAlignmentLeft;
        
        }
    
     [_sendBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"SEND"] forState:UIControlStateNormal];
    
    _wantARestaurantLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Want a restaurant to join TOGO?"];
    _addTheDetailsLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"add the details & we'll get in touch"];
    _areYouLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Are You:"];
    _customerLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Customer"];
    _contactNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Contact Name(if available)"];
    _restaurantLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Restaurant Management"];
    _rstrntNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Restaurant Name"];
    _mobileNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Mobile Number"];
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    [_customerBtn_Outlet addTarget:self action:@selector(customer) forControlEvents:UIControlEventTouchUpInside];
    [_restaurentBtn_Outlet addTarget:self action:@selector(restaurant) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
    
    
}

//-(BOOL)textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

#pragma mark TextFieldDelegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTxt) {
        
        [objForShare textfieldAsLine:_nameTxt lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_suggestScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _number) {
        
        [objForShare textfieldAsLine:_number lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_suggestScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }else if (textField == _contact) {
        
        [objForShare textfieldAsLine:_contact lineColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] myView:self.view];
        [_suggestScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if (textField == _nameTxt) {
        [objForShare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _number){
        [objForShare textfieldAsLine:_number lineColor:[UIColor lightGrayColor] myView:self.view];
    }
    else if (textField == _contact){
        [objForShare textfieldAsLine:_contact lineColor:[UIColor lightGrayColor] myView:self.view];
    }

    
    
    
    [_nameTxt resignFirstResponder];
    [_number resignFirstResponder];
    [_contact resignFirstResponder];
    
    [_suggestScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTxt) {
        
        [_number becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _number) {
        
        [_contact becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _contact) {
        
        [_contact resignFirstResponder];
        
        returnValue = YES;
        
    }
    return returnValue;
}

-(void)customer{
    
        _customerCheckImage.image = [UIImage imageNamed:@"Check"];
        _restaurantCheckImage.image = [UIImage imageNamed:@"UnCheck"];
   
        customerTypeString = [NSString stringWithFormat:@"1"];

}

-(void)restaurant{
    
        _restaurantCheckImage.image = [UIImage imageNamed:@"Check"];
        _customerCheckImage.image = [UIImage imageNamed:@"UnCheck"];
    
        customerTypeString = [NSString stringWithFormat:@"2"];
    
}

- (IBAction)customer_BTN:(id)sender {
//    isClicked=!isClicked;
//    if(isClicked == YES){
//        _customerCheckImage.image = [UIImage imageNamed:@"Check"];
//        _restaurantCheckImage.image = [UIImage imageNamed:@"UnCheck"];
//    }
//    customerString = [NSString stringWithFormat:@"1"];
}

- (IBAction)restaurant_BTN:(id)sender {
//    isClicked=!isClicked;
//    if(isClicked == YES){
//        _restaurantCheckImage.image = [UIImage imageNamed:@"Check"];
//        _customerCheckImage.image = [UIImage imageNamed:@"UnCheck"];
//        
//    }
//    customerString = [NSString stringWithFormat:@"2"];

}

#pragma mark Suggest Restaurant Servicecall
-(void)suggestRestaurantServiceCall{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    /*
     API-KEY:225143
     user_id:134721528
     user_type:1 (1=>customer,2=>managment)
     restaurant_name:test
     mobile_num:1111111
     contact_name:435345345
     latitude:17.40965271
     longitude:78.30155945     */
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/suggest_restaurant";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * suggestPostDictionary = [[NSMutableDictionary alloc]init];
    
    [suggestPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [suggestPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [suggestPostDictionary setObject:customerTypeString forKey:@"user_type"];
    [suggestPostDictionary setObject:_nameTxt.text forKey:@"restaurant_name"];
    [suggestPostDictionary setObject:_number.text forKey:@"mobile_num"];
    [suggestPostDictionary setObject:_contact.text forKey:@"contact_name"];
    [suggestPostDictionary setObject:selectedLat forKey:@"latitude"];
    [suggestPostDictionary setObject:seledctedLong forKey:@"longitude"];
    [suggestPostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"suggest_restaurant" withPostDict:suggestPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
                });
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
            }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
            });
        }
    }];

    
}
- (IBAction)send_BTN:(id)sender {
    
    [_nameTxt resignFirstResponder];
    [_number resignFirstResponder];
    [_contact resignFirstResponder];
    
    if (_nameTxt.text.length < 3 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [_nameTxt becomeFirstResponder]; }];
    }else if (_number.text.length < 5 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [_number becomeFirstResponder]; }];
        
    } else {
        
        [self suggestRestaurantServiceCall];
    }
    
    }


-(void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation * > *)locations
{
    
    
    // User location
    
    currentLocation = [locations lastObject];
    
    [locationManager stopUpdatingLocation];
    
    if (currentLat == NULL && currentLong == NULL)
    {
        currentLat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        currentLong  = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        
        [self loadViewWithAttributes];
        
    }
}

-(void)loadViewWithAttributes{
    
    center.latitude = [currentLat doubleValue];
    center.longitude = [currentLong doubleValue];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[currentLat doubleValue]  longitude:[currentLong doubleValue] zoom:16];
    [_googleMapView setCamera:camera];
    _googleMapView.myLocationEnabled = YES;
    _googleMapView.delegate = self;
    
 }
- (void) mapView: (GMSMapView *)mapView didChangeCameraPosition: (GMSCameraPosition *)position {
    
    
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    currentLat = [NSString stringWithFormat:@"%f",mapView.camera.target.latitude];
    currentLong  = [NSString stringWithFormat:@"%f",mapView.camera.target.longitude];
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            
            NSLog(@"%@", error.debugDescription);
        
            
        } else {
            
            GMSAddress* address = [results firstResult];
            
            NSLog(@"address %@",address);
            
            selectedLat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
            seledctedLong = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
            NSLog(@"lat is %@",selectedLat);
            NSLog(@"lng is %@",seledctedLong);
            
        }
    }];
}


@end
