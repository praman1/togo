//
//  RestaurantmenuCollectionViewCell.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"
@interface RestaurantmenuCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantMenuImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantMenuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantSubMenuLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuMainPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (weak, nonatomic) IBOutlet ANStepperView *stepView;
@property (weak, nonatomic) IBOutlet UIButton *sizeBtn_Outlet;

@end
