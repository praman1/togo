//
//  Signup.h
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface Signup : UIViewController<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;
@property (weak, nonatomic) IBOutlet UITextField *carnumTxt;
@property (weak, nonatomic) IBOutlet UITextField *carcolorTxt;

@property (weak, nonatomic) IBOutlet UIButton *createAccountBtn;
@property (weak, nonatomic) IBOutlet UILabel *signupToCreateAcntLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;
- (IBAction)createAccount_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *alreadyHaveAccountLabel;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIScrollView *signupScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *signupImageview;

@end
