//
//  RestaurantNamesVC.m
//  TO GO
//
//  Created by volive solutions on 1/22/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "RestaurantNamesVC.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "sharedclass.h"
#import "RestaurantMenu.h"
#import "restaurantsNamesTableViewCell.h"
#import "Add.h"

@interface RestaurantNamesVC ()<UITableViewDelegate,UITableViewDataSource>
{
    restaurantsNamesTableViewCell *restaurantCell;
    
     NSMutableArray *restaurantsCountArray,*restaurantShopIdArray,*restaurantCatIdArray,*restaurantNameArray,*restaurantImageArray,*restaurantLogoArray,*restaurantCatNameArray,*restaurantFromTimeArray,*restaurantToTimeArray,*restaurantTotalTimeArray;
    
    NSMutableArray *restaurantSubNameArray,*restaurantTimeArray,*vendorIdArray,*shopIdArray,*catIdArray;
    
    NSString *combineString,*shopIdString;
    NSString *searchText,*searchString;
    NSString *languageString;
    
    NSString *toString;
    NSString *fromString;
    }

@end

@implementation RestaurantNamesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self RestaurantServiceCall];
    
    self.title = @"View All";
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];

    self.navigationItem.leftBarButtonItem = anotherButton;

    // Do any additional setup after loading the view.
}
-(void)back:(UIButton *)sender{
    NSLog(@"press");
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)RestaurantServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
        //http://voliveafrica.com/togo/api/services/home?API-KEY=225143&lang=en
        NSMutableDictionary * homePostDictionary = [[NSMutableDictionary alloc]init];
        [homePostDictionary setObject:APIKEY forKey:@"API-KEY"];
        [homePostDictionary setObject:languageString forKey:@"lang"];
    
    
        [[sharedclass sharedInstance]fetchResponseforParameter:@"home?" withPostDict:homePostDictionary andReturnWith:^(NSData *dataFromJson) {
    
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
    
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
    
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
    
                restaurantsCountArray = [NSMutableArray new];
                restaurantNameArray = [NSMutableArray new];
                restaurantImageArray = [NSMutableArray new];
                restaurantCatNameArray = [NSMutableArray new];
                restaurantLogoArray = [NSMutableArray new];
                restaurantShopIdArray = [NSMutableArray new];
                restaurantCatIdArray = [NSMutableArray new];
                restaurantToTimeArray = [NSMutableArray new];
                restaurantFromTimeArray = [NSMutableArray new];
                restaurantTotalTimeArray = [NSMutableArray new];
                dispatch_async(dispatch_get_main_queue(), ^{
    
                    restaurantsCountArray=[dataDictionary objectForKey:@"restaurants"];
                    if (restaurantsCountArray.count>0) {
                        for (int i=0; i<restaurantsCountArray.count; i++) {
                            [restaurantNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"name"]];
                            [restaurantImageArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"image"]];
                            [restaurantCatNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_name"]];
                            [restaurantLogoArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"logo"]];
                            [restaurantShopIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"shop_id"]];
                            [restaurantCatIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_id"]];
                            
                            fromString =[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"]];
                            
                            toString = [NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                            
                            
                            [restaurantToTimeArray addObject:toString];
                            [restaurantFromTimeArray addObject:fromString];
                            
                            combineString=[NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                            
                            
                            NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
                            [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
                            NSDate* date1 =[[NSDate alloc]init];
                            date1=[df1 dateFromString:fromString];
                            [df1 setDateFormat:@"hh:mm a"];
                            NSString *fromDate = [df1 stringFromDate:date1];
                            
                            NSDateFormatter* df2 = [[NSDateFormatter alloc] init];
                            [df2 setDateFormat:@"dd-MM-yy hh:mm a"];
                            NSDate* date2 =[[NSDate alloc]init];
                            date2=[df2 dateFromString:toString];
                            [df2 setDateFormat:@"hh:mm a"];
                            NSString *toDate = [df2 stringFromDate:date2];
    
                           
                            
                            [restaurantTotalTimeArray addObject:[NSString stringWithFormat:@"%@ - %@",fromDate,toDate]];
                            
                            shopIdString = [NSString stringWithFormat:@"%@",restaurantShopIdArray];
                            //[restaurantToTimeArray addObject:combineString];
                            [[NSUserDefaults standardUserDefaults]setObject:restaurantShopIdArray forKey:@"restaurantShopId"];
                            [[NSUserDefaults standardUserDefaults]setObject:restaurantCatIdArray forKey:@"restaurantCatIdArray"];
                            NSLog(@"%@",restaurantNameArray);
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_restaurantsTV reloadData];
                    });

    
                });
                
                
                
            }else {
    
                dispatch_async(dispatch_get_main_queue(), ^{
    
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
    
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return restaurantNameArray.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    restaurantCell = [_restaurantsTV dequeueReusableCellWithIdentifier:@"restaurantCell"];
    
    if (restaurantCell == nil) {
        
        restaurantCell = [[restaurantsNamesTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"restaurantCell"];
        
    }
    restaurantCell.restaurantNameLbl.text = [restaurantNameArray objectAtIndex:indexPath.row];
     restaurantCell.restaurantSubNameLbl.text = [restaurantCatNameArray objectAtIndex:indexPath.row];
     restaurantCell.restaurantTimeLbl.text = [restaurantTotalTimeArray objectAtIndex:indexPath.row];
    [ restaurantCell.restaurantItemImgView sd_setImageWithURL:[NSURL URLWithString:[restaurantLogoArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    
    //Current Time
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    //FromTime
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSDate* newDate =[[NSDate alloc]init];
    newDate=[df dateFromString:[restaurantFromTimeArray objectAtIndex:indexPath.row]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *reqFromTimeString = [df stringFromDate:newDate];
    
    //ToTime
    NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
    [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df1 setTimeZone:[NSTimeZone systemTimeZone]];
    [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
    NSDate* newDate1 =[[NSDate alloc]init];
    newDate1=[df1 dateFromString:[restaurantToTimeArray objectAtIndex:indexPath.row]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *reqToTimeString = [df stringFromDate:newDate1];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yy hh:mm a"];
    
    NSDate *date1= [formatter dateFromString:reqFromTimeString];
    NSDate *date2 = [formatter dateFromString:reqToTimeString];
    NSDate *date3 = [formatter dateFromString:resultString];
    
    NSComparisonResult FromComparison = [date3 compare:date1];
    NSComparisonResult ToComparison = [date3 compare:date2];
    
    if(FromComparison == NSOrderedDescending)
    {
        NSLog(@"date3 is later than date1");
        if (ToComparison==NSOrderedDescending) {
            
            NSLog(@"date3 is later than date2");
            NSLog(@"Sorry, we are closed!");
            
            [restaurantCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
            
            
        }
        else if (ToComparison==NSOrderedAscending)
        {
            
            NSLog(@"date2 is later than date3");
            NSLog(@"We are Open!");
            
            
            
            [restaurantCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"] forState:UIControlStateNormal];
            
        }
    }
    else if(FromComparison == NSOrderedAscending)
    {
        NSLog(@"date1 is later than date3");
        NSLog(@"Sorry, we are closed!");
        
        
        
        [restaurantCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"date3 is equal to date1");
        NSLog(@"Sorry, we are closed!");
        
        
        
        [restaurantCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
    }

    
    
    
    return restaurantCell;

    
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 100;
//    
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _restaurantsTV)
    {
        
    
        RestaurantMenu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantMenu"];
        [[NSUserDefaults standardUserDefaults]setObject:[restaurantShopIdArray objectAtIndex:indexPath.row] forKey:@"shopId"];
    
        
        [menu restaurantMenuServiceCall:[restaurantShopIdArray objectAtIndex:indexPath.row]];
        menu.selected_catID = [restaurantCatIdArray objectAtIndex:indexPath.row];
        // menu.shopIdArray = shopIdArray;
        // menu.shopIdStr=[shopIdArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:menu animated:TRUE];
    
    }
}
 @end
