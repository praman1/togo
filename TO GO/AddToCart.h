//
//  AddToCart.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddToCart : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *tabCollectionView;
@property NSMutableArray *itemsArray;
@property NSString *menuIdString;
-(void)restaurantItemsServiceCall:(NSString *)shopId;
-(void)repeatServiceCall:(NSString *)menuId;
@property NSString *selected_catID;
@end
