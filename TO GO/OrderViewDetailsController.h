//
//  OrderViewDetailsController.h
//  TO GO
//
//  Created by volive solutions on 20/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewDetailsController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *orderTable;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property NSString *statusString, *checkStatusString;
- (IBAction)conform_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *conformBtn_Outlet;
- (IBAction)reject_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIView *conformRejectView;

@property NSString *checkString,*orderIdString;

@end
