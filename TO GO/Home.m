//
//  Home.m
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Home.h"
#import "HomeCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "HomeTableViewCell.h"
#import "DetailedMenu.h"
#import "sharedclass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"
#import "RestaurantMenu.h"
#import "AppDelegate.h"
#import "RestaurantNamesVC.h"
#import "DiscoverCollectionViewCell.h"

@interface Home ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate>
{
    sharedclass *objForshare;
   // NSString *tokenString,*emailString;
    //UIAlertController *alertController;
    HomeTableViewCell *homeTableCell;
    NSArray *imgs,*names;
    
    NSString *latitudeString;
    NSString *longitudeString;
    
    NSString *combineString,*shopIdString;
    NSString *searchText,*searchString;
    
    NSString *fromString;
    NSString *toString;
    
    NSString *languageString;

    
#pragma mark Home Service Arrays
    NSMutableArray *categoriesCountArray;
    NSMutableArray *categoryIdArray;
    NSMutableArray *categoryNamesArray;
    NSMutableArray *categoryImagesArray;
    
    NSMutableArray *restaurantsCountArray;
    NSMutableArray *restaurantShopIdArray;
    NSMutableArray *restaurantCatIdArray;
    NSMutableArray *restaurantNameArray;
    NSMutableArray *restaurantImageArray;
    NSMutableArray *restaurantLogoArray;
    NSMutableArray *restaurantCatNameArray;
    NSMutableArray *restaurantFromTimeArray;
    NSMutableArray *restaurantToTimeArray;
    NSMutableArray *restaurantCombinedTimeArray;
    
    
#pragma mark Search Restaurant Service Arrays
    NSMutableArray *searchDataCountArray;
    NSMutableArray *searchItemNameArray;
    NSMutableArray *searchItemLogoArray;
    NSMutableArray *searchCatNameArray;
    NSMutableArray *searchItemTimeArray;
    
    
#pragma mark Restaurant Discover Arrays
    NSMutableArray *discoverItemCountArray;
    NSMutableArray *itemImageArray;
    NSMutableArray *itemNameArray;
    NSMutableArray *catNameArray;
    NSMutableArray *distanceArray;
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    NSMutableArray *itemIdArray;
    
    NSString *openOrCloseString;
    NSString *openString;
    NSString *closeString;
    
    //NSString *cityNameString;
   
    AppDelegate * appDelegate;
    int n;

}
@property (weak, nonatomic) IBOutlet UICollectionView *mycollectionview;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *restaurantTabelView;



@end

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];
    searchText = @"search";
    
    // Do any additional setup after loading the view.
    _searchbar.delegate = self;
    
    //    languageString = [NSString stringWithFormat:@"1"];
    //
    //    [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    latitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    longitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
   
    [self loadCustomDesign];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshMenu) name:@"refreshMenu1" object:nil];
}

-(void)refreshMenu
{
    
    if ([appDelegate.langStr isEqualToString:@"en"]) {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"en" forKey:@"language"];
        
        //[self viewDidLoad];
        [self viewWillAppear:YES];
        
    }else if([appDelegate.langStr isEqualToString:@"ar"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"ar" forKey:@"language"];
        
        ///[self viewDidLoad];
        [self viewWillAppear:YES];
    }
    
}

-(void)loadCustomDesign
{
    
    //[self homeServiceCall];
   // [self restaurantDiscoverServiceCall];
    

}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_searchbar resignFirstResponder];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchbar resignFirstResponder];
    
        if ([searchText isEqualToString:@"search"]) {
             [self homeServiceCall];
            
        }else{
            [self searchRestaurantsServiceCall];
           
        }
 
    
}// called when keyboard search button pressed


- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    searchText = [NSString stringWithFormat:@"%@",[searchBar.text stringByReplacingOccurrencesOfString:@"\n" withString:text]];
    
    if ([text isEqualToString:@""]) {
        searchText = @"search";
    }
    
    NSLog(@"String:%@",searchText);
//    if ([searchString isEqualToString:@"search"]) {
//        [self searchRestaurantsServiceCall];
//    }else{
//        [self homeServiceCall];
//    }
    
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
    _selectLocationLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Select Location"];
    _topLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Top Restaurants"];
    
    [_viewAllBtn_Outelt setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"View All"] forState:UIControlStateNormal];
    _searchbar.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Search 589 Restaurants"];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] == NULL|| [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] == NULL) {
        
        NSLog(@"viewWillAppear %@" ,[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"]);
        
        [self homeServiceCall];
        _discoverCollectionView.hidden=TRUE;
        _closeBtn_Outlet.hidden=TRUE;

        
    }else{
        
        latitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
        longitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
        _discoverCollectionView.hidden=FALSE;
        _closeBtn_Outlet.hidden=FALSE;

        [self locationBasedRestaurantsServicecall];
        [self restaurantDiscoverServiceCall];
    }
    
    
    _cityNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"cityName"]] ? [[sharedclass sharedInstance]languageSelectedStringForKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"cityName"]]:@"";
    
    [_searchbar resignFirstResponder];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
   dispatch_async(dispatch_get_main_queue(), ^{
       
       if ([appDelegate.restStr isEqualToString:@"HIDE"]) {
           
           _discoverCollectionView.hidden=TRUE;
           _closeBtn_Outlet.hidden=TRUE;
           
       }
//       else{
//           _discoverCollectionView.hidden=FALSE;
//           _closeBtn_Outlet.hidden=FALSE;
//       }
       
   });
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_sidebarButton addTarget:self.revealViewController action:@selector( revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Home Service Call
-(void)homeServiceCall

{
    languageString=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];

    //http://voliveafrica.com/togo/api/services/home?API-KEY=225143&lang=en
    NSMutableDictionary * homePostDictionary = [[NSMutableDictionary alloc]init];
    [homePostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [homePostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"home?" withPostDict:homePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ Home My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            categoriesCountArray = [NSMutableArray new];
            categoryNamesArray = [NSMutableArray new];
            categoryImagesArray = [NSMutableArray new];
            categoryIdArray = [NSMutableArray new];
            
            restaurantsCountArray = [NSMutableArray new];
            restaurantNameArray = [NSMutableArray new];
            restaurantImageArray = [NSMutableArray new];
            restaurantCatNameArray = [NSMutableArray new];
            restaurantLogoArray = [NSMutableArray new];
            restaurantShopIdArray = [NSMutableArray new];
            restaurantCatIdArray = [NSMutableArray new];
            restaurantToTimeArray = [NSMutableArray new];
            restaurantFromTimeArray = [NSMutableArray new];
            restaurantCombinedTimeArray = [NSMutableArray new];
            dispatch_async(dispatch_get_main_queue(), ^{
                
            categoriesCountArray=[dataDictionary objectForKey:@"categories"];
                
            if (categoriesCountArray.count>0) {
            for (int i=0; i<categoriesCountArray.count; i++) {
                [categoryNamesArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"category"]];
                [categoryImagesArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"image"]];
                [categoryIdArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"id"]];
                       // [subCatIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"sub_cat"]];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:categoryIdArray forKey:@"catIdArray"];
                       // [[NSUserDefaults standardUserDefaults]setObject:subCatIdArray forKey:@"subCatIdArray"];
                        NSLog(@"%@",categoryNamesArray);
                        NSLog(@"%@",categoryImagesArray);
                        NSLog(@"%@",categoryIdArray);
                       // NSLog(@"%@",subCatIdArray);
                     }
                }
                [_mycollectionview reloadData];
                
            restaurantsCountArray=[dataDictionary objectForKey:@"restaurants"];
                
            if (restaurantsCountArray.count>0) {
                for (int i=0; i<restaurantsCountArray.count; i++) {
                    [restaurantNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"name"]];
                    [restaurantImageArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"image"]];
                    [restaurantCatNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_name"]];
                    [restaurantLogoArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"logo"]];
                    [restaurantShopIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"shop_id"]];
                    [restaurantCatIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_id"]];
                    
                    
                    fromString =[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"]];
                    
                    toString = [NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                    
                    
                    NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
                    [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
                    NSDate* date1 =[[NSDate alloc]init];
                    date1=[df1 dateFromString:fromString];
                    [df1 setDateFormat:@"hh:mm a"];
                    NSString *fromDate = [df1 stringFromDate:date1];
                    
                    NSDateFormatter* df2 = [[NSDateFormatter alloc] init];
                    [df2 setDateFormat:@"dd-MM-yy hh:mm a"];
                    NSDate* date2 =[[NSDate alloc]init];
                    date2=[df2 dateFromString:toString];
                    [df2 setDateFormat:@"hh:mm a"];
                    NSString *toDate = [df2 stringFromDate:date2];

                    [restaurantCombinedTimeArray addObject:[NSString stringWithFormat:@"%@ - %@",fromDate,toDate]];
                    
                    combineString=[NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                    
                    shopIdString = [NSString stringWithFormat:@"%@",restaurantShopIdArray];
                    [restaurantToTimeArray addObject:toString];
                     [restaurantFromTimeArray addObject:fromString];
                    //[restaurantCombinedTimeArray addObject:combineString];
                    
                    
                    [[NSUserDefaults standardUserDefaults]setObject:restaurantShopIdArray forKey:@"restaurantShopId"];
                    [[NSUserDefaults standardUserDefaults]setObject:restaurantCatIdArray forKey:@"restaurantCatIdArray"];
                        NSLog(@"%@",restaurantNameArray);
                    }
                }
                
                [_restaurantTabelView reloadData];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}


#pragma mark Search Restaurants Service Call
-(void)searchRestaurantsServiceCall
{
    languageString=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/search_restaurant?API-KEY=225143&lang=en&name_search=cafe
    
    NSMutableDictionary * searchPostDictionary = [[NSMutableDictionary alloc]init];
    [searchPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [searchPostDictionary setObject:languageString forKey:@"lang"];
    [searchPostDictionary setObject:searchText forKey:@"name_search"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"search_restaurant?" withPostDict:searchPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@Search Restaurants My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            searchDataCountArray = [NSMutableArray new];
            searchItemNameArray = [NSMutableArray new];
            searchItemLogoArray = [NSMutableArray new];
            searchCatNameArray = [NSMutableArray new];
            searchItemTimeArray = [NSMutableArray new];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                searchDataCountArray=[dataDictionary objectForKey:@"data"];
                
                if (searchDataCountArray.count>0) {
                    for (int i=0; i<searchDataCountArray.count; i++) {
                        [searchItemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [searchItemLogoArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
                        [searchCatNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category_name"]];
                        combineString = [NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"to_time"]];
                        [searchItemTimeArray addObject:combineString];
                        //[[NSUserDefaults standardUserDefaults]setObject:categoryIdArray forKey:@"catIdArray"];
                        
                        NSLog(@"%@",searchItemNameArray);
                        NSLog(@"%@",searchCatNameArray);
                        
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_restaurantTabelView reloadData];
                    
                });
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

#pragma mark Location Based Restaurants

-(void)locationBasedRestaurantsServicecall{
    
    languageString=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//http://voliveafrica.com/togo/api/services/location_restaurants?API-KEY=225143&latitude=17.4082411&longitude=78.4470965&lang=en

    NSMutableDictionary * locationPostDictionary = [[NSMutableDictionary alloc]init];
    [locationPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    
    //[locationPostDictionary setObject:latitudeString forKey:@"latitude"];
    //[locationPostDictionary setObject:longitudeString forKey:@"longitude"];
    
    
    [locationPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] forKey:@"latitude"];
    [locationPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] forKey:@"longitude"];
    [locationPostDictionary setObject:languageString forKey:@"lang"];
   


    [[sharedclass sharedInstance]fetchResponseforParameter:@"location_restaurants?" withPostDict:locationPostDictionary andReturnWith:^(NSData *dataFromJson) {
    
    NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
    
    NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
    // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
    NSLog(@"%@",dataFromJson);
    NSLog(@"%@ Loction Based My Data Is",dataDictionary);
    
    if ([status isEqualToString:@"1"])
    {
        
        
        [SVProgressHUD dismiss];
        categoriesCountArray = [NSMutableArray new];
        categoryNamesArray = [NSMutableArray new];
        categoryImagesArray = [NSMutableArray new];
        categoryIdArray = [NSMutableArray new];
        
        restaurantsCountArray = [NSMutableArray new];
        restaurantNameArray = [NSMutableArray new];
        restaurantImageArray = [NSMutableArray new];
        restaurantCatNameArray = [NSMutableArray new];
        restaurantLogoArray = [NSMutableArray new];
        restaurantShopIdArray = [NSMutableArray new];
        restaurantCatIdArray = [NSMutableArray new];
        restaurantToTimeArray = [NSMutableArray new];
        restaurantFromTimeArray = [NSMutableArray new];
        restaurantCombinedTimeArray = [NSMutableArray new];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            categoriesCountArray=[dataDictionary objectForKey:@"categories"];
            
            if (categoriesCountArray.count>0) {
                for (int i=0; i<categoriesCountArray.count; i++) {
                    [categoryNamesArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"category"]];
                    [categoryImagesArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"image"]];
                    [categoryIdArray addObject:[[[dataDictionary objectForKey:@"categories"]objectAtIndex:i]objectForKey:@"id"]];
                    // [subCatIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"sub_cat"]];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:categoryIdArray forKey:@"catIdArray"];
                    // [[NSUserDefaults standardUserDefaults]setObject:subCatIdArray forKey:@"subCatIdArray"];
                    NSLog(@"%@",categoryNamesArray);
                    NSLog(@"%@",categoryImagesArray);
                    NSLog(@"%@",categoryIdArray);
                    // NSLog(@"%@",subCatIdArray);
                }
            }
            [_mycollectionview reloadData];
            
            restaurantsCountArray=[dataDictionary objectForKey:@"restaurants"];
            
            if (restaurantsCountArray.count>0) {
                for (int i=0; i<restaurantsCountArray.count; i++) {
                    [restaurantNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"name"]];
                    [restaurantImageArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"image"]];
                    [restaurantCatNameArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_name"]];
                    [restaurantLogoArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"logo"]];
                    [restaurantShopIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"shop_id"]];
                    [restaurantCatIdArray addObject:[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"category_id"]];
                    
                    
                    fromString =[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"]];
                    
                    toString = [NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                    
                    
                    NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
                    [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
                    NSDate* date1 =[[NSDate alloc]init];
                    date1=[df1 dateFromString:fromString];
                    [df1 setDateFormat:@"hh:mm a"];
                    NSString *fromDate = [df1 stringFromDate:date1];
                    
                    NSDateFormatter* df2 = [[NSDateFormatter alloc] init];
                    [df2 setDateFormat:@"dd-MM-yy hh:mm a"];
                    NSDate* date2 =[[NSDate alloc]init];
                    date2=[df2 dateFromString:toString];
                    [df2 setDateFormat:@"hh:mm a"];
                    NSString *toDate = [df2 stringFromDate:date2];
                    
                    [restaurantCombinedTimeArray addObject:[NSString stringWithFormat:@"%@ - %@",fromDate,toDate]];
                    
                    combineString=[NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"restaurants"]objectAtIndex:i]objectForKey:@"to_time"]];
                    
                    shopIdString = [NSString stringWithFormat:@"%@",restaurantShopIdArray];
                    [restaurantToTimeArray addObject:toString];
                    [restaurantFromTimeArray addObject:fromString];
                    //[restaurantCombinedTimeArray addObject:combineString];
                    
                    
                    [[NSUserDefaults standardUserDefaults]setObject:restaurantShopIdArray forKey:@"restaurantShopId"];
                    [[NSUserDefaults standardUserDefaults]setObject:restaurantCatIdArray forKey:@"restaurantCatIdArray"];
                    NSLog(@"%@",restaurantNameArray);
                }
            }
            
            [_restaurantTabelView reloadData];
            
            
        });
    }else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
        });
    }
    
}];
}


#pragma mark Restaurant Discover Service Call

-(void)restaurantDiscoverServiceCall{
    
    languageString=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    /*
     API-KEY:225143
     user_id:34534543
     latitude:17.41957092
     longitude:78.44827271
     lang:en
     */
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/restaurant_discover";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * discoverPostDictionary = [[NSMutableDictionary alloc]init];
    [discoverPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [discoverPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] : @"" forKey:@"user_id"];
    
   // [discoverPostDictionary setObject:@"17.41957092" forKey:@"latitude"];
   // [discoverPostDictionary setObject:@"78.44827271" forKey:@"longitude"];
    
    [discoverPostDictionary setObject:latitudeString ? latitudeString : @"" forKey:@"latitude"];
    [discoverPostDictionary setObject:longitudeString ? longitudeString : @"" forKey:@"longitude"];
    [discoverPostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"restaurant_discover" withPostDict:discoverPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        
        if (dict) {
            NSLog(@"Discover Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                discoverItemCountArray = [NSMutableArray new];
                itemImageArray = [NSMutableArray new];
                itemNameArray = [NSMutableArray new];
                catNameArray = [NSMutableArray new];
                distanceArray = [NSMutableArray new];
                latitudeArray = [NSMutableArray new];
                longitudeArray = [NSMutableArray new];
                itemIdArray = [NSMutableArray new];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    discoverItemCountArray = [dict objectForKey:@"data"];
                    if (discoverItemCountArray.count > 0) {
                        
                        for (int i=0; i<discoverItemCountArray.count; i++) {
                            
                            [itemImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
                            [itemNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                            [catNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category"]];
                            [itemIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                            
                            [distanceArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"distance"]];
                            [latitudeArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"latitude"]];
                            [longitudeArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"longitude"]];
                        }
                    }else
                    {
                        [SVProgressHUD dismiss];
                        
                        _discoverCollectionView.hidden = TRUE;
                        _closeBtn_Outlet.hidden = TRUE;
                        
                        [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
                    }

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_discoverCollectionView reloadData];
                        
                    });
                    
                });
            }
            else
            {
                [SVProgressHUD dismiss];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                _discoverCollectionView.hidden = TRUE;
                   _closeBtn_Outlet.hidden = TRUE;
                    
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
                });
                
            }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _discoverCollectionView.hidden = TRUE;
                _closeBtn_Outlet.hidden = TRUE;

                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"data"] onViewController:self completion:nil];
            });
        }
    }];
    
}


#pragma mark - Tableview delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([searchText isEqualToString:@"search"]) {
        
        return restaurantNameArray.count;
        
    }else{
        
        return searchDataCountArray.count;
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    homeTableCell = [_restaurantTabelView dequeueReusableCellWithIdentifier:@"homeTableViewCell"];
       if(homeTableCell == nil)
    {
        homeTableCell = [[HomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"homeTableViewCell"];
    }
    if ([searchText isEqualToString:@"search"]) {
        
        [homeTableCell.restaurantimg sd_setImageWithURL:[NSURL URLWithString:[restaurantLogoArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
        homeTableCell.name.text = [restaurantNameArray objectAtIndex:indexPath.row];
        homeTableCell.restaurantCatNameLabel.text = [restaurantCatNameArray objectAtIndex:indexPath.row];
        homeTableCell.timeLabel.text = [restaurantCombinedTimeArray objectAtIndex:indexPath.row];
        
    
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yy hh:mm a"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        
        //FromTime
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [df setTimeZone:[NSTimeZone systemTimeZone]];
        [df setDateFormat:@"dd-MM-yy hh:mm a"];
        NSDate* newDate =[[NSDate alloc]init];
        newDate=[df dateFromString:[restaurantFromTimeArray objectAtIndex:indexPath.row]];
        [df setDateFormat:@"dd-MM-yy hh:mm a"];
        NSString *reqFromTimeString = [df stringFromDate:newDate];
        
        //ToTime
        NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
        [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [df1 setTimeZone:[NSTimeZone systemTimeZone]];
        [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
        NSDate* newDate1 =[[NSDate alloc]init];
        newDate1=[df1 dateFromString:[restaurantToTimeArray objectAtIndex:indexPath.row]];
        [df setDateFormat:@"dd-MM-yy hh:mm a"];
        NSString *reqToTimeString = [df stringFromDate:newDate1];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yy hh:mm a"];
        
        NSDate *date1= [formatter dateFromString:reqFromTimeString];
        NSDate *date2 = [formatter dateFromString:reqToTimeString];
        NSDate *date3 = [formatter dateFromString:resultString];
        
        NSComparisonResult FromComparison = [date3 compare:date1];
        NSComparisonResult ToComparison = [date3 compare:date2];
        
        if(FromComparison == NSOrderedDescending)
        {
            NSLog(@"date3 is later than date1");
            if (ToComparison==NSOrderedDescending) {
                
                NSLog(@"date3 is later than date2");
                NSLog(@"Sorry, we are closed!");
                
                [homeTableCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
                
            }
            else if (ToComparison==NSOrderedAscending)
            {
                
                NSLog(@"date2 is later than date3");
                NSLog(@"We are Open!");
                
            [homeTableCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"] forState:UIControlStateNormal];
                
            }
        }
        else if(FromComparison == NSOrderedAscending)
        {
            NSLog(@"date1 is later than date3");
            NSLog(@"Sorry, we are closed!");
            
            [homeTableCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
                    }
        else
        {
            NSLog(@"date3 is equal to date1");
            NSLog(@"Sorry, we are closed!");
            
            [homeTableCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
           
        }
        
        return homeTableCell;
    }
    
    else{
        [homeTableCell.restaurantimg sd_setImageWithURL:[NSURL URLWithString:[searchItemLogoArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
        homeTableCell.name.text = [searchItemNameArray objectAtIndex:indexPath.row];
        homeTableCell.restaurantCatNameLabel.text = [searchCatNameArray objectAtIndex:indexPath.row];
        homeTableCell.timeLabel.text = [searchItemTimeArray objectAtIndex:indexPath.row];
    
    return homeTableCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HomeTableViewCell *cell = (HomeTableViewCell *)[_restaurantTabelView cellForRowAtIndexPath:indexPath];
    
    
    if ([cell.openOrCloseBtn_Outlet.titleLabel.text isEqualToString:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"]]) {
        
       // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"latitude"];
        
       // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"longitude"];
        NSString *open = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"];
        
        RestaurantMenu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantMenu"];
        [[NSUserDefaults standardUserDefaults]setObject:[restaurantShopIdArray objectAtIndex:indexPath.row] forKey:@"shopId"];
        [menu restaurantMenuServiceCall:[restaurantShopIdArray objectAtIndex:indexPath.row]];
        menu.openString = open;
        menu.shopIdStr1=[restaurantShopIdArray objectAtIndex:indexPath.row];
        menu.selected_catID = [restaurantCatIdArray objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:menu animated:TRUE];
    }
    else if ([cell.openOrCloseBtn_Outlet.titleLabel.text isEqualToString:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"]]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!" message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"This Shop is closed"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }


 //[self performSegueWithIdentifier:@"homeToDetails" sender:self];
}

#pragma mark - Collectionview Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _mycollectionview) {
        
        return categoryNamesArray.count;
    }else{
        return discoverItemCountArray.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == _mycollectionview) {
        
        HomeCollectionViewCell *cell = [_mycollectionview dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];
        
        [cell.listimg sd_setImageWithURL:[NSURL URLWithString:[categoryImagesArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"img4"]];
        
        cell.nameLbl.text = [categoryNamesArray objectAtIndex:indexPath.row];
        
        return cell;
    }else{
        
    }
    DiscoverCollectionViewCell *discoverCell = [_discoverCollectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [discoverCell.itemImageView sd_setImageWithURL:[NSURL URLWithString:[itemImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"img4"]];
    discoverCell.itemNameLabel.text = [itemNameArray objectAtIndex:indexPath.row];
    discoverCell.categoryNameLabel.text = [catNameArray objectAtIndex:indexPath.row];
    //discoverCell.contentView.layer.borderColor = [[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0] CGColor];
//    
   // discoverCell.contentView.layer.borderWidth = 1.0f;

    
    return discoverCell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _mycollectionview) {
        
        return CGSizeMake((_mycollectionview.frame.size.width/3)-10, _mycollectionview.frame.size.height);
        
    }else{
        
         return CGSizeMake(80.0,120.0);
    }
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   // n = indexPath.row;
    if (collectionView == _mycollectionview) {
        
        DetailedMenu *details = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailedMenu"];
        [details detailedMenuServiceCall:[categoryIdArray objectAtIndex:indexPath.row]];
        
       // details.categoryIdArray = categoryIdArray;
        [self.navigationController pushViewController:details animated:TRUE];
    }else{
        
        RestaurantMenu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantMenu"];
        
        [[NSUserDefaults standardUserDefaults]setObject:[itemIdArray objectAtIndex:indexPath.row] forKey:@"shopId"];
        [menu restaurantMenuServiceCall:[itemIdArray objectAtIndex:indexPath.row]];
        menu.selected_catID = [itemIdArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:menu animated:TRUE];

    }
   
}

#pragma mark View All Button
- (IBAction)ViewAll:(UIButton *)sender {
    
    RestaurantNamesVC *restaurantsDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantNamesVC"];
    [self.navigationController pushViewController:restaurantsDetails animated:TRUE];
    
}



- (IBAction)close_BTN:(id)sender {
    
    appDelegate.restStr=@"HIDE";
    
    _discoverCollectionView.hidden = TRUE;
    _closeBtn_Outlet.hidden = TRUE;
}
@end
