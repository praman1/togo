//
//  DiscoverCollectionViewCell.h
//  TO GO
//
//  Created by volive solutions on 05/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@end
