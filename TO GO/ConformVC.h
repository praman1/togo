//
//  ConformVC.h
//  TO GO
//
//  Created by volive solutions on 26/02/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConformVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UITextField *timeTextfield;
- (IBAction)saveBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn_Outlet;
- (IBAction)cancelBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIView *conformView;
@property NSString *userId,*orderId;

@end
