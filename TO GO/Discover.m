//
//  Discover.m
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Discover.h"
#import "SWRevealViewController.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "DiscoverCollectionViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "RestaurantMenu.h"


@interface Discover ()<UICollectionViewDelegate,UICollectionViewDataSource,GMSMapViewDelegate,UISearchBarDelegate>
{
    NSMutableArray *discoverItemCountArray;
    NSMutableArray *itemImageArray;
    NSMutableArray *itemNameArray;
    NSMutableArray *catNameArray;
    NSMutableArray *distanceArray;
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    NSMutableArray *itemIdArray;
    
    NSMutableArray *searchDataCountArray;
    NSMutableArray *searchItemNameArray;
    NSMutableArray *searchItemLogoArray;
    NSMutableArray *searchCatNameArray;
    NSMutableArray *searchItemTimeArray;
    
    NSString *latitudeString;
    NSString *longitudeString;
    
    NSString *currentLat;
    NSString *currentLong;
    
    NSString *searchText,*searchString;
    NSString *languageString;
    
    DiscoverCollectionViewCell *discoverCell;
    CLLocationCoordinate2D hydeParkLocation;
    
    GMSCameraPosition *cameraPosition;
    GMSMapView *googleMapView;
    GMSMarker *marker;
    GMSCircle *geoFenceCircle;
    GMSPolygon *polygon;
}

@end

@implementation Discover

- (void)viewDidLoad {
    [super viewDidLoad];
    
     searchText = @"search";
    
     _search.delegate = self;
    
    latitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    longitudeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    _discoverLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Discover"];
    
    [_viewBestSellersBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"View Best Sellers"] forState:UIControlStateNormal];
    _search.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Search 589 Restaurants"];
    
    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Discover"];


    
    [self restaurantDiscoverServiceCall];
    [self loadMaps];
    
   

    // Do any additional setup after loading the view.
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadMaps {
    
    googleMapView.delegate = self;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitudeString doubleValue]
                                                            longitude:[longitudeString doubleValue]
                                                                 zoom:10];
    googleMapView = [GMSMapView mapWithFrame:_discoverMapView.bounds camera:camera];
    
    googleMapView.settings.myLocationButton = NO;
    googleMapView.camera = camera;
    
    googleMapView.myLocationEnabled = YES;
    // _successMapView = googleMapView;
    
    [_discoverMapView addSubview:googleMapView];
    
    
    geoFenceCircle = [[GMSCircle alloc] init];
    geoFenceCircle.position = CLLocationCoordinate2DMake([latitudeString doubleValue], [longitudeString doubleValue]);
    geoFenceCircle.strokeWidth = 0.5;
    geoFenceCircle.strokeColor = [UIColor redColor];
    //geoFenceCircle.fillColor = [UIColor redColor];
    NSLog(@"%@",geoFenceCircle);
    geoFenceCircle.radius =2000.00;
    geoFenceCircle.map = googleMapView;
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([latitudeString doubleValue], [longitudeString doubleValue]);
    marker.tracksViewChanges = YES;
    marker.map = googleMapView;
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_search resignFirstResponder];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_search resignFirstResponder];
    
    if ([searchText isEqualToString:@"search"]) {
        [self restaurantDiscoverServiceCall];
        
    }else{
        [self searchRestaurantsServiceCall];
        
    }
    
    
}// called when keyboard search button pressed


- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    searchText = [NSString stringWithFormat:@"%@",[searchBar.text stringByReplacingOccurrencesOfString:@"\n" withString:text]];
    
    if ([text isEqualToString:@""]) {
        searchText = @"search";
    }
    
    NSLog(@"String:%@",searchText);
    //    if ([searchString isEqualToString:@"search"]) {
    //        [self searchRestaurantsServiceCall];
    //    }else{
    //        [self homeServiceCall];
    //    }
    
    return YES;
}


-(void)searchRestaurantsServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/search_restaurant?API-KEY=225143&lang=en&name_search=cafe
    
    NSMutableDictionary * searchPostDictionary = [[NSMutableDictionary alloc]init];
    [searchPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [searchPostDictionary setObject:languageString forKey:@"lang"];
    [searchPostDictionary setObject:searchText forKey:@"name_search"];

    [[sharedclass sharedInstance]fetchResponseforParameter:@"search_restaurant?" withPostDict:searchPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            searchDataCountArray = [NSMutableArray new];
            searchItemNameArray = [NSMutableArray new];
            searchItemLogoArray = [NSMutableArray new];
            searchCatNameArray = [NSMutableArray new];
            searchItemTimeArray = [NSMutableArray new];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                searchDataCountArray=[dataDictionary objectForKey:@"data"];
                
                if (searchDataCountArray.count>0) {
                    for (int i=0; i<searchDataCountArray.count; i++) {
                        [searchItemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [searchItemLogoArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
                        [searchCatNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category_name"]];
                        
                        //[[NSUserDefaults standardUserDefaults]setObject:categoryIdArray forKey:@"catIdArray"];
                        
                        NSLog(@"%@",searchItemNameArray);
                        NSLog(@"%@",searchCatNameArray);
                        
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_discoverCollectionView reloadData];
                    
                });
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}



-(void)restaurantDiscoverServiceCall{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
/*
API-KEY:225143
user_id:34534543
latitude:17.41957092
longitude:78.44827271
lang:en
 */
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/restaurant_discover";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * discoverPostDictionary = [[NSMutableDictionary alloc]init];
    [discoverPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [discoverPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [discoverPostDictionary setObject:latitudeString ? latitudeString : @"" forKey:@"latitude"];
    [discoverPostDictionary setObject:longitudeString ? longitudeString : @"" forKey:@"longitude"];
    [discoverPostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"restaurant_discover" withPostDict:discoverPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    discoverItemCountArray = [NSMutableArray new];
                    itemImageArray = [NSMutableArray new];
                    itemNameArray = [NSMutableArray new];
                    catNameArray = [NSMutableArray new];
                    distanceArray = [NSMutableArray new];
                    latitudeArray = [NSMutableArray new];
                    longitudeArray = [NSMutableArray new];
                    itemIdArray = [NSMutableArray new];

            dispatch_async(dispatch_get_main_queue(), ^{
                discoverItemCountArray = [dict objectForKey:@"data"];
                
                if (discoverItemCountArray.count > 0) {
                        
                for (int i=0; i<discoverItemCountArray.count; i++) {
                            
                [itemImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
                [itemNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                [catNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category"]];
                [itemIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                    
                [distanceArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"distance"]];
                [latitudeArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"latitude"]];
                [longitudeArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"longitude"]];
                    }
                }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_discoverCollectionView reloadData];
                            
                        });
                     });
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];

}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if ([searchText isEqualToString:@"search"]) {
        
        return discoverItemCountArray.count;
        
    }else{
        return searchDataCountArray.count;
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    discoverCell = [_discoverCollectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if ([searchText isEqualToString:@"search"]) {
        
        [discoverCell.itemImageView sd_setImageWithURL:[NSURL URLWithString:[itemImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
        discoverCell.itemNameLabel.text = [itemNameArray objectAtIndex:indexPath.row];
        discoverCell.categoryNameLabel.text = [catNameArray objectAtIndex:indexPath.row];
        discoverCell.contentView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        discoverCell.contentView.layer.borderWidth = 1.0f;
        
        return discoverCell;
    }else{
        [discoverCell.itemImageView sd_setImageWithURL:[NSURL URLWithString:[searchItemLogoArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
        discoverCell.itemNameLabel.text = [searchItemNameArray objectAtIndex:indexPath.row];
        discoverCell.categoryNameLabel.text = [searchCatNameArray objectAtIndex:indexPath.row];
        discoverCell.contentView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        discoverCell.contentView.layer.borderWidth = 1.0f;

        return discoverCell;
    }
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RestaurantMenu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantMenu"];
    
    [[NSUserDefaults standardUserDefaults]setObject:[itemIdArray objectAtIndex:indexPath.row] forKey:@"shopId"];
    [menu restaurantMenuServiceCall:[itemIdArray objectAtIndex:indexPath.row]];
    menu.selected_catID = [itemIdArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:menu animated:TRUE];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(80.0,120.0);
}



@end
