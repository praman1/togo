//
//  SelectLanguage.h
//  TO GO
//
//  Created by volive solutions on 25/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLanguage : UIViewController
- (IBAction)english_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *englishBtn_Outlet;
- (IBAction)arabic_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *arabicBtn_Outlet;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;


@end
