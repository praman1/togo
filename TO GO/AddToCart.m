     //
//  AddToCart.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "AddToCart.h"
#import "AddToCartCollectionViewCell.h"
#import "Add.h"
#import "TabCollectionViewCell.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AddCartTableViewCell.h"
@interface AddToCart ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
{
    NSArray *titleArray;
    NSMutableArray *ForAddBtn;
    //AddToCartCollectionViewCell *cell;
    TabCollectionViewCell *tabCell;
    NSMutableArray *selectUnselectArray;
    NSMutableArray *menuNameArray,*menuIdArray;
    NSMutableArray *itemsCountArray,*itemIdArray,*itemNameArray,*itemImageArray,*itemCostArray;
    UIView *bView;
    NSString *itemIdString,*menuIdString;
    NSMutableArray *shopIdArray;
    Add *Addobj;
    
    NSString *languageString;
    
    
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewout;
@property (weak, nonatomic) IBOutlet UICollectionView *mycollectionview;

@end

@implementation AddToCart

- (void)viewDidLoad {
    [super viewDidLoad];
    selectUnselectArray = [[NSMutableArray  alloc]initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
   // menuNameArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"menuNameArray"];
    //menuIdArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"menuIdArray"];
    shopIdArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"restaurantShopId"];
    NSString *shpId = [[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"];
  //  [self repeatServiceCall:shpId];
// Do any additional setup after loading the view.
    self.title = @"Starbucks";
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    
    UIBarButtonItem *anotherButton1 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"white search"] style:UIBarButtonItemStylePlain target:self action:nil];
    anotherButton1.tintColor = [UIColor whiteColor];
    
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.rightBarButtonItem = anotherButton1;
    
  //  [self restaurantItemsServiceCall:menuIdString];
    
   // titleArray = @[@"Featured Drinks",@"Brewed Coffee",@"Cool Drink"];
    ForAddBtn = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
  
    
    
     Addobj = [self.storyboard instantiateViewControllerWithIdentifier:@"Addobj"];
    
    [_tabCollectionView reloadData];
    
}
-(void)back:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
// then ...

//- (void)buttonClicked:(UIButton*)sender
//{
//    
//    NSLog(@"X - %f : Y -(long) %f",sender.frame.origin.x,sender.frame.origin.y);
//    bView.frame = CGRectMake(sender.frame.origin.x, 40, sender.frame.size.width, 1.5);
//    
//}


-(void)repeatServiceCall:(NSString *)menuId
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // menuIdString = menuId;
    // http://voliveafrica.com/togo/api/services/items?API-KEY=225143&lang=en&shop_id=1&menu_id=2
    NSMutableDictionary * restaurantPostDictionary = [[NSMutableDictionary alloc]init];
    [restaurantPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [restaurantPostDictionary setObject:languageString forKey:@"lang"];
    //[restaurantPostDictionary setObject:_shopIdStr forKey:@"shop_id"];
    [restaurantPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    //[restaurantPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"restaurantShopId"] forKey:@"shop_id"];
    [restaurantPostDictionary setObject:menuId forKey:@"menu_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"items?" withPostDict:restaurantPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                itemsCountArray = [NSMutableArray new];
                itemIdArray = [NSMutableArray new];
                itemNameArray = [NSMutableArray new];
                itemImageArray = [NSMutableArray new];
                itemCostArray = [NSMutableArray new];
                
                itemsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (itemsCountArray.count>0) {
                    
                    for (int i=0; i<itemsCountArray.count; i++) {
                        [itemIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [itemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [itemImageArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [itemCostArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"cost"]];
                        
                      //  itemIdString = [NSString stringWithFormat:@"%@",itemIdArray];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:itemIdString forKey:@"itemId"];
                        
                        NSLog(@"%@",itemNameArray);
                        NSLog(@"%@",itemCostArray);
                        
                    }
                    [_mycollectionview reloadData];
                }
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)dismiss{
    NSLog(@"clicked");
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [Addobj.view setHidden:YES];
                     }
                     completion:^(BOOL finished){
                      //[Addobj.view setHidden:YES];
                     }];

    [self addToCartServiceCall];

}

-(void)addToCartServiceCall
{
    //    API-KEY:225143
    //    user_id:134721528
    //    vendor_id:3288062319
    //    category_id:4
    //    shop_id:1
    //    item_id:3
    //    item_count:2
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/add_cart
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * cartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [cartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [cartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"] forKey:@"vendor_id"];
    [cartPostDictionary setObject:self.selected_catID forKey:@"category_id"];
    [cartPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    [cartPostDictionary setObject:itemIdString forKey:@"item_id"];
    [cartPostDictionary setValue:@1 forKey:@"item_count"];
    [cartPostDictionary setValue:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_cart" withPostDict:cartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                else
                {
                    [SVProgressHUD dismiss];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Error"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *yes = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Yes"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        [self deleteCartService];
                        
                    }];
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert addAction:yes];
                    [alert addAction:cancel];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
            });
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:@"Message" withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(void)deleteCartService{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/delete_cart?API-KEY=225143&user_id=134721528
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * deleteDictionary = [[NSMutableDictionary alloc]init];
    [deleteDictionary setObject:APIKEY forKey:@"API-KEY"];
    [deleteDictionary setObject:languageString forKey:@"lang"];
    [deleteDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"delete_cart?" withPostDict:deleteDictionary andReturnWith:^(NSData *dataFromJson) {
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:^{
                    [self.navigationController popViewControllerAnimated:YES];

                }];
            }
            else
            {
                [SVProgressHUD dismiss];

                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            }});
        
        
        
    }];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
   
    //[self restaurantItemsServiceCall:menuIdString];
}

#pragma mark - collectionview methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _mycollectionview) {
        return itemIdArray.count;
    }
    else{
        return menuNameArray.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AddToCartCollectionViewCell *cartCell;
    if (collectionView ==  _mycollectionview) {
        cartCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        [cartCell.menuImageView sd_setImageWithURL:[NSURL URLWithString:[itemImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
        cartCell.menuNameLabel.text = [itemNameArray objectAtIndex:indexPath.row];
        cartCell.menuPriceLabel.text = [itemCostArray objectAtIndex:indexPath.row];

      //  cartCell.view.value = 1;
        
        [cartCell.view addTarget:self action:@selector(stepperAction:) forControlEvents:UIControlEventValueChanged];
        
        
        cartCell.AddBtn.tag = indexPath.row;
        
        cartCell.plusbtn.tag = indexPath.row-100;
        
        cartCell.minusbtn.tag = indexPath.row+100;
        
        cartCell.AddBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cartCell.AddBtn.layer.borderWidth = 1;
        
        cartCell.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cartCell.view.layer.borderWidth = 1;
        
        if ([[ForAddBtn objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cartCell.view.hidden = YES;
            cartCell.AddBtn.hidden = NO;
        }else{
            cartCell.qty.text = [NSString stringWithFormat:@"%@",ForAddBtn[indexPath.row]];
            cartCell.view.hidden = NO;
            cartCell.AddBtn.hidden = YES;
        }
        return cartCell;

    }
    else{
        tabCell = [_tabCollectionView dequeueReusableCellWithReuseIdentifier:@"TabViewCell" forIndexPath:indexPath];
        tabCell.itemNameLabel.text = [menuNameArray objectAtIndex:indexPath.row];
        
        if ([[selectUnselectArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            
            tabCell.itemView.hidden = NO;
            
        } else {
            
            tabCell.itemView.hidden = YES;
            
        }
        
        
        return tabCell;

        
    }
//    if ([[ForAddBtn objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
//        cartCell.view.hidden = YES;
//        cartCell.AddBtn.hidden = NO;
//    }else{
//    cartCell.qty.text = [NSString stringWithFormat:@"%@",ForAddBtn[indexPath.row]];
//    cartCell.view.hidden = NO;
//    cartCell.AddBtn.hidden = YES;
//    }
    
    
    return cartCell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _tabCollectionView) {
        
        selectUnselectArray = [[NSMutableArray  alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
        
        [selectUnselectArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
        
        [self restaurantItemsServiceCall:[menuIdArray objectAtIndex:indexPath.row]];
    }
    else{
        
        itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    }
}

-(void)restaurantItemsServiceCall:(NSString *)shopId{
    
   // shopIdString = shopId;
    
     languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    // http://voliveafrica.com/togo/api/services/restaurant_details?API-KEY=225143&lang=en&shop_id=5
    NSMutableDictionary * restaurantPostDictionary = [[NSMutableDictionary alloc]init];
    [restaurantPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [restaurantPostDictionary setObject:languageString forKey:@"lang"];
    [restaurantPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"restaurant_details?" withPostDict:restaurantPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                NSArray *menus =[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"];
                menuNameArray = [NSMutableArray new];
                menuIdArray = [NSMutableArray new];
                
                for (int i=0; i<menus.count; i++) {
                    
                    [menuIdArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"]objectAtIndex:i]objectForKey:@"menu_id"]];
                    [menuNameArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"menus"]objectAtIndex:i]objectForKey:@"menu"]];
                    
                    
                }
                [_tabCollectionView reloadData];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    
    /*
    menuIdString = menuId;
    // http://voliveafrica.com/togo/api/services/items?API-KEY=225143&lang=en&shop_id=1&menu_id=2
    NSMutableDictionary * restaurantPostDictionary = [[NSMutableDictionary alloc]init];
    [restaurantPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [restaurantPostDictionary setObject:@"en" forKey:@"lang"];
    //[restaurantPostDictionary setObject:_shopIdStr forKey:@"shop_id"];
    [restaurantPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    //[restaurantPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"restaurantShopId"] forKey:@"shop_id"];
    [restaurantPostDictionary setObject:menuId forKey:@"menu_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"items?" withPostDict:restaurantPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                itemsCountArray = [NSMutableArray new];
                itemIdArray = [NSMutableArray new];
                itemNameArray = [NSMutableArray new];
                itemImageArray = [NSMutableArray new];
                itemCostArray = [NSMutableArray new];
                
                
                itemsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (itemsCountArray.count>0) {
                    
                    for (int i=0; i<itemsCountArray.count; i++) {
                        [itemIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [itemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [itemImageArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [itemCostArray  addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"cost"]];
                        
                        itemIdString = [NSString stringWithFormat:@"%@",itemIdArray];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:itemIdString forKey:@"itemId"];
                        
                        NSLog(@"%@",itemNameArray);
                        NSLog(@"%@",itemCostArray);
                        
                    }
                    [_mycollectionview reloadData];
                }
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    */
}

- (IBAction)stepperClicked:(id)sender {
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _mycollectionview) {
        CGFloat width = (CGFloat) (_mycollectionview.frame.size.width/2);
        
        return CGSizeMake(width-15,215);
    }
    else{
        CGFloat width = (CGFloat) (_tabCollectionView.frame.size.width/2);
        
        return CGSizeMake(width-5,45);
    }
}
-(void)stepperAction:(id)sender{
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    AddToCartCollectionViewCell *datasetCell =
    (AddToCartCollectionViewCell *)[self.mycollectionview cellForItemAtIndexPath:indexPath];
    
    int value = datasetCell.view.value;
    [self updateCart:[NSString stringWithFormat:@"%d",value]];
    
    
    

    
}

-(void)updateCart:(NSString *)stepperValue{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/update_cart?API-KEY=225143&user_id=134721528&item_id=3&item_count=1

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    [SVProgressHUD dismiss];
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    [viewCartPostDictionary setObject:itemIdString forKey:@"item_id"];
    [viewCartPostDictionary setObject:stepperValue forKey:@"item_count"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"update_cart?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
            }
        });
        
    }];
    
}
- (IBAction)AddToCart:(UIButton *)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mycollectionview];
    NSIndexPath *indexPath = [self.mycollectionview indexPathForItemAtPoint:btnPosition];
    
    itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    
    NSLog(@"%ld",(long)sender.tag);
    [ForAddBtn replaceObjectAtIndex:sender.tag withObject:@"1"];
    [_mycollectionview reloadData];
    
    [UIView animateWithDuration:1.0 delay:0.2 options: UIViewAnimationOptionCurveEaseInOut animations:^{
         
         Addobj.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
         [Addobj.add addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
         [self.view addSubview:Addobj.view];
         Addobj.view.layer.cornerRadius = 5;
         [Addobj.view setHidden:NO];
     
     }
     completion:^(BOOL finished){
     //[Addobj.view setHidden:NO];
     }];
    
}

- (IBAction)plus:(UIButton *)sender {
    
    NSLog(@"%ld",sender.tag+100);
    
    NSString *str = [NSString stringWithFormat:@"%@",ForAddBtn[sender.tag+100]];
    
    int i = [str intValue];
    i = i+1;
    
   [ForAddBtn replaceObjectAtIndex:sender.tag+100 withObject:[NSString stringWithFormat:@"%d",i]];
    
    [_mycollectionview reloadData];
    
    
}

- (IBAction)minus:(UIButton *)sender {
    NSLog(@"%ld",sender.tag-100);
    NSString *str = [NSString stringWithFormat:@"%@",ForAddBtn[sender.tag-100]];
    int i = [str intValue];
    i = i-1;
    
    [ForAddBtn replaceObjectAtIndex:sender.tag-100 withObject:[NSString stringWithFormat:@"%d",i]];
    
    [_mycollectionview reloadData];
    
}

-(void)viewCartServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/cart_items?API-KEY=225143&user_id=134721528&lang=en
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    

    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"cart_items?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
//            searchDataCountArray = [NSMutableArray new];
//            searchItemNameArray = [NSMutableArray new];
//            searchItemLogoArray = [NSMutableArray new];
//            searchCatNameArray = [NSMutableArray new];
//            searchItemTimeArray = [NSMutableArray new];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               // searchDataCountArray=[dataDictionary objectForKey:@"data"];
                
//                if (searchDataCountArray.count>0) {
//                    for (int i=0; i<searchDataCountArray.count; i++) {
//                        [searchItemNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
//                        [searchItemLogoArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
//                        [searchCatNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category_name"]];
//                        combineString = [NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"to_time"]];
//                        [searchItemTimeArray addObject:combineString];
//                        //[[NSUserDefaults standardUserDefaults]setObject:categoryIdArray forKey:@"catIdArray"];
//                        
//                        NSLog(@"%@",searchItemNameArray);
//                        NSLog(@"%@",searchCatNameArray);
//                        
//                    }
//                }
                //[_restaurantTabelView reloadData];
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    
}
@end
