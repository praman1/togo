//
//  HomeCollectionViewCell.h
//  TO GO
//
//  Created by Volive solutions on 11/3/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *listimg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end
