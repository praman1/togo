//
//  AddToCartCollectionViewCell.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"

@interface AddToCartCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet ANStepperView *view;

@property (weak, nonatomic) IBOutlet UIButton *AddBtn;
@property (weak, nonatomic) IBOutlet UILabel *qty;
@property (weak, nonatomic) IBOutlet UIButton *plusbtn;
@property (weak, nonatomic) IBOutlet UIButton *minusbtn;
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuCatNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuSubPriceLabel;
@end
