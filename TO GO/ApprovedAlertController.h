//
//  ApprovedAlertController.h
//  TO GO
//
//  Created by volive solutions on 16/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApprovedAlertController : UIViewController
- (IBAction)closeBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *approvedLbl;
@property (weak, nonatomic) IBOutlet UIView *approvedView;

@property (weak, nonatomic) IBOutlet UIImageView *aprrovedImg;
@property (weak, nonatomic) IBOutlet UILabel *successLbl
;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@end
