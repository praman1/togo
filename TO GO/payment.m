//
//  payment.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "payment.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AddnewCard.h"
#import "paymentCell.h"
#import "success.h"
#import "OrderHistory.h"


@interface payment ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSArray *cards;
    NSMutableArray *card_namesArray;
    NSMutableArray *card_numbersArray;
    NSMutableArray *card_idsArray;
    NSMutableArray *cart_itemsArray;
    NSMutableArray *sent_cartItmesArray;
    NSMutableArray *selected_array;
    NSString *dateString,*orderTotalString,*payByCashString,*paymentTypeString,*shopLatitudeString,*shopLongitudeString,*transactionIdString,*codString,*cardIDString;
    NSString *amountString;
    paymentCell *cell ;

}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;

@end

@implementation payment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    amountString = [[NSUserDefaults standardUserDefaults]objectForKey:@"totalAmount"];
    
    
    cards = @[@"",@""];
    _mytableview.tableFooterView = [[UIView alloc] init];
    sent_cartItmesArray = [[NSMutableArray alloc]init];
    [self getServiceCall];
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-100,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,320,560)];
}



-(void)getServiceCall{
    //http://voliveafrica.com/togo/api/services/payment_cards?API-KEY=225143&user_id=134721528
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading,please wait..."]];
    [SVProgressHUD dismiss];
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"payment_cards?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
           
            card_namesArray = [NSMutableArray new];
            card_numbersArray = [NSMutableArray new];
            card_idsArray = [NSMutableArray new];
            selected_array = [NSMutableArray new];
            
            
            if ([status isEqualToString:@"1"])
            {
                NSArray *data  = [dict objectForKey:@"data"];
                
                for(NSDictionary *mainData in data){
                    
                    [card_namesArray addObject:[mainData objectForKey:@"name"]];
                    [card_numbersArray addObject:[mainData objectForKey:@"card_number"]];
                    [card_idsArray addObject:[mainData objectForKey:@"id"]];
                    [selected_array addObject:@"0"];
                }
                [selected_array addObject:@"0"];
                [selected_array addObject:@"0"];
                
                [SVProgressHUD dismiss];
                [_mytableview reloadData];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
            }
        });
        
    }];
    

}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated :YES];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return card_numbersArray.count+2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == card_numbersArray.count) {
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
    }else if (indexPath.row == card_numbersArray.count+1){
     cell= [tableView dequeueReusableCellWithIdentifier:@"cell2"];
        if ([[selected_array objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [cell.codBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];

        }
        else
        {
            [cell.codBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];

        }
        
        cell.payLbl.text = self.pay_amount;
    }else{

        
    cell= [tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        cell.nameLbl.text = [card_namesArray objectAtIndex:indexPath.row];
        cell.numberLbl.text = [card_numbersArray objectAtIndex:indexPath.row];
        
        if ([[selected_array objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            
            [cell.cardsBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];

        }else{
            [cell.cardsBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];

        }
        
    }

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if (indexPath.row == card_numbersArray.count) {
        return 50;
        
        
    }else if (indexPath.row == card_numbersArray.count+1){
        return 190;
    }else{
        
      return 60;
    }


}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == card_numbersArray.count) {
        AddnewCard *card = [self.storyboard instantiateViewControllerWithIdentifier:@"AddnewCard"];
        [self.navigationController pushViewController:card animated:YES];
    
    }
}


- (IBAction)COD_BTN:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mytableview];
    NSIndexPath *indexPath = [self.mytableview indexPathForRowAtPoint:btnPosition];
    
    
    for (int i=0; i<selected_array.count; i++) {
        if (i==indexPath.row) {
            
            [selected_array replaceObjectAtIndex:indexPath.row withObject:@"1"];
            codString = @"COD";
            cardIDString =@"";
            
        }
        else{
            [selected_array replaceObjectAtIndex:i withObject:@"0"];
        }
        
    }
    
    [self.mytableview reloadData];
//    [cell.codBtn_Outlet setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
//    [cell.cardsBtn_Outlet setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
}


- (IBAction)paymentBtnAction:(id)sender {
    
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.mytableview];
    NSIndexPath *indexPath = [self.mytableview indexPathForRowAtPoint:btnPosition];
    
    
    for (int i=0; i<selected_array.count; i++) {
        if (i==indexPath.row) {
            [selected_array replaceObjectAtIndex:indexPath.row withObject:@"1"];
            codString = @"Online";
            cardIDString = [card_idsArray objectAtIndex:indexPath.row];

        }
        else{
            [selected_array replaceObjectAtIndex:i withObject:@"0"];
        }
        
        
        
    }
    
    [self.mytableview reloadData];
    
}


-(void)cartArray:(NSMutableArray *)cart{
    
    cart_itemsArray = cart;
    NSLog(@"items cart %@",cart_itemsArray);
}

- (IBAction)deletePayment:(id)sender {
    //http://voliveafrica.com/togo/api/services/delete_card?API-KEY=225143&user_id=134721528&card_id=1
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mytableview];
    NSIndexPath *indexPath = [_mytableview indexPathForRowAtPoint:btnPosition];


    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc]init];
    [paymentDictionary setObject:APIKEY forKey:@"API-KEY"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [paymentDictionary setObject:[card_idsArray objectAtIndex:indexPath.row] forKey:@"card_id"];
    [[sharedclass sharedInstance]fetchResponseforParameter:@"delete_card?" withPostDict:paymentDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                [self getServiceCall];
                
            }
            else
            {
                [[sharedclass sharedInstance]showAlertWithTitle:@"Error" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                [SVProgressHUD dismiss];
                
            }
        });
        
    }];
    
}

-(void)paymentPOSTService:(id)sender{
    //http://voliveafrica.com/togo/api/services/payment
    
    /*
     API-KEY:225143
     card_id:5
     car_number:apo72252
     car_type:dfgah
     payment_type:online
     user_id:2856066910
     vendor_id:3288062319
     order_id:5530161372713
     pay_by_cash:2010
     */
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mytableview];
    NSIndexPath *indexPath = [_mytableview indexPathForRowAtPoint:btnPosition];
    
    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc]init];
    
    [paymentDictionary setObject:APIKEY forKey:@"API-KEY"];
    [paymentDictionary setObject:cardIDString forKey:@"card_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"carNumber"] forKey:@"car_number"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"carType"] forKey:@"car_type"];
    
    
    [paymentDictionary setObject:codString forKey:@"payment_type"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [paymentDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"] forKey:@"vendor_id"];
    [paymentDictionary setObject:_orderIdString forKey:@"order_id"];
    
    [paymentDictionary setObject:_pay_amount forKey:@"pay_by_cash"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"payment" withPostDict:paymentDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
       // dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    OrderHistory *payVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderHistory"];
                    
                    [self.navigationController pushViewController:payVC animated:YES];
                    
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                 [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            }
            else
            {
                [[sharedclass sharedInstance]showAlertWithTitle:@"Error" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                [SVProgressHUD dismiss];
                
            }
       // });

    }];
    
   }

- (IBAction)submitBtn:(id)sender {
    [self paymentPOSTService:sender];
//    success *successVC = [self.storyboard instantiateViewControllerWithIdentifier:@"success"];
//    [self.navigationController pushViewController:successVC animated:TRUE];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
   

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
