//
//  Add.m
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Add.h"
#import "SVProgressHUD.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "AddCartTableViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "CartScreen.h"

@interface Add ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSArray *tableArr;
    NSArray *idArray;
    NSDictionary *myDic,* favDict;
    AddCartTableViewCell *cell;
    
    NSArray * finalArr;
    
    
    NSMutableArray * favIdsArr,* favCountArr,*selected_favIds;
    NSMutableArray * final_countArr;
    NSMutableArray * sizeArray;
    
    NSMutableArray *priceArray;
    NSMutableArray * smallCostArray ;
    NSMutableArray * mediumCostArray ;
    NSMutableArray *largeCostArray ;
    NSString *addShopIdString;
    
     UIToolbar *toolbar1;
    
    int totalPrice;
    int price1 , priceA,priceB,priceC;
    int price2;
    int mainItemCount;
    int price;
    
    NSString *smallCostString;
    NSString *mediumCostString;
    NSString *largeCostString;
    
    NSString *languageString;
    NSString *idString;
    NSString *sizeString;
    NSString *totalItems;
    int value;
    BOOL isFirstTime;
}
@end

@implementation Add

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // selected_favIds = [NSMutableArray new];
    
//    [_imageView sd_setImageWithURL:[NSURL URLWithString:_imageStr] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
//    
//    _nameLabel.text = _nameStr;
    
    favCountArr=[NSMutableArray new];
    favDict=[NSMutableDictionary new];
    finalArr=[NSMutableArray new];
    
    _flavourLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Flavour"];
    _costLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Cost"];
    
    [_add setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"ADD"] forState:UIControlStateNormal];
    
    idString = [[NSUserDefaults standardUserDefaults]objectForKey:@"itemId"];
    sizeString = [[NSUserDefaults standardUserDefaults]objectForKey:@"size"];
    

    
    self.navigationItem.hidesBackButton = YES;
    // Do any additional setup after loading the view.
}

-(void)show
{
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    [self.flavorTbl setContentOffset:CGPointZero animated:YES];
    
    [cell.requestTextField resignFirstResponder];
    
}
-(void)cancel
{
    [cell.requestTextField resignFirstResponder];
    [self.flavorTbl setContentOffset:CGPointZero animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.imageView.image =[UIImage imageNamed:@""];
    self.nameLabel.text = @"";
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:_imageStr] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    
    _nameLabel.text = _nameStr ? _nameStr :@"";
}

- (void) hideKeyboard {
    
    [cell.requestTextField resignFirstResponder];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    
//    [textField becomeFirstResponder];
//    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.flavorTbl];
//    CGPoint contentOffset = self.flavorTbl.contentOffset;
//    
//    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
//    
//    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
//    
//    [self.flavorTbl setContentOffset:contentOffset animated:YES];
//    
//    return YES;
//}
//
//
//-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    
//    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
//    {
//        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
//        NSIndexPath *indexPath = [self.flavorTbl indexPathForCell:cell];
//        
//        [self.flavorTbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
//    }
//    
//    return YES;
//}

- (IBAction)addClicked:(id)sender {
    
//    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.flavorTbl];
//    NSIndexPath *indexPath = [self.flavorTbl indexPathForRowAtPoint:btnPosition];
//
//    AddCartTableViewCell *datasetCell =
//    (AddCartTableViewCell *)[self.flavorTbl cellForRowAtIndexPath:indexPath];
    
    if (value > 0) {
        
        [self addToCartServiceCall];
        
    }else{
    
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please select flavour quantity"] onViewController:self completion:nil];

    }
 
}

-(void)addToCartServiceCall
{
//{"API-KEY":225143,"user_id":134721528,"vendor_id":3288062319,"item_id":3,"shop_id":3,"category_id":4,"request":"test","flavour_status":1,"flavour_ids":[{"flavour_id":1,"item_count":3},{"flavour_id":2,"item_count":2}]}
    
        NSString *url=@"http://voliveafrica.com/togo/api/services/add_cart";
        
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
        
        NSDictionary *dic = @{
                              @"API-KEY":APIKEY,
                              @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"],
                              @"vendor_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"vendorId"],
                              @"category_id":self.selected_catID,
                              @"request":cell.requestTextField.text ? cell.requestTextField.text : @"" ,
                              @"shop_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"],
                              @"item_id":_itemIdString,
                              @"flavour_status":@1,
                              @"flavour_ids":final_countArr,
                              @"item_count":[NSString stringWithFormat:@"%d",value],
                              @"item_size":_sizeStr ? _sizeStr : @""
                              };
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                           options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *postlength2=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];;
        
        NSString *postlength =[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonData];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:
         [NSString stringWithFormat:@"%lu",
          (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSLog(@"The request is %@",request);
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData*  _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)  {
            
            NSDictionary *statusDict = [[NSDictionary alloc]init];
            statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            if (statusDict) {
                NSLog(@"Images from server %@", statusDict);
                NSString * status = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
                // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([status isEqualToString:@"1"])
                    {
                        [SVProgressHUD dismiss];
                        
                        addShopIdString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"shop_id"]];
                        
                        NSLog(@"my shop id is %@",addShopIdString);
                        
                        [[NSUserDefaults standardUserDefaults]setObject:addShopIdString forKey:@"addShopId"];

                        //                    [[sharedclass sharedInstance]showAlertWithTitle:@"Success" withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] onViewController:self completion:^{
                        //
                        //                    }];
                        //
                        
                        if ([languageString isEqualToString:@"1"]) {
                            
                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                [self getServiceCall];
                                
                                [self.navigationController popViewControllerAnimated:TRUE];
                                
                            }];
                            [alert addAction:ok];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:@"تم أضافة المنتجات بنجاح " preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                [self getServiceCall];
                                
                                [self.navigationController popViewControllerAnimated:TRUE];
                                
                            }];
                            [alert addAction:ok];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
 
                    }
                    else
                    {
                        [SVProgressHUD dismiss];

                    }
                });
            }
            else{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] onViewController:self completion:nil];
            }
        }]
         
         resume];

    }


-(void)updateServicecall{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    [SVProgressHUD dismiss];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:idString forKey:@"item_id"];
    //int value1 = cell.quantityStepperView.value;
    
    [viewCartPostDictionary setObject:[NSString stringWithFormat:@"%d",value ] forKey:@"item_count"];
    [viewCartPostDictionary setObject:sizeString forKey:@"size"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"update_cart?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
                NSString * itemCountStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"no_of_items"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:itemCountStr forKey:@"count"];
                
                NSString * itemCostStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"total_cost"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:itemCostStr forKey:@"cost"];
                
                [self.navigationController popViewControllerAnimated:TRUE];
//
//                _itemLbl.text=[NSString stringWithFormat:@"%@ %@ | %@ SAR",itemCountStr,[[sharedclass sharedInstance] languageSelectedStringForKey:@"Items"],itemCostStr];
                
            }
            else
            {
                [SVProgressHUD dismiss];
                
                //                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                
            }
        });
        
    }];

    
}

-(void)getServiceCall{
    
   // myLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
   // myLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/cart_items?API-KEY=225143&user_id=134721528&lang=en
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
//    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
   // [SVProgressHUD showWithStatus:progress];
    
    NSMutableDictionary * viewCartPostDictionary = [[NSMutableDictionary alloc]init];
    [viewCartPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [viewCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [viewCartPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"cart_items?" withPostDict:viewCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        totalItems = [NSString stringWithFormat:@"%@",[dict objectForKey:@"total_items"]];
        
        NSLog(@"Cart Total Items Are %@",totalItems);
        dispatch_async(dispatch_get_main_queue(), ^{
            
         
            priceArray = [NSMutableArray new];
            
            smallCostArray = [NSMutableArray new];
            mediumCostArray = [NSMutableArray new];
            largeCostArray = [NSMutableArray new];
            sizeArray = [NSMutableArray new];
            
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                NSArray *array = [dict objectForKey:@"data"];
                int totalCost = 0;
                
                for(NSDictionary *mainData in array){
                    
                    
                    sizeString = [mainData objectForKey:@"item_size"];
                    
                    if ([sizeString isEqualToString:@"small"]) {
                        
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"small_cost"] intValue];
                        priceA = [[mainData objectForKey:@"small_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        
                        totalPrice = (mainItemCount * price1) + price;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                       // [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                        
                    }else if ([sizeString isEqualToString:@"medium"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"medium_cost"] intValue];
                        priceA = [[mainData objectForKey:@"medium_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        totalPrice = (mainItemCount * price1) + price;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        //[priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                        
                    }else if ([sizeString isEqualToString:@"large"])
                    {
                        mainItemCount = [[mainData objectForKey:@"item_count"]intValue];
                        price1 = [[mainData objectForKey:@"large_cost"] intValue];
                        priceA = [[mainData objectForKey:@"large_cost"] intValue];
                        price2 = [[mainData objectForKey:@"flavor_price"] intValue];
                        
                        price = mainItemCount * price2;
                        totalPrice = (mainItemCount * price1) + price;
                        
                        [priceArray addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                       // [priceArrayConstant addObject:[NSString stringWithFormat:@"%d",totalPrice]];
                        
                    }
                    
                    NSLog(@"Price Array Is%@",priceArray);
                    
                    [self.delegate receiveMessage:priceArray count:[NSString stringWithFormat:@"%@",totalItems]];
                   
                    [sizeArray addObject:[mainData objectForKey:@"item_size"]];
                    
                    [smallCostArray addObject:[mainData objectForKey:@"small_cost"]];
                    [mediumCostArray addObject:[mainData objectForKey:@"medium_cost"]];
                    [largeCostArray addObject:[mainData objectForKey:@"large_cost"]];
                    
                    smallCostString = [mainData objectForKey:@"small_cost"];
                    mediumCostString = [mainData objectForKey:@"medium_cost"];
                    largeCostString = [mainData objectForKey:@"large_cost"];
                    
                    totalCost = (totalCost + [[mainData objectForKey:@"cost"] intValue]);
                    
                    
                }
                
            }
            
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
                
            }
            
        });
        
    }];
    
}


-(void)stepperAction:(id)sender{

    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.flavorTbl];
    NSIndexPath *indexPath = [self.flavorTbl indexPathForRowAtPoint:btnPosition];

//    itemIdString = [itemIdArray objectAtIndex:indexPath.row];
    AddCartTableViewCell *datasetCell =
   (AddCartTableViewCell *)[self.flavorTbl cellForRowAtIndexPath:indexPath];
    
     value = datasetCell.quantityStepperView.value;
//    [self updateCart:[NSString stringWithFormat:@"%d",value]];
    
    NSLog(@"stepper count is *** %d",value);
    
    NSString * strr=[NSString stringWithFormat:@"%d",value];
    
    [favCountArr addObject:strr];

    //finalArr=[favIdsArr arrayByAddingObjectsFromArray:favCountArr];
    
    NSLog(@"final array values **** %@",finalArr);
    
    NSString *selected_favId = [favIdsArr objectAtIndex:indexPath.row];
    NSString *check_id = [selected_favIds objectAtIndex:indexPath.row];
    
    
    if ([strr isEqualToString:@"0"]) {
        
        //[final_countArr removeObjectAtIndex:indexPath.row];
        

    }else{
    
        NSDictionary * finalDict=@{
                               @"flavour_id":[favIdsArr objectAtIndex:indexPath.row],
                               @"item_count": strr
                               
                               };
    
        if ([check_id isEqualToString:@"1"]){
            
        [final_countArr replaceObjectAtIndex:indexPath.row withObject:finalDict];
        
        }else{
            
            [final_countArr addObject:finalDict];
        }
        
        [selected_favIds replaceObjectAtIndex:indexPath.row withObject:@"1"];

        }
    
    NSLog(@"final array values **** %@",final_countArr);
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row ==tableArr.count) {
        return 81;
    }else {
        return 55;
        
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableArr.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == tableArr.count) {
       
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell2"];
        NSInteger index = indexPath.row-1;
        
        toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
        toolbar1.barStyle = UIBarStyleBlackOpaque;
        toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        toolbar1.backgroundColor=[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0];
        
        UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
        
        [doneButton1 setTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
        
        [cancelButton1 setTintColor:[UIColor whiteColor]];
        toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
        
        [cell.requestTextField setInputAccessoryView:toolbar1];
        
        cell.splRequestLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Special Request"];
        cell.requestTextField.layer.borderColor = [[UIColor grayColor]CGColor];
        cell.requestTextField.layer.cornerRadius = 5.0f;
        cell.requestTextField.layer.borderWidth = 1;
        cell.requestTextField.placeholder = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Do you have a special request?"];

        NSLog(@"%ld",(long)index );
       // cell.requestTextField.text=@"suman";
        
    }else
    {
        cell = [_flavorTbl dequeueReusableCellWithIdentifier:@"cell1"];
        myDic = [tableArr objectAtIndex:indexPath.row];
        NSLog(@"my dic %@ ",myDic);
        cell.flavourLabel.text = [myDic objectForKey:@"flavour"];
        cell.costLabel.text = [NSString stringWithFormat:@"%@ SAR",[myDic objectForKey:@"price"]];
        cell.quantityLabel.text = [myDic objectForKey:@"size"];
        //cell.quantityStepperView.value = [[myDic objectForKey:@"flavour_count"]integerValue];
        //cell.quantityLabel.text = [NSString stringWithFormat:@"Quantity:%@",[sizeArray objectAtIndex:indexPath.row]];
     
       
        cell.quantityStepperView.minimumValue = 0;
        cell.quantityStepperView.maximumValue = 100000000000;
        [cell.quantityStepperView addTarget:self action:@selector(stepperAction:) forControlEvents:UIControlEventValueChanged];
        isFirstTime = YES;
        
        
        cell.quantityStepperView.value = 0;
        return cell;
        
           }
    return cell;
    
        }

-(void)flavoursArray:(NSArray *)fArr{
    
    NSLog(@"myarr : %@",fArr);
    tableArr = fArr;
    
    NSLog(@"ids array %@",tableArr);
    
    final_countArr = [NSMutableArray new];
    favIdsArr = [NSMutableArray new];
    selected_favIds = [NSMutableArray new];
    if (tableArr>0) {
        
        for (int i=0; i<tableArr.count; i++) {
            
            favDict=[tableArr objectAtIndex:i];
            NSString * favStr=[favDict objectForKey:@"flavour_id"];
            [favIdsArr addObject:favStr];
            [selected_favIds addObject:@"0"];
        }
        NSLog(@"final ARr %@",final_countArr);
        
    }
    // NSString * idStr=[
    [self.flavorTbl reloadData];
}

@end
