//
//  ServiceProviderController.h
//  TO GO
//
//  Created by volive solutions on 27/02/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceProviderController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *SPOrderHistoryTableview;
@property NSString *navigateString;

@end
