//
//  sharedclass.m
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "sharedclass.h"
#import "HeaderAppConstant.h"
@implementation sharedclass

static sharedclass * sharedClassObj = nil;

+(sharedclass *)sharedInstance {
    
    if (sharedClassObj == nil) {
        sharedClassObj = [[super allocWithZone:NULL]init];
    }
    
    return sharedClassObj;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return sharedClassObj;
}

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        
        
    }
    return self;
}

#pragma mark Reachability Checking
-(BOOL)isNetworkAvalible {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [Reachability reachabilityWithHostName:@"http://www.google.co.in/"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        return NO;
    }
    else {
        return YES;
    }
}
#pragma mark - textfieldAsLine
-(void)textfieldAsLine:(UITextField *)myTextfield lineColor:(UIColor *)lineColor myView:(UIView *)view{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.0;
    border.borderColor = lineColor.CGColor;
    border.frame =CGRectMake(0, myTextfield.frame.size.height - borderWidth, view.frame.size.width, myTextfield.frame.size.height);
    border.borderWidth = borderWidth;
    [myTextfield.layer addSublayer:border];
    myTextfield.layer.masksToBounds = YES;
    
}


-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(ToGoGet)completionHandler
{
    
    NSString * urlString;
    
    urlString = [self modifyURLString:parameterString valuesDictionary:postDictionary];
    
    NSLog(@"%@ URL string :%@",parameterString,urlString);
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSURLSessionConfiguration * urlSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * urlSession = [NSURLSession sessionWithConfiguration:urlSessionConfiguration];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            
            NSData * _Nullable  dataFromJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            if (dataFromJson != nil) {
                
                completionHandler ( dataFromJson );
                
            }else
            {
                [self showprogressfor:@"Server problem,please try after sometime"];
                [SVProgressHUD dismiss];
                
            }
            
        }
        else
        {
            [self showprogressfor:@"Server problem,please try after sometime"];
            [SVProgressHUD dismiss];
            
        }
        
        
        
    }];
    
    [dataTask resume];
    
}

-(NSString *)modifyURLString: (NSString *) urlString
            valuesDictionary: (NSDictionary *)valuesFromUser

{
    
    NSString * finalURLString;
    
    if ([urlString isEqualToString:@"forgot?"]) {
        
        //http://voliveafrica.com/togo/api/services/forgot?API-KEY=225143&email=testcase1@yopmail.com
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&email=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"email"],[valuesFromUser objectForKey:@"lang"]];
        
    } else if ([urlString isEqualToString:@"home?"]) {
        
        //http://voliveafrica.com/togo/api/services/home?API-KEY=225143&lang=en
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"]];
        
    }else if ([urlString isEqualToString:@"restaurants?"]) {
        
        //http://voliveafrica.com/togo/api/services/restaurants?API-KEY=225143&lang=en&category_id=4
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&category_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"category_id"]];
        
    }else if ([urlString isEqualToString:@"restaurant_details?"]) {
        
        //http://voliveafrica.com/togo/api/services/restaurant_details?API-KEY=225143&lang=en&shop_id=5
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&shop_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"shop_id"]];
        
    }else if ([urlString isEqualToString:@"items?"]) {
        
        //http://voliveafrica.com/togo/api/services/items?API-KEY=225143&lang=en&shop_id=1&menu_id=2
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&shop_id=%@&menu_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"shop_id"],[valuesFromUser objectForKey:@"menu_id"]];
        
    }
    else if ([urlString isEqualToString:@"search_restaurant?"]) {
        
        //http://voliveafrica.com/togo/api/services/search_restaurant?API-KEY=225143&lang=en&name_search=cafe
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&name_search=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"name_search"]];
        
    }
    else if ([urlString isEqualToString:@"cart_items?"]) {
        
        //http://voliveafrica.com/togo/api/services/cart_items?API-KEY=225143&user_id=134721528&lang=en
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"payment_cards?"]) {
        
        //http://voliveafrica.com/togo/api/services/payment_cards?API-KEY=225143&user_id=134721528
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"]];
        
    }
    else if ([urlString isEqualToString:@"delete_card?"]) {
        
        //http://voliveafrica.com/togo/api/services/delete_card?API-KEY=225143&user_id=134721528&card_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&card_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"card_id"]];
        
    }
    else if ([urlString isEqualToString:@"profile?"]) {
        
        //http://voliveafrica.com/togo/api/services/profile?API-KEY=225143&user_id=134721528
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"delete_cart?"]) {
        
//    http://voliveafrica.com/togo/api/services/delete_cart?API-KEY=225143&user_id=134721528
        
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"update_cart?"])
    {
        //http://voliveafrica.com/togo/api/services/update_cart?API-KEY=225143&user_id=134721528&item_id=3&item_count=1
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&item_id=%@&item_count=%@&lang=%@&size=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"item_id"],[valuesFromUser objectForKey:@"item_count"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"size"]];

    }
    else if ([urlString isEqualToString:@"order_history?"]) {
        
        //http://voliveafrica.com/togo/api/services/order_history?API-KEY=225143&user_id=3208428429&lang=en
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&user_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"track_order?"]) {
        
        //http://voliveafrica.com/togo/api/services/track_order?API-KEY=225143&order_id=0379621881623
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&order_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"order_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"confirm_pickup?"]) {
        
        //http://voliveafrica.com/togo/api/services/confirm_pickup?API-KEY=225143&order_id=0379621881623&arrival_time=2017-12-19%2004:04:08&confirm_pickup=2017-12-19%2004:04:08
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&order_id=%@&arrival_time=%@&confirm_pickup=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"order_id"],[valuesFromUser objectForKey:@"arrival_time"],[valuesFromUser objectForKey:@"confirm_pickup"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"notifications?"]) {
        
        //http://voliveafrica.com/togo/api/services/notifications?API-KEY=225143&lang=ar&user_id=134721528
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&user_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"user_id"]];
        
    }
    else if ([urlString isEqualToString:@"order_approval?"]) {
        
        //http://voliveafrica.com/togo/api/services/order_approval?API-KEY=225143&order_id=5530161372713
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&order_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"order_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }else if ([urlString isEqualToString:@"order_view?"]) {
        
    //http://voliveafrica.com/togo/api/services/order_view?API-KEY=225143&order_id=5530161372713
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&order_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"order_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"order_expired?"]) {
        
        //http://voliveafrica.com/togo/api/services/order_expired?API-KEY=225143&lang=ar&order_id=5273323857251
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&order_id=%@&tax_amount=%@&grand_total=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"order_id"],[valuesFromUser objectForKey:@"tax_amount"],[valuesFromUser objectForKey:@"grand_total"]];
        
    }
    else if ([urlString isEqualToString:@"update_cartcount?"]) {
        
        //http://voliveafrica.com/togo/api/services/update_cartcount?API-KEY=225143&id=75&count=1
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&id=%@&count=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"id"],[valuesFromUser objectForKey:@"count"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"location_restaurants?"]) {
        
        //http://voliveafrica.com/togo/api/services/location_restaurants?API-KEY=225143&latitude=17.4082411&longitude=78.4470965&lang=en
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&latitude=%@&longitude=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"latitude"],[valuesFromUser objectForKey:@"longitude"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    else if ([urlString isEqualToString:@"confirmation_orders?"]) {
        
        //http://voliveafrica.com/togo/api/services/confirmation_orders?API-KEY=225143&service_provider_id=2620161185&lang=en
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&service_provider_id=%@&lang=%@",base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"service_provider_id"],[valuesFromUser objectForKey:@"lang"]];
        
    }
    
    finalURLString = [finalURLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    return finalURLString;
}


#pragma mark - urlPerameterforPost
-(void)urlPerameterforPost:(NSString* )paramStr withPostDict:(NSDictionary* )postDict andReturnwith:(ToGoPost)completionHandler {
    
    NSString *UrlStr;
    
    if ([paramStr isEqualToString:@"login"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"registration"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"social"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    
    else if ([paramStr isEqualToString:@"add_cart"]){
            UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if([paramStr isEqualToString:@"add_payment_card"]){
         UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"payment"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"confirm_pickup"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"suggest_restaurant"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"restaurant_discover"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"user_rating"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"order_requst_restaurant"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"delete_cart_item"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"update_order_status"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }

    
    NSMutableURLRequest*urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:UrlStr]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:30];
    [urlRequest setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in postDict) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [postDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:body];
        
    }
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session=[NSURLSession sharedSession];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData*  _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError *err;
        
        if (!error) {
            NSDictionary *serverDatadict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
                if (data != nil) {
                
                if ([paramStr isEqualToString:@"login"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"registration"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"social"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"add_cart"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"add_payment_card"]){
                    completionHandler (serverDatadict, nil, true, @"Success");

                }
                else if ([paramStr isEqualToString:@"payment"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"confirm_pickup"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"suggest_restaurant"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"restaurant_discover"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"user_rating"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"order_requst_restaurant"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"delete_cart_item"]) {
                
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"update_order_status"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                                
                else {
                    
                    completionHandler (nil, nil, false, @"Failed");
                }
                
            }
            
            else {
                
                completionHandler (nil, nil, false, @"Failed");
            }
            
        }
        
        else {
            
            
            NSLog(@"Error is %@", error.debugDescription);
            
            completionHandler (nil, nil, false, @"Unable to fetch.");
            
        }
        
    }];
    
    [postDataTask resume];
    
}

#pragma mark - showAlertWithTitle
-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

{
//    if (message == nil || [message isKindOfClass:[NSNull class]]) {
//        
//        title =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sorry!"] ;
//        message =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Server problem,please try after some time"] ;
//        
//    }
    UIAlertController * suggestionsAlert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
     //  completionBlock ();
        
    }];
    
    [suggestionsAlert addAction:okAction];
    
    [viewController presentViewController:suggestionsAlert animated:YES completion:nil];
}

-(void)showprogressfor:(NSString *)str {
    
    [SVProgressHUD showWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    
}

-(void)showprogressforSucsess:(NSString *)str {
    
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"SuccessTick3.png"]];
    [SVProgressHUD showSuccessWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD dismissWithDelay:1.0];
    
}

#pragma mark - GET
-(void)GETList:(NSString *)post completion :(void (^)(NSDictionary *, NSError *))completion{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[post stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@" res :%@",response);
        
        if ([data length] == 0) {
            
            
        }else{
            //NSLog(@"-data-%@",data);
            NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            _json = [NSJSONSerialization JSONObjectWithData:data
                                                    options:kNilOptions
                                                      error:nil];
            //NSLog(@"json : %@",_json);
            NSLog(@"myString : %@",myString);
            
        }
        completion(_json, error);
    }]resume];
    
}

#pragma mark - POST
-(void)postMethodForServicesRequest:(NSString *)serviceType :(NSString *)post completion:(void (^)(NSDictionary *, NSError *))completion{
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",base_URL,serviceType]]];
    NSLog(@"---%@",[NSString stringWithFormat:@"%@%@",base_URL, serviceType]);
    [request setHTTPMethod:@"POST"];
    //[request setTimeoutInterval: 100];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    // code here
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        _json = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions
                                                  error:nil];
        NSLog(@"_json :%@",_json);
        
        completion(_json, error);
    }]resume];
}

#pragma mark - languageSelectedStringForKey
-(NSString*)languageSelectedStringForKey:(NSString*) key
{
    
    NSBundle *path;
    
    NSString *  selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"language"];
    
    if([selectedLanguage isEqualToString:ENGLSIH_LANGUAGE])
        
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
    
    else if ([selectedLanguage isEqualToString:ARABIC_LANGUAGE])
    {
        
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
    }
    else{
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        
    }
    
    //NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    NSString *str = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(key, @"LocalizedStrings", path, nil)];
    // NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;

}

-(void)showalrtfor:(NSString* )str inViewCtrl:(UIViewController* )vc
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] message:str                                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    [vc presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            // do something ?
        }];
        
    });
}
@end
