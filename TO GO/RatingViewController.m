//
//  RatingViewController.m
//  TO GO
//
//  Created by volive solutions on 10/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "RatingViewController.h"
#import "HCSStarRatingView.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "Home.h"
#import "SWRevealViewController.h"

@interface RatingViewController ()<UITextFieldDelegate>
{
    NSString *languageString;
}

@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [_itemImageView sd_setImageWithURL:[NSURL URLWithString:_imageString] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    _itemNameLabel.text=_nameString;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _noteTextField) {
        
        [_ratingScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-180) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_noteTextField resignFirstResponder];
    
    
    [_ratingScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _noteTextField) {
        
        [_noteTextField resignFirstResponder];
        
        returnValue = YES;
        
    }    return returnValue;
}


-(void)ratingServiceCall{
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    /*
     API-KEY:225143
     user_id:12343454
     order_id:224234
     shop_id:1
     rating:1
     review:hello
     */
    
    // NSString *url = @"http://voliveafrica.com/togo/api/services/user_rating";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * ratingPostDictionary = [[NSMutableDictionary alloc]init];
    [ratingPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [ratingPostDictionary setObject:languageString forKey:@"lang"];
    [ratingPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [ratingPostDictionary setObject:_orderIdString forKey:@"order_id"];
    [ratingPostDictionary setObject:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"]] forKey:@"shop_id"];
    [ratingPostDictionary setObject:[NSString stringWithFormat:@"%f",_starRatingView.value] forKey:@"rating"];
    [ratingPostDictionary setObject:_noteTextField.text forKey:@"review"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"user_rating" withPostDict:ratingPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
//                        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//                        
//                        [self.navigationController pushViewController:home animated:YES];
                        
                        SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                        
                        //self.navigationController.navigationBar.hidden = YES;
                        
                       // [self.navigationController pushViewController:reveal animated:TRUE];
                        [self presentViewController:reveal animated:TRUE completion:nil];

                        
                        
                        
                    }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                });
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (IBAction)submit_BTN:(id)sender {
    
    if (_starRatingView.value ==0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Please Give Rating..."] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
            
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else
    {
        [self ratingServiceCall];
    }
    
    
    
}
@end
