//
//  CartScreen.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface CartScreen : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (weak, nonatomic) IBOutlet UITableView *cartTbl;
@property (weak, nonatomic) IBOutlet UIButton *payBtn_Outlet;

    @property (weak, nonatomic) IBOutlet UIButton *checkStatus_Btn;
- (IBAction)checkStatusBtnAction:(id)sender;
@property NSString *itemSizeString;

@property (weak, nonatomic) IBOutlet UIButton *proceedToPay_Outlet;



@end
