//
//  TrackOrder.m
//  TO GO
//
//  Created by Volive solutions on 11/3/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "TrackOrder.h"
#import "sharedclass.h"
#import "HeaderAppConstant.h"
#import "RatingViewController.h"
#import "Home.h"
#import "SWRevealViewController.h"
#import "ServiceProviderController.h"


@interface TrackOrder ()
{
    NSString *orderIdString;
    NSString *trackLatitudeString;
    NSString *trackLongitudeString;
    
    NSString *myLat;
    NSString *myLong;
    
    NSString *userLatString;
    NSString *userLongString;
    
    GMSCameraPosition *cameraPosition;
    GMSMapView *googleMapView;
    GMSMarker *marker,*destMarker;
    GMSPath *path;
    NSString * arrival_timeStr;
    NSString * confirm_pickupStr;
    
    NSString *languageString;
}

@end

@implementation TrackOrder

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLat = [[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"];
    myLong = [[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"];
    
    // Do any additional setup after loading the view.
    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Track Order"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.leftBarButtonItem = anotherButton;
    _minBtn.layer.cornerRadius = _minBtn.frame.size.height/2;
    _cnfmBtn_Outlet.layer.cornerRadius = 4;
    
    _orderRcvdLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Order Received"];
    _orderCnfmdLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order Confirmed"];
    _preparingTimeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Preparing Time"];
    _arrivalTimeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Arrival Time"];
    _cnfmPickupLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Confirm pickup by client "];

    [_cnfmBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Confirm Pickup"] forState:UIControlStateNormal];
    
    
    
    
}

-(void)back:(UIButton *)sender{
    
    if ([_checkString isEqualToString:@"back"]) {
        
        ServiceProviderController *service = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceProviderController"];
        //[self.navigationController presentViewController:service animated:TRUE completion:nil];
        [self.navigationController pushViewController:service animated:TRUE];
        
        
    }else {
        
        SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        
        [self.navigationController presentViewController:reveal animated:TRUE completion:nil];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)trackOrderServiceCall:(NSString *)orderId{
    
    orderIdString = orderId;
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/togo/api/services/track_order?API-KEY=225143&order_id=0379621881623
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
    [orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [orderHistoryPostDictionary setObject:orderId forKey:@"order_id"];
    [orderHistoryPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"track_order?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                _orderReceivedInfoLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"order_recived"]];
                _orderConfirmedInfoLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"order_confirmed"]];
                
                NSString *time = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"preparing_time"]];
                
               // _preparingTimeInfoLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"preparing_time"]];
                _arrivalTimeInfoLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"arrival_time"]];
                _confirmPickupInfoLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"confirm_pickup"]];
                trackLatitudeString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"shop_lat"]];
                trackLongitudeString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"shop_long"]];
                userLatString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"user_lat"]];
                userLongString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"user_long"]];
                
                
                if (_orderReceivedInfoLabel.text.length != 0 ) {
                    
                     _orderRcvdImageView.image = [UIImage imageNamed:@"Check"];
                
                }else{
                
                    _orderRcvdImageView.image = [UIImage imageNamed:@"Path 235"];

                }
                if (_orderConfirmedInfoLabel.text.length != 0 ) {
                    
                    _orderConfirmedImageView.image = [UIImage imageNamed:@"Check"];
                    
                }else{
                    
                    _orderConfirmedImageView.image = [UIImage imageNamed:@"Path 235"];
                    
                }
                if (time.length > 0 ) {
                    
                    _preparingTimeInfoLabel.text = [NSString stringWithFormat:@"%@ %@",time,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Mins"]];
                    
                    [_minBtn setTitle:[NSString stringWithFormat:@"%@",_preparingTimeInfoLabel.text] forState:UIControlStateNormal];
                    
                    NSString *timeString = [NSString stringWithFormat:@"%@|%@",_preparingTimeInfoLabel.text,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Estimated time to pickup"]];
                    
                    [_timeBtn_Outlet setTitle:timeString forState:UIControlStateNormal];
                    
                    _preparingTimeImageView.image = [UIImage imageNamed:@"Check"];
                    
                }else{
                    
                    _preparingTimeImageView.image = [UIImage imageNamed:@"Path 235"];
                    
                    NSString *timeString = [NSString stringWithFormat:@"%@|%@",_preparingTimeInfoLabel.text,[[sharedclass sharedInstance]languageSelectedStringForKey:@"Estimated time to pickup"]];
                    
                    [_timeBtn_Outlet setTitle:timeString forState:UIControlStateNormal];
                    
                }
                if (_arrivalTimeInfoLabel.text.length != 0 ) {
                    
                    _arrivalImageView.image = [UIImage imageNamed:@"Check"];
                    
                }else{
                    
                    _arrivalImageView.image = [UIImage imageNamed:@"Path 235"];
                    
                }
                if (_confirmPickupInfoLabel.text.length != 0 ) {
                    
                    _confirmImageView.image = [UIImage imageNamed:@"Check"];
                    
                }else{
                    
                    _confirmImageView.image = [UIImage imageNamed:@"Path 235"];
                    
                }

                
                [self loadMaps];
            }
            
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:@"Message" withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
        });
        
    }];
}

- (IBAction)confirmPickup_BTN:(id)sender {
    
    [self confirmPickupServiceCall];
}

-(void)confirmPickupServiceCall{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    /*
     API-KEY:225143
     order_id=0379621881623
     arrival_time=2017-12-19 04:04:08
     confirm_pickup=2017-12-19 04:04:08
     */
    // NSString *url = @"http://voliveafrica.com/togo/api/services/confirm_pickup";
    
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
    //        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * confirmPostDictionary = [[NSMutableDictionary alloc]init];
    
    [confirmPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [confirmPostDictionary setObject:orderIdString forKey:@"order_id"];
    [confirmPostDictionary setObject:@"2018-01-19 04:04:08" forKey:@"arrival_time"];
    [confirmPostDictionary setObject:@"2018-01-19 06:04:08" forKey:@"confirm_pickup"];
    [confirmPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"confirm_pickup" withPostDict:confirmPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSString * arrival_timeStr1=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"arrival_time"]];
                    
                    _arrivalTimeInfoLabel.text=arrival_timeStr1;
                    
                    
                    NSString * confirm_pickupStr1=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"confirm_pickup"]];
                    _confirmPickupInfoLabel.text=confirm_pickupStr1;
                    
                    
                    NSString * confirmStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"order_confirmed"]];
                    NSString * receivedStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"order_recived"]];
                    NSString * preparingStr=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"preparing_time"]];
                    
                    if ([preparingStr length] == 0) {
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Message"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"You're not paid for this order"] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }else{
                        
                        _orderConfirmedInfoLabel.text=confirmStr ? confirmStr:@"";
                        _orderReceivedInfoLabel.text=receivedStr ? receivedStr:@"";
                        
                        _preparingTimeInfoLabel.text=preparingStr ? preparingStr:@"";
                        [_minBtn setTitle:[NSString stringWithFormat:@"%@ %@",preparingStr ? preparingStr:@"",[[sharedclass sharedInstance] languageSelectedStringForKey:@"Mins"]] forState:UIControlStateNormal];
                        
                        _arrivalImageView.image = [UIImage imageNamed:@"Check"];
                        _confirmImageView.image = [UIImage imageNamed:@"Check"];
                        
//                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
//                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                            
                            if ([_checkString isEqualToString:@"customer"]) {
                                
                                RatingViewController *rating = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
                                rating.orderIdString = orderIdString;
                                rating.imageString=_imageStr;
                                rating.nameString=_nameStr;
                                
                                [self.navigationController pushViewController:rating animated:TRUE];
                            }else{
                                
                                [_cnfmBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Order Completed"] forState:UIControlStateNormal];
                            }
                            
//                        }];
//                        [alert addAction:ok];
//                        [self presentViewController:alert animated:YES completion:nil];


                    }
                    
                  
                    
//                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    
                  
                });
            }
            else
            {
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            }
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];
}


- (void)loadMaps {
    
    googleMapView.delegate = self;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[myLat doubleValue]
                                                            longitude:[myLong doubleValue]
                                                                 zoom:10];
    googleMapView = [GMSMapView mapWithFrame:self.trackOrderMapView.bounds camera:camera];
    
    googleMapView.settings.myLocationButton = NO;
    googleMapView.camera = camera;
    
    googleMapView.myLocationEnabled = YES;
    // _successMapView = googleMapView;
    destMarker = [[GMSMarker alloc]init];
    destMarker.position = CLLocationCoordinate2DMake([trackLatitudeString doubleValue], [trackLongitudeString doubleValue]);
    destMarker.map = googleMapView;
    
    UIImage *userIcon = [UIImage imageNamed:@"pin"];
    userIcon = [userIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (userIcon.size.height/2), 0)];
    destMarker.icon = userIcon;

   
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([trackLatitudeString doubleValue], [trackLongitudeString doubleValue]);
    marker.tracksViewChanges = YES;
    marker.map = googleMapView;
    [self drawRoute];
    [self.trackOrderMapView addSubview:googleMapView];
    
}

- (void)drawRoute
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CLLocation *mylocation = [[CLLocation alloc]initWithLatitude:[myLat doubleValue] longitude:[myLong doubleValue]];
        
        CLLocation *destination = [[CLLocation alloc]initWithLatitude:[trackLatitudeString doubleValue] longitude:[trackLongitudeString doubleValue]];
        
        [self fetchPolylineWithOrigin:mylocation destination:destination completionHandler:^(GMSPolyline *polyline)
         {
             if(polyline)
                 polyline.map = googleMapView;
         }];
    });
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                                     
                                                     GMSPolyline *polyline = nil;
                                                     if ([routesArray count] > 0)
                                                     {
                                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                         path = [GMSPath pathFromEncodedPath:points];
                                                         polyline = [GMSPolyline polylineWithPath:path];
                                                     }
                                                     polyline.strokeColor = [UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0];
                                                     polyline.strokeWidth = 2;
                                                     
                                                     // run completionHandler on main thread
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         if(completionHandler)
                                                             completionHandler(polyline);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}



@end
