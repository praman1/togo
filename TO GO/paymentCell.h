//
//  paymentCell.h
//  TO GO
//
//  Created by volive on 12/28/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface paymentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *numberLbl;
@property (weak, nonatomic) IBOutlet UILabel *payLbl;
@property (weak, nonatomic) IBOutlet UIButton *cardsBtn_Outlet;

//cell1
@property (weak, nonatomic) IBOutlet UITextField *payByCashTextField;
@property (weak, nonatomic) IBOutlet UIButton *codBtn_Outlet;


@end
