//
//  success.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "success.h"
#import "TrackOrder.h"

@interface success ()<GMSMapViewDelegate>
{
    NSString *shopLatitude;
    NSString *shopLongitude;
    NSString *transactionIdString;
    
    GMSCameraPosition *cameraPosition;
    GMSMapView *googleMapView;
    GMSMarker *marker;
    
}

@end

@implementation success

- (void)viewDidLoad {
    [super viewDidLoad];

    transactionIdString = [[NSUserDefaults standardUserDefaults]objectForKey:@"transactionId"];
    shopLatitude = [[NSUserDefaults standardUserDefaults]objectForKey:@"shopLatitude"];
    shopLongitude = [[NSUserDefaults standardUserDefaults]objectForKey:@"shopLongitude"];
   
    
    [self loadMaps];
    
       
}
-(void)back:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadMaps {
    
    googleMapView.delegate = self;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[shopLatitude doubleValue]
                                                            longitude:[shopLongitude doubleValue]
                                                                 zoom:10];
    googleMapView = [GMSMapView mapWithFrame:self.successMapView.bounds camera:camera];
    
    googleMapView.settings.myLocationButton = NO;
    googleMapView.camera = camera;
    
    googleMapView.myLocationEnabled = YES;
   // _successMapView = googleMapView;
    
    [self.successMapView addSubview:googleMapView];
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([shopLatitude doubleValue], [shopLongitude doubleValue]);
    marker.tracksViewChanges = YES;
    marker.map = googleMapView;
    
}

-(void)loadCustomDesign
{
    self.title = @"Successful";
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    //initWithTitle:@"Show" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList:)
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    
    _dateLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"date"];
    _paymentTypeLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"paymentType"];
    
    
    _detailsLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    _transactionIdLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"transactionId"];
    _orderTotalLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"orderTotal"];
    // _cashToBringLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"date"];
    // _dateLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"date"];
}


-(void)viewWillAppear:(BOOL)animated{
    [self loadCustomDesign];
    dispatch_async(dispatch_get_main_queue(), ^{

        //[self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides

        [UIView animateWithDuration:0 animations:^{

            self.successImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);

        }completion:^(BOOL finished){

            // Finished scaling down imageview, now resize it

            [UIView animateWithDuration:0.4 animations:^{

                self.successImageView.transform = CGAffineTransformIdentity;

            }completion:^(BOOL finished){


            }];


        }];

    });

}

- (IBAction)trackOrder_BTN:(id)sender {
    
    TrackOrder *trackOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
    
    [trackOrderVC trackOrderServiceCall:transactionIdString];
    [self.navigationController pushViewController:trackOrderVC animated:TRUE];

    
}
@end
