//
//  DetailedMenu.m
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "DetailedMenu.h"
#import "HomeTableViewCell.h"
#import "HeaderAppConstant.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "sharedclass.h"
#import "RestaurantMenu.h"
#import "DetailedMenuTableViewCell.h"
@interface DetailedMenu ()<UITableViewDataSource,UITableViewDelegate>
{
    DetailedMenuTableViewCell *detailedCell;
    NSArray *imgs,*names;
    NSString *categoryIdString,*combinedString,*vendorIdString;
    NSMutableArray *restaurantsCountArray,*restaurantNameArray,*restaurantSubNameArray,*restaurantLogoArray,*restaurantTimeArray,*vendorIdArray,*shopIdArray,*catIdArray;
    
    NSString *languageString;
    
    NSString *toString;
    NSString *fromString;

    NSMutableArray *restaurantToTimeArray;
    NSMutableArray *restaurantFromTimeArray;
    
}
@end

@implementation DetailedMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadCustomDesign];
    // Do any additional setup after loading the view.
}

-(void)loadCustomDesign
{
    imgs = @[@"Restaurant",@"Coffee Shop",@"Food Truck"];
    NSLog(@"_bartitle %@",_bartitle);
    self.title = _bartitle;
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    anotherButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    
    UIBarButtonItem *anotherButton1 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"white search"] style:UIBarButtonItemStylePlain target:self action:nil];
    anotherButton1.tintColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = anotherButton1;

}



-(void)back:(UIButton *)sender{
    NSLog(@"press");
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)detailedMenuServiceCall:(NSString *)categoryId
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    categoryIdString = categoryId;
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    // http://voliveafrica.com/togo/api/services/restaurants?API-KEY=225143&lang=en&category_id=4
    NSMutableDictionary * detailedMenuPostDictionary = [[NSMutableDictionary alloc]init];
    [detailedMenuPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [detailedMenuPostDictionary setObject:languageString forKey:@"lang"];
    [detailedMenuPostDictionary setObject:categoryId forKey:@"category_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"restaurants?" withPostDict:detailedMenuPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@" My Data Is %@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            [SVProgressHUD dismiss];
            restaurantsCountArray = [NSMutableArray new];
            restaurantNameArray = [NSMutableArray new];
            restaurantSubNameArray = [NSMutableArray new];
            restaurantLogoArray = [NSMutableArray new];
            restaurantTimeArray = [NSMutableArray new];
            vendorIdArray = [NSMutableArray new];
            shopIdArray = [NSMutableArray new];
            catIdArray = [NSMutableArray new];
            restaurantToTimeArray = [NSMutableArray new];
            restaurantFromTimeArray = [NSMutableArray new];
           
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                 restaurantsCountArray=[dataDictionary objectForKey:@"data"];
                    if (restaurantsCountArray.count>0) {
                        for (int i=0; i<restaurantsCountArray.count; i++) {
                            [restaurantNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                            [restaurantSubNameArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category_name"]];
                            [restaurantLogoArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"logo"]];
                            
                            fromString =[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"from_time"]];
                            
                            toString = [NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"to_time"]];
                            
                            [restaurantToTimeArray addObject:toString];
                            [restaurantFromTimeArray addObject:fromString];
                            
                            
                            
                            combinedString = [NSString stringWithFormat:@"%@ - %@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"from_time"],[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"to_time"]];
                            
                            NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
                            [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
                            NSDate* date1 =[[NSDate alloc]init];
                            date1=[df1 dateFromString:fromString];
                            [df1 setDateFormat:@"hh:mm a"];
                            NSString *fromDate = [df1 stringFromDate:date1];
                            
                            NSDateFormatter* df2 = [[NSDateFormatter alloc] init];
                            [df2 setDateFormat:@"dd-MM-yy hh:mm a"];
                            NSDate* date2 =[[NSDate alloc]init];
                            date2=[df2 dateFromString:toString];
                            [df2 setDateFormat:@"hh:mm a"];
                            NSString *toDate = [df2 stringFromDate:date2];
                            
                            
                            [restaurantTimeArray addObject:[NSString stringWithFormat:@"%@ - %@",fromDate,toDate]];
                            
                            
                           [vendorIdArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"vendor_id"]]];
                            [shopIdArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"shop_id"]]];
                            [catIdArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category_id"]]];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:vendorIdArray forKey:@"vendorId"];
                            [[NSUserDefaults standardUserDefaults]setObject:shopIdArray forKey:@"shopId"];
                
                                        NSLog(@"%@",restaurantNameArray);
                                        NSLog(@"%@",restaurantSubNameArray);
                
                                    }
                                }
                    dispatch_async(dispatch_get_main_queue(), ^{
                                [_detailedMenuTableView reloadData];
                });
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

#pragma mark - tableview methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return restaurantsCountArray.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    detailedCell = [_detailedMenuTableView dequeueReusableCellWithIdentifier:@"detailedCell"];
    
    if (detailedCell == nil) {
        
        detailedCell = [[DetailedMenuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"detailedCell"];

    }
    detailedCell.restaurantNameLabel.text = [restaurantNameArray objectAtIndex:indexPath.row];
    detailedCell.restaurantSubNameLabel.text = [restaurantSubNameArray objectAtIndex:indexPath.row];
    detailedCell.restaurantTimeLabel.text = [restaurantTimeArray objectAtIndex:indexPath.row];
    [detailedCell.restaurantItemImageView sd_setImageWithURL:[NSURL URLWithString:[restaurantLogoArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    
    //Current Time
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    //FromTime
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSDate* newDate =[[NSDate alloc]init];
    newDate=[df dateFromString:[restaurantFromTimeArray objectAtIndex:indexPath.row]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *reqFromTimeString = [df stringFromDate:newDate];
    
    //ToTime
    NSDateFormatter* df1 = [[NSDateFormatter alloc] init];
    [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df1 setTimeZone:[NSTimeZone systemTimeZone]];
    [df1 setDateFormat:@"dd-MM-yy hh:mm a"];
    NSDate* newDate1 =[[NSDate alloc]init];
    newDate1=[df1 dateFromString:[restaurantToTimeArray objectAtIndex:indexPath.row]];
    [df setDateFormat:@"dd-MM-yy hh:mm a"];
    NSString *reqToTimeString = [df stringFromDate:newDate1];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yy hh:mm a"];
    
    NSDate *date1= [formatter dateFromString:reqFromTimeString];
    NSDate *date2 = [formatter dateFromString:reqToTimeString];
    NSDate *date3 = [formatter dateFromString:resultString];
    
    NSComparisonResult FromComparison = [date3 compare:date1];
    NSComparisonResult ToComparison = [date3 compare:date2];
    
    if(FromComparison == NSOrderedDescending)
    {
        NSLog(@"date3 is later than date1");
        if (ToComparison==NSOrderedDescending) {
            
            NSLog(@"date3 is later than date2");
            NSLog(@"Sorry, we are closed!");
            
            [detailedCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
            
            
        }
        else if (ToComparison==NSOrderedAscending)
        {
            
            NSLog(@"date2 is later than date3");
            NSLog(@"We are Open!");
            
            
            
            [detailedCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"] forState:UIControlStateNormal];
            
        }
    }
    else if(FromComparison == NSOrderedAscending)
    {
        NSLog(@"date1 is later than date3");
        NSLog(@"Sorry, we are closed!");
        
        
        
        [detailedCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
    }
    else
    {
        NSLog(@"date3 is equal to date1");
        NSLog(@"Sorry, we are closed!");
        
        
        
        [detailedCell.openOrCloseBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"] forState:UIControlStateNormal];
    }
    return detailedCell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailedMenuTableViewCell *cell = (DetailedMenuTableViewCell *)[_detailedMenuTableView cellForRowAtIndexPath:indexPath];
    if ([cell.openOrCloseBtn_Outlet.titleLabel.text isEqualToString:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Opened"]])  {
        
        RestaurantMenu *menu = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantMenu"];
        [[NSUserDefaults standardUserDefaults]setObject:[shopIdArray objectAtIndex:indexPath.row] forKey:@"shopId"];
        [menu restaurantMenuServiceCall:[shopIdArray objectAtIndex:indexPath.row]];
        menu.selected_catID = [catIdArray objectAtIndex:indexPath.row];
       // menu.shopIdArray = shopIdArray;
       // menu.shopIdStr=[shopIdArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:menu animated:TRUE];

    }
    else if ([cell.openOrCloseBtn_Outlet.titleLabel.text isEqualToString:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Closed"]]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!" message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"This Shop is closed"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"OK"] style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
   // [self performSegueWithIdentifier:@"RestuarentDetails" sender:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
