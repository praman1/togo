//
//  notifications.m
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "notifications.h"
#import "NotificationTableViewCell.h"
#import "SWRevealViewController.h"
#import "HeaderAppConstant.h"
#import "sharedclass.h"

@interface notifications ()<UITableViewDataSource,UITableViewDelegate>{
    NSArray *arrNotification;
    NSMutableArray *notificationArrayCount;
    NSMutableArray *notificationsArray;
    NSMutableArray *createdDateArray;
    NotificationTableViewCell *notificationCell;

}
@property (weak, nonatomic) IBOutlet UITableView *mytblview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;


@end

@implementation notifications

- (void)viewDidLoad {
    [super viewDidLoad];
    [self notificationsServiceCall];
    // Do any additional setup after loading the view.
    _mytblview.rowHeight = UITableViewAutomaticDimension;
    _mytblview.estimatedRowHeight = 500;
    
     self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menu setTarget: self.revealViewController];
        [self.menu setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (IBAction)back:(UIBarButtonItem *)sender {
    
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)notificationsServiceCall{
    
    //http://voliveafrica.com/togo/api/services/notifications?API-KEY=225143&lang=ar&user_id=134721528
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading,please wait..."]];
    
    NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
    [orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [orderHistoryPostDictionary setObject:@"en" forKey:@"lang"];
    [orderHistoryPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"notifications?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            notificationArrayCount = [NSMutableArray new];
            notificationsArray = [NSMutableArray new];
            createdDateArray = [NSMutableArray new];
            
            
            if ([status isEqualToString:@"1"])
            {
                [SVProgressHUD dismiss];
                notificationArrayCount = [NSMutableArray new];
                notificationsArray = [NSMutableArray new];
                createdDateArray = [NSMutableArray new];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    notificationArrayCount = [dict objectForKey:@"data"];
                    if (notificationArrayCount.count > 0) {
                        
                        for (int i=0; i<notificationArrayCount.count; i++) {
                            
                            [notificationsArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"notifications"]];
                            [createdDateArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"created_date"]];
                            
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_mytblview reloadData];
                        
                    });
                });
            }
            else
            {
                [SVProgressHUD dismiss];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[sharedclass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"data"]] onViewController:self completion:nil];
//                });
                
                
            }
        });
        
    }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return notificationArrayCount.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    notificationCell = [_mytblview dequeueReusableCellWithIdentifier:@"cell"];
    
    if (notificationCell == nil) {
        notificationCell = [[NotificationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    notificationCell.notificationLbl.text = [NSString stringWithFormat:@"%@",[notificationsArray objectAtIndex:indexPath.row]];
    notificationCell.createdDateLabel.text = [NSString stringWithFormat:@"%@",[createdDateArray objectAtIndex:indexPath.row]];
    
    return notificationCell;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return UITableViewAutomaticDimension;

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
