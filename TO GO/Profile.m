//
//  Profile.m
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "Profile.h"
#import "sharedclass.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"

@interface Profile (){

    sharedclass *objForshare;
    UIImage *profile_pic;
    UIImagePickerController * picker_Profile_Pic;
    NSString *FileParamConstant;
    NSString *languageString;

}

@end

@implementation Profile

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadCustomDesign];
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width/2;
    _profileImageView.clipsToBounds = YES;

    // Do any additional setup after loading the view.
     [self getProfileServiceCall];
}

-(void)loadCustomDesign
{

    self.title = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Profile"];
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    objForshare = [[sharedclass alloc] init];
    
    if([languageString isEqualToString:@"ar"]){
        
        _nameTxt.textAlignment = NSTextAlignmentRight;
        _emailTxt.textAlignment = NSTextAlignmentRight;
        _pswTxt.textAlignment = NSTextAlignmentRight;
        _numberTxt.textAlignment = NSTextAlignmentRight;
        _Cpsw.textAlignment = NSTextAlignmentRight;
        _Carnum.textAlignment = NSTextAlignmentRight;
        _carType.textAlignment = NSTextAlignmentRight;
    
        
    } else  {
        
        _nameTxt.textAlignment = NSTextAlignmentLeft;
        _emailTxt.textAlignment = NSTextAlignmentLeft;
        _pswTxt.textAlignment = NSTextAlignmentLeft;
        _numberTxt.textAlignment = NSTextAlignmentLeft;
        _Cpsw.textAlignment = NSTextAlignmentLeft;
        _Carnum.textAlignment = NSTextAlignmentLeft;
        _carType.textAlignment = NSTextAlignmentLeft;
    }
    
    
    [objForshare textfieldAsLine:_nameTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    
    [objForshare textfieldAsLine:_pswTxt lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_Cpsw lineColor:[UIColor lightGrayColor] myView:self.view];
    
    [objForshare textfieldAsLine:_carType lineColor:[UIColor lightGrayColor] myView:self.view];
    
    [objForshare textfieldAsLine:_Carnum lineColor:[UIColor lightGrayColor] myView:self.view];
    
    
    _nameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Name"];
    _emailLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Email"];
    _passwordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Password"];
    _cnfmPasswordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Confirm Password"];
    _mobileNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Mobile Number"];
    _carNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Car Number"];
    _carTypeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Car Type"];
    _changePasswordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Password"];
    _hintLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Leave passwords empty,when you don't want change it"];
    _changeCarLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Car"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menu setTarget: self.revealViewController];
        [self.menu setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    

}

-(void)viewWillAppear:(BOOL)animated{
   
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _emailTxt) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _numberTxt) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _pswTxt) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _Cpsw) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }
    else if (textField == _Carnum) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _carType) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_nameTxt resignFirstResponder];
    [_emailTxt resignFirstResponder];
    [_numberTxt resignFirstResponder];
    [_pswTxt resignFirstResponder];
    [_Cpsw resignFirstResponder];
    [_Carnum resignFirstResponder];
    [_carType resignFirstResponder];
    
    [_profileScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTxt) {
        
        [_emailTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailTxt) {
        
        [_numberTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _numberTxt) {
        
        [_pswTxt becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _pswTxt) {
        
        [_Cpsw becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _Cpsw) {
        
        [_Carnum becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _Carnum) {
        
        [_carType becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _carType) {
        
        [_carType resignFirstResponder];
        
        returnValue = YES;
        
    }
    return returnValue;
}

-(void)getProfileServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    // [objforsharedclass alertforLoading:self];
    
    // http://voliveafrica.com/togo/api/services/profile?API-KEY=225143&user_id=134721528
    NSMutableDictionary * profilePostDictionary = [[NSMutableDictionary alloc]init];
    
    [profilePostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [profilePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [profilePostDictionary setObject:languageString forKey:@"lang"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"profile?" withPostDict:profilePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _nameTxt.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"name"]];
                _emailTxt.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"email"]];
                _numberTxt.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"] valueForKey:@"mobile"]];
                //_pswTxt.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"email"]];
                _Carnum.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"car_number"]];
                _carType.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"car_type"]];
                NSString *imageString = [[dataDictionary objectForKey:@"data"]objectForKey:@"image"];
                
                [_profileImageView sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@""]];
                [[NSUserDefaults standardUserDefaults]setObject:imageString forKey:@"profileImage"];
                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"name"] forKey:@"name"];
                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"email"] forKey:@"emailId"];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}

-(void)updateProfileServiceCall{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    // NSString *url = @"http://voliveafrica.com/togo/api/services/update_profile";
    /*
     API-KEY:225143
     name:test
     email:ramukithada@gmail.com
     mobile:965231478
     passwd:
     confirm_passwd:
     car_number:
     car_type:
     user_id:134721528
     file (file parameter to upload profile pic)
     */
   
    NSString *url = @"http://voliveafrica.com/togo/api/services/update_profile";
    
    
    NSMutableDictionary * updateProfilePostDictionary = [[NSMutableDictionary alloc]init];
    
    
    [updateProfilePostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [updateProfilePostDictionary setObject:_nameTxt.text forKey:@"name"];
    [updateProfilePostDictionary setObject:_emailTxt.text forKey:@"email"];
    [updateProfilePostDictionary setObject:_numberTxt.text forKey:@"mobile"];
    [updateProfilePostDictionary setObject:_pswTxt.text forKey:@"passwd"];
    [updateProfilePostDictionary setObject:_Cpsw.text forKey:@"confirm_passwd"];
    [updateProfilePostDictionary setObject:_Carnum.text forKey:@"car_number"];
    [updateProfilePostDictionary setObject:_carType.text forKey:@"car_type"];
    [updateProfilePostDictionary setObject:languageString forKey:@"lang"];
    [updateProfilePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    // [updateProfilePostDictionary setObject:_profileImageView.image forKey:@"file"];
    
    FileParamConstant = @"file";
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in updateProfilePostDictionary) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [updateProfilePostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    _profileImageView.image = [self scaleImage:profile_pic toSize:CGSizeMake(200.0,200.0)];
    
    
    //For Image Uploading
    NSData *imageData = UIImageJPEGRepresentation(profile_pic,1.0);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
        
        NSDictionary *statusDict = [[NSDictionary alloc]init];
        statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"Images from server %@", statusDict);
        //
       NSString * status  = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
        _profileImageView.image = profile_pic;
        
        // message1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[statusDict objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
                [[NSUserDefaults standardUserDefaults]setObject:_emailTxt.text forKey:@"emailId"];
                [[NSUserDefaults standardUserDefaults]setObject:_nameTxt.text forKey:@"name"];
                //[[NSUserDefaults standardUserDefaults]setObject:_profileImageView.image forKey:@"profileImage"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];

                
                
            });
            
            [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:nil];
            
            
            [self getProfileServiceCall];
        }
        else
        {
            [SVProgressHUD dismiss];
            [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:nil];
            
        }    }]
     
     resume];

    
}


- (IBAction)updateProfile_BTN:(id)sender {
    [_nameTxt resignFirstResponder];
    [_emailTxt resignFirstResponder];
    [_numberTxt resignFirstResponder];
    [_pswTxt resignFirstResponder];
    [_Cpsw resignFirstResponder];
    [_Carnum resignFirstResponder];
    [_carType resignFirstResponder];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (_nameTxt.text.length < 3 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [_nameTxt becomeFirstResponder]; }];
        
    } else if ([emailTest evaluateWithObject:_emailTxt.text] == NO ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [_emailTxt becomeFirstResponder]; }];
        
    }
    //    else if (_pswTxt.text.length < 5 ) {
    //
    //        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [_pswTxt becomeFirstResponder]; }];
    //
    //    }
    else if (![_Cpsw.text isEqualToString:_pswTxt.text]) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Password and Confirm Password must be same"] onViewController:self completion:^{  [_Cpsw becomeFirstResponder]; }];
        
    } else if (_numberTxt.text.length < 5 ) {
        
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [_numberTxt becomeFirstResponder]; }];
        
    }
    [self updateProfileServiceCall];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage* )image editingInfo:(NSDictionary *)editingInfo {
    
    if (picker == picker_Profile_Pic) {
        
        profile_pic = image;
        
        _profileImageView.image=profile_pic;
        //[self networkCheck];
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(UIImage *)scaleImage:(UIImage* )image toSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)addProfileImage_BTN:(id)sender {
    
    UIAlertController * view=   [UIAlertController alertControllerWithTitle:@"Pick the image" message:@"From" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* PhotoLibrary = [UIAlertAction actionWithTitle:
                                   @"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                       
                                       picker_Profile_Pic = [[UIImagePickerController alloc] init];
                                       picker_Profile_Pic.delegate = self;
                                       [picker_Profile_Pic setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                       [self presentViewController:picker_Profile_Pic animated:YES completion:NULL];
                                       [view dismissViewControllerAnimated:YES completion:nil];    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {  }];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action) {
        
        picker_Profile_Pic = [[UIImagePickerController alloc] init];
        picker_Profile_Pic.delegate = self;
        [picker_Profile_Pic setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:picker_Profile_Pic animated:YES completion:NULL];
        [view dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [view addAction:PhotoLibrary];
    [view addAction:camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}
@end
