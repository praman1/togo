//
//  OrderHistoryTableViewCell.h
//  TO GO
//
//  Created by volive solutions on 03/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placedOnDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *trackOrderLabel;

@property (weak, nonatomic) IBOutlet UIButton *viewDetailsBtn;
@property (weak, nonatomic) IBOutlet UIButton *trackOrder_Outlet;

@property (weak, nonatomic) IBOutlet UIButton *callBtn_Outlet;


@end
