//
//  OrderHistory.h
//  TO GO
//
//  Created by Volive solutions on 11/1/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistory : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (weak, nonatomic) IBOutlet UITableView *orderHistoryTableView;
- (IBAction)orderBack:(id)sender;

@end
