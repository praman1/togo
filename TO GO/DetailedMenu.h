//
//  DetailedMenu.h
//  TO GO
//
//  Created by Volive solutions on 11/7/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedMenu : UIViewController
@property NSString *bartitle;
@property NSMutableArray *categoryIdArray;
-(void)detailedMenuServiceCall:(NSString *)categoryId;
@property (weak, nonatomic) IBOutlet UITableView *detailedMenuTableView;

@end
