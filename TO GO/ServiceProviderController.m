//
//  ServiceProviderController.m
//  TO GO
//
//  Created by volive solutions on 27/02/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "ServiceProviderController.h"
#import "OrderHistoryTableViewCell.h"
#import "SWRevealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HeaderAppConstant.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "UIViewController+ENPopUp.h"
#import "TrackOrder.h"
#import "OrderViewDetailsController.h"
#import "Login.h"
#import "AppDelegate.h"



@interface ServiceProviderController ()<UITableViewDelegate,UITableViewDataSource>
{
    OrderHistoryTableViewCell *cell;
    
    NSMutableArray *placedDateArray;
    NSMutableArray *orderIdArray;
    NSMutableArray *itemImageArray;
    NSMutableArray *itemNameArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *totalAmountArray;
    NSMutableArray *statusInfoArray;
    NSMutableArray *mobileNumberArray;
    NSMutableArray *ordersCountArray;
    
    NSString *imageString;
    NSString *shopNameString;
    
    NSString *languageString;
    
    OrderViewDetailsController * order;
    AppDelegate *delegate;
    
}

@end

@implementation ServiceProviderController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    [self.navigationController setNavigationBarHidden:NO];
    
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    backBtn.tintColor =[UIColor clearColor];
    
    [backBtn setEnabled:NO];
    
    
    self.navigationItem.leftBarButtonItem = backBtn;
    
    UIBarButtonItem *logOut = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sign Out"] style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    logOut.tintColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = logOut;

    
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order History"];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    NSLog(@"hfhsdfghjdshg %@", self.navigationController.navigationBar.tintColor);
    
    [self orderHistoryServicecall];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
//    [self.SPOrderHistoryTableview addGestureRecognizer:tap];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reload) name:@"reload" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshMenu) name:@"refreshData" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"refresh" object:nil];
    
    // Do any additional setup after loading the view.
}
//-(void)hide{
//    
//    [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
//}

-(void)reload{
    
    [self orderHistoryServicecall];
    
}

-(void)back
{
    
}

-(void)refreshMenu
{
    delegate.spCheckString = @"back";
    
    if ([delegate.orderStr isEqualToString:@"order"]) {
        TrackOrder *track = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
        
        NSString * orderID =[[NSUserDefaults standardUserDefaults]objectForKey:@"orderId"];
        track.checkString = delegate.spCheckString;
        [track trackOrderServiceCall:orderID];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [self.navigationController pushViewController:track animated:TRUE];
        
    }

}
-(void)refresh{
    
     if ([delegate.orderStr isEqualToString:@"service"]) {
        
        ServiceProviderController *service = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceProviderController"];
        [self presentViewController:service animated:TRUE completion:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [self.navigationController pushViewController:service animated:TRUE];
        
    }
}


-(void)logout
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Logout"]
                                 message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Are you sure to Logout?"]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Yes"]
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction * action) {
                                    
                                    //appDelegate.side=@"side1";
                                    
                                    
                                    UIViewController * gotoMainNav = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavVC"];
                                    [[UIApplication sharedApplication] delegate].window.rootViewController = gotoMainNav;
                                    [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                                    
//                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                                    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
//                                    window.rootViewController = [storyboard instantiateInitialViewController];

                                    
                                    

                                    
                                    }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 //Handle no, thanks button
                             }];
    
    //Add your buttons to alert controller
    [alert addAction:cancel];
    [alert addAction:yesButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
 

}

#pragma mark Tableview Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return ordersCountArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell = [_SPOrderHistoryTableview dequeueReusableCellWithIdentifier:@"orderCell"];
    
    if (cell == nil) {
        
        cell = [[OrderHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderCell"];
    }
    
    [cell.itemImageView sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
    cell.itemNameLabel.text = [NSString stringWithFormat:@"%@",shopNameString ? shopNameString :@""];
    cell.categoryNameLabel.text = [categoryNameArray objectAtIndex:indexPath.row];
    cell.placedOnDateLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On:"],[placedDateArray objectAtIndex:indexPath.row]];
    cell.orderIdLabel.text = [NSString stringWithFormat:@"%@ : %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order id:"],[orderIdArray objectAtIndex:indexPath.row]];
    cell.totalAmountLabel.text =[NSString stringWithFormat:@"%@SAR",[totalAmountArray objectAtIndex:indexPath.row]];
    cell.statusInfoLabel.text = [statusInfoArray objectAtIndex:indexPath.row];
    cell.totalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Total"];
    cell.statusLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Status:"];
    
    [cell.viewDetailsBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"View Details"] forState:UIControlStateNormal];
    
    [cell.trackOrder_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Track Order"] forState:UIControlStateNormal];
    
    
    [cell.viewDetailsBtn addTarget:self action:@selector(viewDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.trackOrder_Outlet addTarget:self action:@selector(viewTrackOrder:) forControlEvents:UIControlEventTouchUpInside];
    
    // [cell.callBtn_Outlet addTarget:self action:@selector(callToRestaurant:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 172;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark OrderHistory Servicecall
-(void)orderHistoryServicecall{
    
    //http://voliveafrica.com/togo/api/services/confirmation_orders?API-KEY=225143&service_provider_id=2620161185&lang=en
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
    [orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    [orderHistoryPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"service_provider_id"];
    [orderHistoryPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"confirmation_orders?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            placedDateArray = [NSMutableArray new];
            orderIdArray = [NSMutableArray new];
            itemImageArray = [NSMutableArray new];
            itemNameArray = [NSMutableArray new];
            categoryNameArray = [NSMutableArray new];
            totalAmountArray = [NSMutableArray new];
            statusInfoArray = [NSMutableArray new];
            mobileNumberArray = [NSMutableArray new];
            ordersCountArray = [NSMutableArray new];
            
            if ([status isEqualToString:@"1"])
            {
                
                [SVProgressHUD dismiss];
                
                shopNameString = [[[dict objectForKey:@"data"]objectForKey:@"shop_details"] objectForKey:@"shop_name"] ? [[[dict objectForKey:@"data"]objectForKey:@"shop_details"] objectForKey:@"shop_name"] : @"";
                imageString = [[[dict objectForKey:@"data"]objectForKey:@"shop_details"] objectForKey:@"logo"];
                
                ordersCountArray = [[dict objectForKey:@"data"]objectForKey:@"orders"];
                
                if (ordersCountArray.count > 0) {
                    
                    for (int i=0; i<ordersCountArray.count; i++) {
                        
                        [placedDateArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_recived"]]];
                        [orderIdArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_id"]]];
                        [totalAmountArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"net_amount"]]];
                        [statusInfoArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"order_status"]]];
                        [categoryNameArray addObject:[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"orders"] objectAtIndex:i] objectForKey:@"category"]]];
                        
                    }
                }
                else
                {
                    [SVProgressHUD dismiss];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[sharedclass sharedInstance]showAlertWithTitle:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"No orders found"] onViewController:self completion:nil];
                    });
                }
                [_SPOrderHistoryTableview reloadData];
                //[[NSNotificationCenter defaultCenter] removeObserver:self];
            }
            else
            {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[sharedclass sharedInstance]showAlertWithTitle:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
        });
        
    }];
}

-(void)viewTrackOrder:(id)sender{
    
    delegate.spCheckString = @"back";
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_SPOrderHistoryTableview];
    NSIndexPath *indexPath = [_SPOrderHistoryTableview indexPathForRowAtPoint:btnPosition];
    
    TrackOrder *trackOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrder"];
    
    trackOrderVC.nameStr= shopNameString;
    trackOrderVC.imageStr=imageString;
    trackOrderVC.checkString = delegate.spCheckString;
    
    [trackOrderVC trackOrderServiceCall:[orderIdArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:trackOrderVC animated:TRUE];
    
}


-(void)viewDetails:(id)sender
{
    delegate.checkStatusString = @"sp";
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_SPOrderHistoryTableview];
    NSIndexPath *indexPath = [_SPOrderHistoryTableview indexPathForRowAtPoint:btnPosition];
    
    order =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewDetailsController"];
    order.statusString = [statusInfoArray objectAtIndex:indexPath.row];
    order.checkStatusString = delegate.checkStatusString;
    order.orderIdString = [orderIdArray objectAtIndex:indexPath.row];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:order.orderIdString forKey:@"orderId"];
    
    
    order.view.frame = CGRectMake(0, 0, 300.0f, 450.0f);
    
    // [order.closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self presentViewController:order animated:TRUE completion:nil];
    // [self presentPopUpViewController:order];
    
}

//-(void)dismiss
//{
//
//    [self dismissPopUpViewController];
//
//}





@end

