

//
//  OTPverification.m
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "OTPverification.h"
#import "sharedclass.h"
#import "Login.h"

@interface OTPverification (){
    sharedclass *objForshare;
    
    NSString *languageString;

}

@end

@implementation OTPverification

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _mblNoLabel.text = _mobileNumberStr;
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    _infoLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Waiting to automatically detect an SMS sent to your mobile number"];
    _enterDigitLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter 4 - Digit code"];
    _rcveCodeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Didn't receive the code?"];
    
    [_resendCodeBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"RESEND CODE"] forState:UIControlStateNormal];
    
    objForshare = [[sharedclass alloc] init];
    
    [objForshare textfieldAsLine:_OTP1 lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_OTP2 lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_OTP3 lineColor:[UIColor lightGrayColor] myView:self.view];
    [objForshare textfieldAsLine:_OTP4 lineColor:[UIColor lightGrayColor] myView:self.view];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
//    [self.view addGestureRecognizer:tap];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(clearNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    _OTP1.inputAccessoryView = numberToolbar;
      _OTP2.inputAccessoryView = numberToolbar;
      _OTP3.inputAccessoryView = numberToolbar;
      _OTP4.inputAccessoryView = numberToolbar;

    
}
-(void)clearNumberPad
{
    [_OTP1 resignFirstResponder];
     [_OTP2 resignFirstResponder];
     [_OTP4 resignFirstResponder];
     [_OTP3 resignFirstResponder];
}
-(void)doneWithNumberPad
{
    [_OTP1 resignFirstResponder];
    [_OTP2 resignFirstResponder];
    [_OTP4 resignFirstResponder];
    [_OTP3 resignFirstResponder];
}

#pragma mark TextField delegates
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_OTP4.text.length > 0) {
        Login * sign=[self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
        [self.navigationController pushViewController:sign animated:YES];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    
//    BOOL returnValue = NO;
//    
//    if (textField == _OTP1) {
//        
//        [_OTP2 becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _OTP2) {
//        
//        [_OTP3 becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _OTP3) {
//        
//        [_OTP4 becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _OTP4) {
//        
//        [_OTP4 resignFirstResponder];
//        
//        returnValue = YES;
//        
//    }
//    return returnValue;
//}

- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString* )string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == self.OTP1)
        {
            
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP2 afterDelay:0.1];
            
        }
        else if (textField == self.OTP2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP3 afterDelay:0.1];
        }
        else if (textField == self.OTP3)
        {
            
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP4 afterDelay:0.1];
            
        }
        else if (textField == self.OTP4)
        {
            
            [self performSelector:@selector(setResignResponder:) withObject:self.OTP4 afterDelay:0.1];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == self.OTP4)
        {
            
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP3 afterDelay:0.1];
            
        }
        else if (textField == self.OTP3)
        {
            
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP2 afterDelay:0.1];
            
        }
        else if (textField == self.OTP2)
        {
            
            [self performSelector:@selector(setNextResponder:) withObject:self.OTP1 afterDelay:0.1];
            
        }
    }
    
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
    
}
- (void)setResignResponder:(UITextField *)nextResponder
{
    [nextResponder resignFirstResponder];
    
}



- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-100,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,320,560)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
