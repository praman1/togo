//
//  OrderViewDetailsController.m
//  TO GO
//
//  Created by volive solutions on 20/01/18.
//  Copyright © 2018 Volive solutions. All rights reserved.
//

#import "OrderViewDetailsController.h"
#import "HeaderAppConstant.h"
#import "sharedclass.h"
#import "SVProgressHUD.h"
#import "SWRevealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "historyCell.h"
#import "AppDelegate.h"
#import "ConformVC.h"
#import "UIViewController+ENPopUp.h"


@interface OrderViewDetailsController ()
{
    NSMutableArray * itemImgArr;
    NSMutableArray * nameArr;
    NSMutableArray *sizeArray;
    NSMutableArray *totalAmountArray;
    NSMutableArray *taxAmountArray;
    NSMutableArray *grandTotalArray;
    
    NSString *taxAmountString;
    NSString *grandTotalString;
    NSString *userIdString;
    NSString *orderIdString;
    NSString *orderStatus;
    int tax;
    int netAmount;

    NSMutableArray * itemCategoryArr;

    NSMutableArray * itemNameArr;
    NSMutableArray * itemAmountArr ,* orderCountArr;
    
    NSString *languageString;
    historyCell * cell;
    
    AppDelegate *delegate;
    ConformVC *conformOrReject;
    
}

@end

@implementation OrderViewDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    //orderCountArr=[NSMutableArray new];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    if ([_checkString isEqualToString:@"yes"]) {
        
        _conformRejectView.hidden = TRUE;
        _closeBtn.hidden = FALSE;
        
        [_closeBtn setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"CLOSE"] forState:UIControlStateNormal];
        
    }else{
        if ([_statusString isEqualToString:@"Order Preparing"]) {
            
            _conformRejectView.hidden = TRUE;
            _closeBtn.hidden = FALSE;
        }else{
            _conformRejectView.hidden = FALSE;
            _closeBtn.hidden = TRUE;
            
            [_conformBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"CONFORM"] forState:UIControlStateNormal];
            [_rejectBtn_Outlet setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"REJECT"] forState:UIControlStateNormal];

        }

    }
    
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
        [self.view addGestureRecognizer:tap];

    [self orderViewServiceCall];
    
    // Do any additional setup after loading the view.
}

-(void)hide{
    
   
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

-(void)orderViewServiceCall{
    
    //http://voliveafrica.com/togo/api/services/order_view?API-KEY=225143&order_id=5530161372713
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];
    
    NSMutableDictionary * orderHistoryPostDictionary = [[NSMutableDictionary alloc]init];
    [orderHistoryPostDictionary setObject:APIKEY forKey:@"API-KEY"];
    
    if ([_checkString isEqualToString:@"yes"]) {
        
        [orderHistoryPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"orderId"] forKey:@"order_id"];
    }
    else{
        [orderHistoryPostDictionary setObject:_orderIdString ?_orderIdString :@"" forKey:@"order_id"];
    }
    
    [orderHistoryPostDictionary setObject:languageString forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"order_view?" withPostDict:orderHistoryPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary *dict = (NSDictionary *)dataFromJson;
        
        NSLog(@"Images from server %@", dict);
        NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([status isEqualToString:@"1"]) {
                [SVProgressHUD dismiss];
                
                orderCountArr=[NSMutableArray new];
                itemImgArr=[NSMutableArray new];
                nameArr=[NSMutableArray new];
                itemNameArr=[NSMutableArray new];
                itemAmountArr=[NSMutableArray new];
                itemCategoryArr=[NSMutableArray new];
                sizeArray=[NSMutableArray new];
                
                orderCountArr=[dict objectForKey:@"data"];
                
               dispatch_async(dispatch_get_main_queue(), ^{
                
                
                if (orderCountArr.count >0) {
                   
                    for (int i=0; i<orderCountArr.count; i++) {
                        
                        NSString * sizeStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"item_size"]];
                        
                        [sizeArray addObject:sizeStr];
                        
                        NSString * nameStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"restaurant_name"]];
                        
                        [nameArr addObject:nameStr];
                       
                        NSString * itemNameStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"item_name"]];
                        
                        [itemNameArr addObject:itemNameStr];
                        
                        
                        NSString * itemCostStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"total_amount"]];
                        
                        [itemAmountArr addObject:itemCostStr];
                        
                        // [itemCategoryArr addObject:[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"shop_category"]]];
                        
                        
                        NSString * itemImgStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        
                        [itemImgArr addObject:itemImgStr];
                        
                        taxAmountString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tax_amount"]];
                        
                        orderStatus = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_status"]];
                        
                        tax = [[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tax"] intValue];
                        
                        netAmount = [[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"net_amount"] intValue];
                        
                        grandTotalString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"grand_total"]];
                        
                        userIdString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"user_id"]];
                        
                        orderIdString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_id"]];
                        
                    }
                }
               
                 [_orderTable reloadData];
                
               });
                
            }else{
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Error"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            }
        });
        
    }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return nameArr.count+3;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
        NSString * imageStr=[NSString stringWithFormat:@"%@",[itemImgArr objectAtIndex:indexPath.row]];
        
        
        [cell.itemImage sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@""] completed:nil];
        
        cell.name.text=[nameArr objectAtIndex:indexPath.row];
        //cell.categoryName.text=[itemCategoryArr objectAtIndex:indexPath.row];
        
    }else if (indexPath.row == 1)
    {
         cell= [tableView dequeueReusableCellWithIdentifier:@"cell4"];
        
        cell.itemNameLbl.text=[[sharedclass sharedInstance] languageSelectedStringForKey:@"Item name"];
        cell.quantity.text=[[sharedclass sharedInstance] languageSelectedStringForKey:@"Quantity"];
        cell.price.text=[[sharedclass sharedInstance] languageSelectedStringForKey:@"Price(SAR)"];
       
        
    }
    
    else if (indexPath.row == nameArr.count+2){
        
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell3"];
        if ([orderStatus isEqualToString:@"0"]) {
            
            float totalA= netAmount;
            
            int taxA= tax;
            
            int totalstr= taxA * (totalA / 100);
            
           // [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",totalstr] forKey:@"totalStr"];
            
            int Total = totalA + totalstr;
            
           // cell.taxNameLabel.text=[NSString stringWithFormat:@"Tax(%dPer)",taxA];
            
           // cell.taxValueLabel.text=[NSString stringWithFormat:@"%d SAR",Total];
            cell.taxValueLabel.text = [NSString stringWithFormat:@"%dSAR",totalstr];
            cell.grandTotalValueLabel.text = [NSString stringWithFormat:@"%dSAR",Total];

        
        }else{
            
            cell.taxNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Tax"];
            cell.grandTotalNameLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Grand Total"];
            
            cell.taxValueLabel.text = [NSString stringWithFormat:@"%@",taxAmountString];
            cell.grandTotalValueLabel.text = [NSString stringWithFormat:@"%@",grandTotalString];
            cell.status.text = _statusString;

            
        }
        
        cell.statusLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Status"];
        cell.commentLabel.text = [[sharedclass sharedInstance] languageSelectedStringForKey:@"Comment"];
        
    }else{
        
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell2"];
        NSInteger index = indexPath.row-2;
        
        cell.ItemName.text=[NSString stringWithFormat:@"%@(%@)",[itemNameArr objectAtIndex:index],[sizeArray objectAtIndex:index]];
        cell.itemPrice.text=[NSString stringWithFormat:@"%@",[itemAmountArr objectAtIndex:index]];
        
    }
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 80;
    }else if (indexPath.row == 1){
        
        return 40;
    }
    
    else if (indexPath.row == nameArr.count+2){
        return 156;
        
    }else{
        return 46;
    }
    
    
}

- (IBAction)conform_BTN:(id)sender {
    
    
    delegate.spCheckString = @"conform";

    conformOrReject = [self.storyboard instantiateViewControllerWithIdentifier:@"ConformVC"];
    conformOrReject.userId = userIdString;
    conformOrReject.orderId = orderIdString;
    
    //[self dismissViewControllerAnimated:TRUE completion:nil];
    [conformOrReject setModalPresentationStyle:UIModalPresentationCurrentContext];
   [self presentViewController:conformOrReject animated:YES completion:nil];
    
     //[[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu" object:nil];
    
    
    
    //[self presentPopUpViewController:conformOrReject];
    
   // [self dismissPopUpViewController];
    
    //
    
}


- (IBAction)closeBtn:(id)sender {
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}



- (IBAction)reject_BTN:(id)sender {
    //[self dismissViewControllerAnimated:TRUE completion:nil];
    
    delegate.spCheckString = @"reject";
    
    conformOrReject = [self.storyboard instantiateViewControllerWithIdentifier:@"ConformVC"];
    conformOrReject.userId = userIdString;
    conformOrReject.orderId = orderIdString;
    
    [conformOrReject setModalPresentationStyle:UIModalPresentationCurrentContext];
    [self presentViewController:conformOrReject animated:YES completion:nil];

    
    //[self presentPopUpViewController:conformOrReject];
    
   // [self dismissViewControllerAnimated:TRUE completion:nil];
}




@end
