//
//  OTPverification.h
//  TO GO
//
//  Created by Volive solutions on 10/30/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPverification : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *OTP1;
@property (weak, nonatomic) IBOutlet UITextField *OTP2;
@property (weak, nonatomic) IBOutlet UITextField *OTP3;
@property (weak, nonatomic) IBOutlet UITextField *OTP4;

    @property (weak, nonatomic) IBOutlet UILabel *infoLabel;
    @property (weak, nonatomic) IBOutlet UILabel *enterDigitLabel;
    @property (weak, nonatomic) IBOutlet UILabel *rcveCodeLabel;
    @property (weak, nonatomic) IBOutlet UIButton *resendCodeBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *mblNoLabel;

@property NSString *mobileNumberStr;

@end
